<?php

namespace app\models;

use lithium\util\Inflector;
use app\extensions\helper\Debug;

/**
 * This class defines the relationship between an class and a submission.
 * Assignments must be submitted within the due date and they can be visible or hidden.
 */
class Assignments extends \app\extensions\data\Model {

	public $belongsTo = ['Classes', 'Semesters'];

	public $hasMany = ['Submissions'];

	/**
	 * An array of possible visibilities.
	 *
	 * @var array
	 */
	protected $_visibilities = [
		0 => 'Hidden',
		1 => 'Visible',
	];

	protected $_meta = [
		'constraints' => [
			[
				'type'   => 'primary',
				'column' => 'id'
			],
			[
				'type'     => 'foreign_key',
				'column'   => 'class_id',
				'toColumn' => 'id',
				'to'       => 'classes',
			],
			[
				'type'     => 'foreign_key',
				'column'   => 'semester_id',
				'toColumn' => 'id',
				'to'       => 'semesters',
			]
		],
		'table' => [
			'charset' => 'utf8',
			'collate' => 'utf8_unicode_ci',
			'engine'  => 'MyISAM'
		]
	];

	protected $_schema = [
		'id' => ['type' => 'id'],
		'class_id' => [
			'type' => 'integer',
			'null' => false
		],
		'semester_id' => [
			'type' => 'integer',
			'null' => false
		],
		'name' => [
			'type' => 'string',
			'length' => 255,
			'null' => false,
		],
		'slug' => [
			'type' => 'string',
			'length' => 255,
			'null' => false,
			'comment' => 'Slug of the name. Added automatically'
		],
		'visibility' => [
			'type' => 'integer',
			'length' => 1,
			'default' => 0,
			'null' => false,
			'comment' => '0 = hidden; 1 = visible;'
		],
		'due' => [
			'type' => 'integer',
			'null' => false
		],
		'created' => ['type' => 'integer', 'null' => false],
		'updated' => ['type' => 'integer', 'null' => false]
	];

	/**
	 * Returns a specific visibility text.
	 *
	 * @see User::$_visibilities
	 * @param lithium\data\Entity $entity
	 * @return array Returns the specific visibility text based on `$entity->visibility`
	 */
	public function visibility(Entity $entity) {
		return $this->_visibilities[$entity->visibility];
	}

	/**
	 * @return array Returns a list of possible visibilities an assignment can have
	 */
	public function visibilities() {
		return $this->_visibilities;
	}
}

/**
 * Filters the save() method in order to create the `slug` based on the `name`.
 */
Assignments::applyFilter('save', function($self, $params, $chain) {
	if ($params['data']) {
		$params['entity']->set($params['data']);
		$params['data'] = [];
	}

	$params['entity']->slug = Inflector::slug($params['entity']->name);
	return $chain->next($self, $params, $chain);
});

?>