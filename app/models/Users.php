<?php

namespace app\models;

use lithium\security\Password;
use lithium\data\Entity;
use lithium\util\Inflector;

use app\extensions\helper\Debug;

class Users extends \app\models\ApplicationModel {

	/**
	 * Let `li3_db` know that this model should be processed even though it extends ApplicationModel.
	 * @var boolean
	 */
	public static $persist = true;

	/**
	 * An array of possible statuses available.
	 *
	 * @var array
	 */
	protected $_statuses = [
		0 => 'Inactive',
		1 => 'Active',
	];

	public $hasMany = ['RolesUsers'];

	protected $_meta = [
		'constraints' => [
			[
				'type'   => 'primary',
				'column' => 'id'
			],
			[
				'type'   => 'unique',
				'column' => 'email',
				'index'  => true,
				'key'    => true,
			],
			[
				'type'   => 'unique',
				'column' => 'slug',
				'index'  => true,
				'key'    => true,
			],
		],
		'table' => [
			'charset' => 'utf8',
			'collate' => 'utf8_unicode_ci',
			'engine'  => 'MyISAM'
		]
	];

	protected $_schema = [
		'id' => ['type' => 'id'],
		'first_name' => [
			'type' => 'string',
			'length' => 50,
			'null' => false,
			'searchable' => 'First Name'
		],
		'last_name' => [
			'type' => 'string',
			'length' => 50,
			'null' => false,
			'searchable' => 'Last Name'
		],
		'slug' => [
			'type' => 'string',
			'length' => 255,
			'null' => false,
			'comment' => 'Slug of the `first_name` and `last_name`. Added automatically'
		],
		'password' => [
			'type' => 'string',
			'length' => 60,
			'null' => false
		],
		'salt' => [
			'type' => 'string',
			'length' => 60,
			'null' => false
		],
		'email' => [
			'type' => 'string',
			'length' => 100,
			'null' => false,
			'searchable' => 'Email'
		],
		'status' => [
			'type' => 'integer',
			'length' => 5,
			'default' => 1,
			'null' => false,
			'comment' => '1 = enabled; 0 = disabled;'
		],
		'timezone' => [
			'type' => 'string',
			'null' => false,
			'default' => 'Europe/Budapest'
		],
		'created' => ['type' => 'integer', 'null' => false],
		'updated' => ['type' => 'integer', 'null' => false],
	];

	public $validates = [
		'email' => [
			'exists' => ['notEmpty'],
			'valid' => ['email'],
			'unique' => [
				'unique',
				'model' => '\app\models\Users'
			]
		],
		'password' => [
			[
				'notEmpty',
				'message'  => 'Please enter a password.',
				'on'       => ['changePassword', 'create']
			],
			[
				'lengthBetween',
				'message'   => 'The password must be at least 6 characters.',
				'min'       => 6,
				'skipEmpty' => true,
				'on'       => ['changePassword', 'create']
			]
		],
		'first_name' => [
			'notEmpty',
			'message' => 'Please provide your first name'],
		'last_name'  => [
			'notEmpty',
			'message' => 'Please provide your last name']
	];

	public static function __init() {
		parent::__init();

		static::applyFilter('save', function($self, $params, $chain) {
			if ($params['data']) {
				$params['entity']->set($params['data']);
				$params['data'] = [];
			}

			if ($params['entity']->password || $params['entity']->modified('password')) {
				$salt = Password::salt();
				$params['entity']->password = Password::hash($params['entity']->password, $salt);
				$params['entity']->salt = $salt;
			}

			$fullName = "{$params['entity']->first_name} {$params['entity']->last_name}";
			$params['entity']->slug = Inflector::slug($fullName);

			return $chain->next($self, $params, $chain);
		});
	}

	/**
	 * Overriding Model::title() in order to get a custom title for use in select lists.
	 */
	public function title($entity) {
		return "{$entity->first_name} {$entity->last_name}";
	}

	/**
	 * Returns a specific status text.
	 *
	 * @see User::$_statuses
	 * @param lithium\data\Entity $entity
	 * @return array Returns the specific status text based on `$entity->status`
	 */
	public function status(Entity $entity) {
		return $this->_statuses[$entity->status];
	}

	/**
	 * @return array Returns a list of possible statuses a user can have
	 */
	public function statuses() {
		return $this->_statuses;
	}

	/**
	 * Iterates through the schema for the model and excludes any fields that should not be displayed.
	 *
	 * @param  array $exclude An array of fields to be excluded.
	 * @return array Returns an array of fields safe for display.
	 */
	public static function safeFields(array $exclude = [], $model = 'Users') {
		$defaults = ['salt', 'password'];
		$exclude += $defaults;
		$fields = [];
		foreach (Users::schema()->names() as $fieldVal) {
			if (!in_array($fieldVal, $exclude)) {
				$fields[] = "$model.{$fieldVal}";
			}
		}
		return $fields;
	}
}
?>