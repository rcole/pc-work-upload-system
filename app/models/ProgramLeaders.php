<?php

namespace app\models;

use app\extensions\helper\Debug;

/**
 * A Program Leader can be in charge of many `Programs` which they create and maintain.
 */
class ProgramLeaders extends \app\extensions\data\Model {

	protected $_meta = [
		'source' => 'users',
	];

	/**
	 * Let `li3_db` know that this model should not be processed since it uses the users table.
	 * @var boolean
	 */
	public static $persist = false;

	/**
	 * For RolesUsers, we specify that the key used for the match should be `user_id` and not
	 * `program_leader_id` since we are actually querying the `users` table.
	 */
	public $hasMany = ['ProgramAuthorizations', 'RolesUsers' => [
		'key' => ['id' => 'user_id']
	]];

	public $validates = [];

	/**
	 * Filtering the finder to only return records whose Roles.id = 2. In other words, all records
	 * returned will be those of Program Leaders.
	 */
	public static function __init() {
		parent::__init();

		static::applyFilter('find', function($self, $params, $chain) {
			$params['options']['conditions']['Roles.id'] = 2;
			$params['options']['with'][] = 'RolesUsers.Roles';
			return $chain->next($self, $params, $chain);
		});
	}

	/**
	 * Overriding Model::title() in order to get a custom title for use in select lists.
	 */
	public function title($entity) {
		return "{$entity->first_name} {$entity->last_name}";
	}
}

?>