<?php

namespace app\models;

use app\extensions\helper\Debug;

class ExtensionCodes extends \app\extensions\data\Model {

	protected $_statuses = [
		0 => 'Unused',
		1 => 'Used',
		10 => 'Expired',
		20 => 'Canceled'
	];

	public $belongsTo = ['Submissions', 'Users', 'Assignments', 'Students'];

	protected $_meta = [
		'constraints' => [
			[
				'type'   => 'primary',
				'column' => 'id'
			],
			[
				'type'     => 'foreign_key',
				'column'   => 'submission_id',
				'toColumn' => 'id',
				'to'       => 'submissions'
			],
			[
				'type'     => 'foreign_key',
				'column'   => 'user_id',
				'toColumn' => 'id',
				'to'       => 'users'
			],
			[
				'type'     => 'foreign_key',
				'column'   => 'assignment_id',
				'toColumn' => 'id',
				'to'       => 'assignments'
			],
			[
				'type'     => 'foreign_key',
				'column'   => 'student_id',
				'toColumn' => 'id',
				'to'       => 'users',
			],
			[
				'type'   => 'unique',
				'column' => 'title',
				'index'  => true,
				'key'    => true
			]
		],
		'table' => [
			'charset' => 'utf8',
			'collate' => 'utf8_unicode_ci',
			'engine'  => 'MyISAM'
		]
	];

	protected $_schema = [
		'id' => ['type' => 'id'],
		'submission_id' => [
			'type' => 'integer',
			'null' => false
		],
		'user_id' => [
			'type' => 'integer',
			'null' => false
		],
		'assignment_id' => [
			'type' => 'integer',
			'null' => false
		],
		'student_id' => [
			'type' => 'integer',
			'null' => false
		],
		'title' => [
			'type' => 'string',
			'length' => 7,
			'null' => false
		],
		'status' => [
			'type' => 'string',
			'length' => 100,
			'null' => false,
			'default' => 0,
			'comment' => '0 = Unused; 1 = Used; 10 = Expired; 20 = Canceled'
		],
		'expires_date' => [
			'type' => 'integer',
			'null' => false
		],
		'created' => ['type' => 'integer', 'null' => false],
		'updated' => ['type' => 'integer', 'null' => false]
	];

	public $validates = [];

	/**
	 * Returns a specific status text.
	 *
	 * @see ExtensionCodes::$_statuses
	 * @param  $entity
	 * @return array Returns the specific status text based on `$entity->status`
	 */
	public function status($status) {
		return $this->_statuses[$status];
	}

	/**
	 * @return array Returns a list of possible statuses an Extension Code can have
	 */
	public function statuses() {
		return $this->_statuses;
	}
}

/**
 * Generates 8 unique and random characters on creation for the `title`.
 */
ExtensionCodes::applyFilter('save', function($self, $params, $chain) {
	if ($params['data']) {
		$params['entity']->set($params['data']);
		$params['data'] = [];
	}

	if (!$params['entity']->exists()) {
		$params['entity']->title = strtoupper(substr(md5(uniqid(mt_rand(), true)), 0, 8));
	}
	return $chain->next($self, $params, $chain);
});

?>