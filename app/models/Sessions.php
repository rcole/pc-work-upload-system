<?php

namespace app\models;

use lithium\storage\Session;

use app\extensions\helper\Debug;

class Sessions extends \app\extensions\data\Model {

	protected $_meta = [
		'connection' => false,
		'source' => null
	];

	protected static $_name = 'default';

	/**
	 * Writes the necessary session variables.
	 *
	 * If a user has multiple roles, all role information will be written.
	 * The first `label` and `home` key will be the default role of that user and is defined in
	 * RolesUsers.default.
	 *
	 * @return bool  True if sessions were set, else false.
	 */
	public static function initiate(array $data) {
		$defaults = [
			'user' => null,
		];
		$data += $defaults;
		$fields = [];

		$usersRoles = $data['user']->roles_users;
		foreach ($usersRoles->data() as $role) {
			$fields['home'][] = $role['role']['home'];
			$fields['label'][] = $role['role']['label'];
			$fields['rank'][] = $role['role']['rank'];
		}

		$session = (array) Session::read('default', ['name' => static::$_name]);
		$session += (array) $fields;

		return Session::write('default', $session);
	}

	public static function clear($keys = []) {
		foreach (Session::config() as $key => $val) {
			$default[] = $key;
		}
		(array) $keys += $default;

		foreach ($keys as $key) {
			Session::clear(['name' => $key]);
		}
	}

	/**
	 * Returns Roles.label of user.
	 *
	 * If the user has many roles and if `$highest` is `true` (default), it will return the role
	 * with the lowest integer value for Roles.rank.
	 * Otherwise, all labels will be returned.
	 *
	 * @param  boolean $highest True if you want to return the highest ranking role.
	 * @return string           The label or labels of the user.
	 */
	public static function label($highest = true) {
		$labels = (array) Session::read('default.label', [
			'name' => static::$_name
		]);

		if (empty($labels)) {
			$labels = (array) Session::read('default')['label'];
		}
		if ($highest) {
			$ranks = (array) Session::read('default.rank', [
				'name' => static::$_name
			]);
			if (empty($ranks)) {
				$ranks = (array) Session::read('default')['rank'];
			}
			$roles = array_combine($ranks, $labels);
			ksort($roles);
			return reset($roles);
		}

		return $labels;
	}

	public static function home($default = true) {
		$homes = (array) Session::read('default.home', [
			'name' => static::$_name
		]);

		if (empty($homes)) {
			$homes = Session::read('default')['home'];
		}

		if ($default) {
			return reset($homes);
		}

		return $homes;
	}

	public static function fullName() {
		$firstName = Session::read('default.first_name', [
			'name' => static::$_name
		]);
		if (empty($firstName)) {
			$firstName = Session::read('default')['first_name'];
		}

		$lastName = Session::read('default.last_name', [
			'name' => static::$_name
		]);
		if (empty($lastName)) {
			$lastName = Session::read('default')['last_name'];
		}

		return "$firstName $lastName";
	}

	public static function id() {
		$id = Session::read('default.id', [
			'name' => static::$_name
		]);
		if (empty($id)) {
			$id = Session::read('default')['id'];
		}
		return $id;
	}

	public static function timezone() {
		$timezone = Session::read('default.timezone', [
			'name' => static::$_name
		]);
		if (empty($timezone)) {
			$timezone = Session::read('default')['timezone'];
		}
		return $timezone;
	}
}

?>
