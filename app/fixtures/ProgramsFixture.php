<?php

namespace app\fixtures;

use app\extensions\helper\Debug;

class ProgramsFixture extends \app\extensions\data\Fixture {

	protected static $_model = 'app\models\Programs';

	protected static $_fixtures = [
		[
			'id' => 1,
			'title' => 'Graphic Design'
		],
		[
			'id' => 2,
			'title' => 'Interactive Media'
		],
		[
			'id' => 3,
			'title' => 'Computing'
		]
	];
}

?>