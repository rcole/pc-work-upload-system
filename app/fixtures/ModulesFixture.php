<?php

namespace app\fixtures;

use app\extensions\helper\Debug;

class ModulesFixture extends \app\extensions\data\Fixture {

	protected static $_model = 'app\models\Modules';

	protected static $_fixtures = [
		[
			'id' => 1,
			'program_id' => 1,
			'title' => 'Design Principals',
			'short_name' => 'DP',
			'unit_number' => 'U54'
		],

		[
			'id' => 2,
			'program_id' => 1,
			'title' => 'Interactive Media Design and Prototyping',
			'short_name' => 'IMDP',
			'unit_number' => 'U24'
		],

		[
			'id' => 3,
			'program_id' => 2,
			'title' => 'Typographic Skills',
			'short_name' => 'TS',
			'unit_number' => 'U32'
		],

		[
			'id' => 4,
			'program_id' => 2,
			'title' => 'Interactive Media Web Authoring',
			'short_name' => 'IMWA',
			'unit_number' => 'U87'
		],

		[
			'id' => 5,
			'program_id' => 3,
			'title' => 'Human User Interfaces',
			'short_name' => 'HUI',
			'unit_number' => 'U65'
		],
		[
			'id' => 6,
			'program_id' => 3,
			'title' => 'Introduction to Object-Oriented Programming',
			'short_name' => 'IOOP',
			'unit_number' => 'U12'
		]
	];
}

?>