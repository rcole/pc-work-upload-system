<?php

namespace app\fixtures;

use app\extensions\helper\Debug;

class AssignmentsFixture extends \app\extensions\data\Fixture {

	protected static $_model = 'app\models\Assignments';

	protected static $_fixtures = [
		[
			'id' => 1,
			'class_id' => 5,
			'semester_id' => 4,
			'name' => 'Semesters first assignment',
			'visibility' => 1,
			'due' => 1393632000
		],
		[
			'id' => 2,
			'class_id' => 4,
			'semester_id' => 4,
			'name' => 'Semesters second assignment',
			'visibility' => 1,
			'due' => 1393632000
		],
	];
}

?>