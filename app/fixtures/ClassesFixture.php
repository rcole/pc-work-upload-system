<?php

namespace app\fixtures;

use app\extensions\helper\Debug;

class ClassesFixture extends \app\extensions\data\Fixture {

	protected static $_model = 'app\models\Classes';

	protected static $_fixtures = [
		[
			'id' => 1,
			'module_id' => 1,
			'title' => 'Intro to Rasters',
			'begins' => 1404259200,
			'ends' => 1412035200
		],
		[
			'id' => 2,
			'module_id' => 1,
			'title' => 'Tracing Rasters',
			'begins' => 1412208000,
			'ends' => 1419984000
		],
		[
			'id' => 3,
			'module_id' => 1,
			'title' => 'Intro to Pottery',
			'begins' => 1412208000,
			'ends' => 1419984000
		],
		[
			'id' => 4,
			'module_id' => 1,
			'title' => 'Photoshop 101',
			'begins' => 1412208000,
			'ends' => 1419984000
		],
		[
			'id' => 5,
			'module_id' => 2,
			'title' => 'Intro to Vectors',
			'begins' => 1412208000,
			'ends' => 1419984000
		],
	];
}

?>