<?php

namespace app\fixtures;

use app\extensions\helper\Debug;

class UsersFixture extends \app\extensions\data\Fixture {

	protected static $_model = 'app\models\Users';

	protected static $_fixtures = [
		// Admins
		[
			'id' => 1,
			'first_name' => 'Housni',
			'last_name' => 'Yakoob',
			'password' => 'housni@koobi.co',
			'email' => 'housni@koobi.co',
			'status' => 1,
			'timezone' => 'Asia/Colombo',
			'created' => 0,
			'updated' => 0
		],
		[
			'id' => 2,
			'first_name' => 'Ryan',
			'last_name' => 'Cole',
			'password' => 'ryan@koobi.co',
			'email' => 'ryan@koobi.co',
			'status' => 1,
			'timezone' => 'Europe/Budapest',
			'created' => 0,
			'updated' => 0
		]	
	];
}

?>