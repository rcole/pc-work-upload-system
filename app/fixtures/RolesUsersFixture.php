<?php

namespace app\fixtures;

use app\extensions\helper\Debug;

class RolesUsersFixture extends \app\extensions\data\Fixture {

	protected static $_model = 'app\models\RolesUsers';

	protected static $_fixtures = [
		// Housni is admin
		[
			'role_id' => 1,
			'user_id' => 1,
			'default' => true
		],
		// Housni is Teacher (default role)
		[
			'role_id' => 3,
			'user_id' => 1
		],

		// Ryan is admin
		[
			'role_id' => 1,
			'user_id' => 2
		],
		// Ryan is Teacher (default role)
		[
			'role_id' => 3,
			'user_id' => 2
		]
	];
}

?>