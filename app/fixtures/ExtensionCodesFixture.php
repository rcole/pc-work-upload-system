<?php

namespace app\fixtures;

use app\extensions\helper\Debug;

class ExtensionCodesFixture extends \app\extensions\data\Fixture {

	protected static $_model = 'app\models\ExtensionCodes';

	protected static $_fixtures = [
		[
			'id' => 1,
			'submission_id' => 1,
			'title' => '13AB76B',
			'assignment_id' => 2,
			'user_id' => 60,
			'student_id' => 71,
			'expires_date' => 1413802760,
			'status' => 'claimed'
		],
		[
			'id' => 2,
			'submission_id' => 2,
			'title' => '5361EED',
			'assignment_id' => 2,
			'user_id' => 60,
			'student_id' => 70,
			'expires_date' => 1413802760,
			'status' => 'expired'
		],
		[
			'id' => 3,
			'submission_id' => 3,
			'title' => '555C5A2',
			'user_id' => 60,
			'student_id' => 73,
			'expires_date' => 1413802760,
			'status' => 'unclaimed'
		],
		[
			'id' => 4,
			'submission_id' => 4,
			'title' => '20B856D',
			'user_id' => 60,
			'student_id' => 72,
			'expires_date' => 1413802760,
			'status' => 'unclaimed'
		]
	];
}



?>