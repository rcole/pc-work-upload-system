<?php

namespace app\fixtures;

use app\extensions\helper\Debug;

class SemestersFixture extends \app\extensions\data\Fixture {

	protected static $_model = 'app\models\Semesters';

	protected static $_fixtures = [
		/**
		 * 2014, Jan 1st - 2014, Mar 31st
		 */
		[
			'id' => 1,
			'title' => 'Spring',
			'year' => 2014,
			'semester' => 1,
			'begins' => 1388534400,
			'ends' => 1396224000
		],

		/**
		 * 2014, Apr 2nd - 2014, Jun 30th
		 */
		[
			'id' => 2,
			'title' => 'Summer',
			'year' => 2014,
			'semester' => 2,
			'begins' => 1396396800,
			'ends' => 1404086400
		],

		/**a
		 * 2014, Jul 2nd - 2014, Sep 30th
		 */
		[
			'id' => 3,
			'title' => 'Fall',
			'year' => 2014,
			'semester' => 3,
			'begins' => 1404259200,
			'ends' => 1412035200
		],

		/**
		 * 2014, Oct 2nd - 2014, Dec 31st
		 */
		[
			'id' => 4,
			'title' => 'Winter',
			'year' => 2014,
			'semester' => 4,
			'begins' => 1412208000,
			'ends' => 1419984000
		]
	];
}

?>