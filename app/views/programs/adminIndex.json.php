<?php
	use app\models\Sylos;
	use app\extensions\helper\Debug;

	$programsData = $programs->to('array', ['indexed' => false]);
	$error = array_pop($programsData);

	$data = [
		'programs' => $programsData,
		'studentCount' => $studentCount,
		'teacherCount' => $teacherCount,
		'semesterClassesCount' => $semesterClassesCount,
		'modulesCount' => $modulesCount,
		'expectedSubmissions' => $expectedSubmissions,
		'submissionCount' => $submissionCount,
		'_error' => array_pop($error)
	];
	echo json_encode($data, JSON_PRETTY_PRINT);
?>