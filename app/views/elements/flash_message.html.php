<?php
/**
 * li3_flash_message plugin for Lithium: the most rad php framework.
 *
 * @copyright     Copyright 2010, Michael Hüneburg
 * @license       http://opensource.org/licenses/bsd-license.php The BSD License
 */

/**
 * Copy this file to `app/views/elements` to customize the output.
 */
?>
<div class="row">
    <div class="span4 offset4">
        <div class="alert alert-block alert-<?= $class; ?>">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php echo $message; ?>
        </div>
    </div>
</div>