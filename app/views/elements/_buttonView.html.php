<?= $this->html->link(
    'view',
    [$action, 'id' => $id],
    [
        'class' => 'btn view',
        'title' => $title, 'escape' => false
]); ?>