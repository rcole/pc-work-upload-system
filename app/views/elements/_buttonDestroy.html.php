<?= $this->html->link(
    'delete',
    '#delete-' . $id,
    [
        'class' => 'btn destroy',
        'title'       => $title,
        'escape'      => false,
        'data-toggle' => 'modal',
        'data-target' => '#delete-' . $id
]); ?>