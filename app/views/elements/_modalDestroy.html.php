<div class="modal hide fade" id="delete-<?= $id;?>">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?= $t('Confirm Deletion'); ?></h3>
    </div>
    <div class="modal-body">
        <p><?php echo $t('Are you sure you want to delete {:title} ?', ['title' => "<strong>$title</strong>"]); ?></p>
        <p><?= $t('This action can not be undone.'); ?></p>
    </div>
    <div class="modal-footer">
        <a href="#cancel" class="btn" data-dismiss="modal"><?= $t('Cancel'); ?></a>
        <?= $this->form->create(null, [
            'url'    => "/$role/$action/$id",
/*
            'url'    => [
                $action,
                'id' => $id
            ],
*/
            'method' => 'delete'
        ]); ?>
        <?= $this->form->submit($t('Confirm Deletion'), ['class' => 'btn btn-danger']); ?>
        <?= $this->form->end(); ?>
    </div>
</div>