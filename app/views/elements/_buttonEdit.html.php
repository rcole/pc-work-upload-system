<?= $this->html->link(
    'edit',
    [$action, 'id' => $id],
    [
        'class' => 'btn edit',
        'title'  => $title,
        'escape' => false
]); ?>