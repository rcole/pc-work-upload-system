<?php
/**
 * Lithium: the most rad php framework
 *
 * @copyright     Copyright 2013, Union of RAD (http://union-of-rad.org)
 * @license       http://opensource.org/licenses/bsd-license.php The BSD License
 */
?>
<!DOCTYPE html>

<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]> 	   <html class="no-js"> <![endif]-->

<html lang="en" ng-app="programleaderApp" ng-cloak>
<head>
	<title>Sylo</title>
	<?php echo $this->html->charset();?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!--[if IE]>
  		<?php echo $this->html->style(['ie.css']); ?>
  <![endif]-->

  	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
	<?php echo $this->html->style(['screen.css', 'print.css']); ?>

	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?= $this->path('img/apple-touch-icon-144x144-precomposed.png'); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?= $this->path('img/apple-touch-icon-114x114-precomposed.png'); ?>">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?= $this->path('img/apple-touch-icon-72x72-precomposed.png'); ?>">
	<link rel="apple-touch-icon-precomposed" href="<?= $this->path('apple-touch-icon-57x57-precomposed.png'); ?>">
	<?php echo $this->html->link('Icon', null, ['type' => 'icon']); ?>
	<base href="<?= $this->request()->env('base'); ?>/">
	<!-- heap tracking code -->
	<script type="text/javascript">
		window.heap=window.heap||[],heap.load=function(t,e){window.heap.appid=t,window.heap.config=e;var a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=("https:"===document.location.protocol?"https:":"http:")+"//cdn.heapanalytics.com/js/heap.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n);for(var o=function(t){return function(){heap.push([t].concat(Array.prototype.slice.call(arguments,0)))}},p=["clearEventProperties","identify","setEventProperties","track","unsetEventProperty"],c=0;c<p.length;c++)heap[p[c]]=o(p[c])};
		heap.load("912591352");
	</script>
</head>
<body class="admin_default" ng-controller="globalsController as globals">
	<div mc-messages></div>
    <!--[if lt IE 7]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
    <![endif]-->
		<section id="topbar" ng-include="'app/webroot/tpl/topbar.tpl.html'"></section>
		<nav id="sidebar" ng-include="'app/webroot/tpl/adminSidebar.tpl.html'"></nav>
		<section class="pageWidth content" ui-view></section>

	<!-- this is the dev set of scripts to include -->
    <?php echo $this->html->script([
    	'angular.js',
    	'programLeader.js',
    	'libs.js'
    ]); ?>

	<!-- this would be the production js 
 	<script type="text/javascript" src="/js/programLeader.min.js"></script>
 	-->
</body>
</html>