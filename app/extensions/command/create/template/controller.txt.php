namespace {:namespace};

use {:use};
use lithium\g11n\Message;

use app\extensions\helper\Debug;

class {:class} extends \app\extensions\action\Controller {

	/**
	 * [ short description of {:prefix}Destroy action ]
	 *
	 * [ long description of {:prefix}Destroy action ]
	 *
	 * @todo   Incomplete
	 * @url    DELETE /{:plural}/:id
	 */
	public function {:prefix}Destroy() {
		extract(Message::aliases());
		${:singular} = {:model}::find($this->request->id);

		if (${:singular}->delete()) {
			$this->flashSuccess($t('The {:singular} was deleted.'));
			return $this->redirect(['{:name}::index']);
		}

		$this->flashError($t('The {:singular} could not be deleted.'));
		return $this->redirect($this->request->referer());
	}

	/**
	 * [ short description of {:prefix}Edit action ]
	 *
	 * [ long description of {:prefix}Edit action ]
	 *
	 * @todo   Incomplete
	 * @url    GET /{:plural}/:id/edit
	 * @url    PUT /{:plural}/:id/edit
	 */
	public function {:prefix}Edit() {
		${:singular} = {:model}::find($this->request->id);
		if ($this->request->is('put')) {
			extract(Message::aliases());

			if (${:singular}->save($this->request->data)) {
				$this->flashSuccess($t('The {:singular} was updated.'));
				return $this->redirect([
					'{:name}::view',
					'id' => ${:singular}->id
				]);
			}
			$this->flashError($t('The {:singular} could not be updated.'));
		}
		return compact('{:singular}');
	}

	/**
	 * [ short description of {:prefix}Index action ]
	 *
	 * [ long description of {:prefix}Index action ]
	 *
	 * @todo   Incomplete
	 * @url    GET /{:plural}
	 * @url    GET /{:plural}/index
	 */
	public function {:prefix}Index() {
		$limit = 10;
		$page  = $this->request->page ?: 1;

		${:plural} = {:model}::all([
			'limit' => $limit,
			'page'  => $page,
			'order' => ['id' => 'ASC']
		]);

		$total = {:model}::find('count');
		return compact('{:plural}', 'total', 'limit', 'page');
	}

	/**
	 * [ short description of {:prefix}Create action ]
	 *
	 * [ long description of {:prefix}Create action ]
	 *
	 * @todo   Incomplete
	 * @url    GET  /{:plural}/create
	 * @url    POST /{:plural}/create
	 */
	public function {:prefix}Create() {
		${:singular} = {:model}::create();

		if ($this->request->is('post')) {
			extract(Message::aliases());

			if (${:singular}->save($this->request->data)) {
				$this->flashSuccess($t('The {:singular} was added.'));
				return $this->redirect([
					'{:name}::view',
					'id' => ${:singular}->id
				]);
			}
			$this->flashError($t('The {:singular} could not be added.'));
		}

		return compact('{:singular}');
	}

	/**
	 * Allows users with role `{:prefix}` to search within this model.
	 *
	 * @todo   Incomplete
	 * @see    app\models\{:model}
	 * @url    GET /{:plural}/search/q=<search term>&<field_1>=&<field_2>...
	 */
	public function {:prefix}Search() {
		return $this->search();
	}

	/**
	 * [ short description of {:prefix}View action ]
	 *
	 * [ long description of {:prefix}View action ]
	 *
	 * @todo   Incomplete
	 * @url GET /{:plural}/1
	 */
	public function {:prefix}View() {
		${:singular} = {:model}::find($this->request->id);
		return compact('{:singular}');
	}
}