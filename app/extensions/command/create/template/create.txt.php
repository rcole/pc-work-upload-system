<?php $this->title('Create {:title}'); ?>

<div>
	<div>
		<h1><?= 'Create {:title}' ?></h1>
	</div>
	<div>
		<?= $this->form->create(${:singular}, [
			'url' => ['{:name}::create'],
			'method' => 'post'
		]); ?>
			<fieldset>
				<legend><?= '{:title} Details' ?></legend>
				{:form}
				<div>
					<?= $this->form->submit('Save') ?>
					<input type="reset" value="Reset">
				</div>
			</fieldset>
		<?= $this->form->end(); ?>
	</div>
</div>