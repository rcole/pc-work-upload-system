<?php

namespace app\extensions\command\create;

use lithium\util\Inflector;
use lithium\core\Libraries;

/**
 * `li3 create view --template=index Users administratorIndex.html`
 */
class View extends \lithium\console\command\create\View {

	/**
	 * @param string $request
	 * @return string
	 */
	protected function _head($request) {
		$action = $request->params['action'];
		$model = Libraries::instance('models', $action);
		$names = $model::schema()->names();

		foreach ($names as $name) {
			$head[] = '<td>' . Inflector::humanize($name) . "</td>\r\n";
		}
		return implode("\t\t\t", $head);
	}

	protected function _title($request) {
		return Inflector::humanize($this->_singular($request));
	}

	/**
	 * @param string $request
	 * @return string
	 */
	protected function _data($request) {
		$action = $request->params['action'];
		$model = Libraries::instance('models', $action);
		$names = $model::schema()->names();

		foreach ($names as $name) {
			$body[] = "<td><?= \${$this->_singular($request)}->$name ?></td>\r\n";
		}
		return implode("\t\t\t\t", $body);
	}

	/**
	 * @param string $request
	 * @return string
	 */
	protected function _datum($request) {
		$action = $request->params['action'];
		$model = Libraries::instance('models', $action);
		$names = $model::schema()->names();
		$data = '';

		foreach ($names as $name) {
			$data .= '
			<tr>
				<th>' . Inflector::humanize($name) . "</th>
				<td><?= \${$this->_singular($request)}->$name ?></td>
			</tr>";
		}
		return $data;
	}

	protected function _form($request) {
		$action = $request->params['action'];
		$model = Libraries::instance('models', $action);
		$names = $model::schema()->names();
		$data = '';

		foreach ($names as $name) {
			$data .= "
				<?= \$this->form->field('$name'); ?>";
		}
		return $data;
	}
}

?>