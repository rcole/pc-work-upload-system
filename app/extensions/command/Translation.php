<?php
namespace app\extensions\command;

use lithium\g11n\Catalog;
use lithium\core\Environment;



/**
 * @see http://mackstar.com/blog/2011/12/31/practical-internationalization-lithium
 */
class Translation extends \lithium\console\Command {

	protected $_languages = [];

	protected function _init() {
		parent::_init();
		$this->_languages = array_keys(Environment::get('development')['locales']);
	}

	/**
	 * Extract all translated strings from the codebase
	 */
	public function extract() {
		$data = Catalog::read('code', 'messageTemplate', 'root', array(
			'lossy' => false
		));
		$scope = 'default';
		return Catalog::write('default', 'messageTemplate', 'root', $data, compact('scope'));
	}

	/**
	 * Generate a language files for each translation
	 */
	public function create() {
		$app = LITHIUM_APP_PATH;
		// msginit -l <locale> -i message_default.pot -o <locale>/LC_MESSAGES/default.po --no-translator
		foreach ($this->_languages as $locale) {
			$dir = "{$app}/resources/g11n/{$locale}/LC_MESSAGES";
			if (!file_exists($dir)) {
				passthru("mkdir -p {$dir} > /dev/null 2>&1");
			}

			$default = "{$dir}/default.po";
			if (!file_exists($default)) {
				passthru("touch {$app}/resources/g11n/{$locale}/LC_MESSAGES/default.po > /dev/null 2>&1");
			}

			$command = "msgmerge -U --backup=off {$app}/resources/g11n/{$locale}/LC_MESSAGES/default.po ";
			$command .= "{$app}/resources/g11n/message_default.pot ";
			passthru($command);
		}
	}

	/**
	 * Compile a binary for each translation file
	 */
	public function compile() {
		$app = LITHIUM_APP_PATH;
		foreach ($this->_languages as $locale) {
			$command  = "msgfmt -D {$app}/resources/g11n/{$locale}/LC_MESSAGES ";
			$command .= "-o {$app}/resources/g11n/{$locale}/LC_MESSAGES/default.pot default.po";
			passthru($command);
		}
	}
}
?>