<?php

namespace app\extensions\util\format;

use app\models\Sessions;

class Date extends \app\extensions\util\Format {

	/**
     * Converts a Unix timestamp to a human readable date specified by $format
     * and in compliance with $timezone.
     * 
	 * @todo    Possibly make this method filterable so that grabbing the
	 *          timezone via session isn't hardcoded. That should probably be
	 *          specified in a `config/bootstrap/*` file.
	 *
	 * @todo    Specifying the default `$format` should also probably be done
	 *          via a config file.
     * @see http://www.php.net/manual/en/function.date.php   
     * @see http://www.php.net/manual/en/timezones.php
     * @param  integer  $timestamp The Unix timestamp to be converted
     * @param  string  $format A format recognized by PHP
     * @param  string  $timezone  A timezone recognized by PHP
     * @return  string  A huamn readable version of $timestamp
	 */
    public static function unixToHumanDate($timestamp, $format = 'M dS, Y @ g:i a', $timezone = null) {
        if (null === $timezone) {
            $timezone = Sessions::timezone();
        }

        $datetime = new \DateTime();
        $datetime->setTimestamp($timestamp);
        $datetime->setTimezone(new \DateTimeZone($timezone));

        return $datetime->format($format);
    }

    /**
     * Converts a human readable date to a Unix timestamp.
     * 
     * @see http://www.php.net/manual/en/timezones.php
     * @param  integer  $date The date to be converted
     * @param  string  $timezone  A timezone recognized by PHP
     * @return integer  Unix timestamp
     */
    public static function humanToUnixDate($date) {
        $timestamp = strtotime($date);

        $datetime = new \DateTime();
        $datetime->setTimestamp($timestamp);

        return $datetime->format('U');
    }
}

?>