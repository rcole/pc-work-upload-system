<?php
namespace app\tests\integration\net\http;

use lithium\action\Request;
use lithium\net\http\Router;
use lithium\core\Libraries;
use app\fixtures\RolesFixture;
use app\tests\mocks\data\MockRoles;
use app\fixtures\UsersFixture;
use app\tests\mocks\data\MockUsers;
use app\fixtures\RolesUsersFixture;
use app\tests\mocks\data\MockRolesUsers;
use lithium\analysis\Debugger;

class RouterTest extends \lithium\test\Integration {

	// We must include routes.php in order to test the defined routes.
	public function setUp() {
		MockRoles::remove();
		foreach (RolesFixture::load() as $rolesData) {
			$roles = MockRoles::create();
			$roles->save($rolesData);
		}

		MockUsers::remove();
		foreach (UsersFixture::load() as $usersData) {
			$users = MockUsers::create();
			$users->save($usersData);
		}

		MockRolesUsers::remove();
		foreach (RolesUsersFixture::load() as $rolesUsersData) {
			$rolesUsers = MockRolesUsers::create();
			$rolesUsers->save($rolesUsersData);
		}

		$routes = Libraries::get(true, 'path') . '/config/routes.php';
		require $routes;
	}

	public function tearDown() {
		Router::reset();
		MockRolesUsers::remove();
		MockUsers::remove();
		MockRoles::remove();
	}

	// GET  /login  Sessions::create
	public function testLoginRoute() {
		$url = '/login';
		Router::connect($url,  ['Sessions::create']);
		$expected = ['controller' => 'Sessions', 'action' => 'create'];
		$request = new Request(['url' => $url]);
		$result = Router::process($request);
		$this->assertEqual($expected, $result->params);
		$this->assertTrue($request->is('get'));
		$this->assertFalse($request->is('put'));
		$this->assertFalse($request->is('post'));
		$this->assertFalse($request->is('delete'));
	}

	// GET  /logout  Sessions::destroy
	public function testLogoutRoute() {
		$url = '/logout';
		Router::connect($url,  ['Sessions::destroy']);
		$expected = ['controller' => 'Sessions', 'action' => 'destroy'];
		$request = new Request(['url' => $url]);
		$result = Router::process($request);
		$this->assertEqual($expected, $result->params);
		$this->assertTrue($request->is('get'));
		$this->assertFalse($request->is('put'));
		$this->assertFalse($request->is('post'));
		$this->assertFalse($request->is('delete'));
	}

	/**
	 * POST    /extensioncodes    ExtensionCodes::create    [1.3]
	 * POST    /programs    Programs::create    [1.1]
	 * POST    /semesters    Semesters::create    [1.2]
	 * POST    /users    Users::create    [1.4]
	 */
	public function testCreatePostRoute() {
		$controllers = [
			'ExtensionCodes' => 'extensioncodes',
			'Programs' => 'programs',
			'Semesters' => 'semesters',
			'Users' => 'users'
		];

		foreach ($controllers as $controller => $uri) {
			$expected = ['controller' => $controller, 'action' => 'create'];
			$request = new Request([
				'url' => "/{$uri}",
				'env' => [
					'REQUEST_METHOD' => 'POST'
				]
			]);
			$result = Router::process($request);
			$this->assertEqual($expected, $result->params);
			$this->assertFalse($request->is('get'));
			$this->assertFalse($request->is('put'));
			$this->assertTrue($request->is('post'));
			$this->assertFalse($request->is('delete'));
		}
	}

	/**
	 * GET    /extensioncodes    ExtensionCodes::index    [1.3]
	 * GET    /extensioncodes/index    ExtensionCodes::index    [1.3]
	 * GET    /programs    Programs::index    [1.1]
	 * GET    /programs/index    Programs::index    [1.1]
	 * GET    /semesters    Semesters::index    [1.2]
	 * GET    /semesters/index    Semesters::index    [1.2]
	 * GET    /users    Users::index    [1.4]
	 * GET    /users/index    Users::index    [1.4]
	 */
	public function testIndexGetRoute() {
		$controllers = [
			'ExtensionCodes' => 'extensioncodes',
			'Programs' => 'programs',
			'Semesters' => 'semesters',
			'Users' => 'users'
		];

		foreach ($controllers as $controller => $uri) {
			$expected = ['controller' => $controller, 'action' => 'index'];
			$request = new Request([
				'url' => "/{$uri}",
				'env' => [
					'REQUEST_METHOD' => 'GET'
				]
			]);
			$result = Router::process($request);
			$this->assertEqual($expected, $result->params);
			$this->assertTrue($request->is('get'));
			$this->assertFalse($request->is('put'));
			$this->assertFalse($request->is('post'));
			$this->assertFalse($request->is('delete'));

			$expected = ['controller' => $controller, 'action' => 'index'];
			$request = new Request([
				'url' => "/{$uri}/index",
				'env' => [
					'REQUEST_METHOD' => 'GET'
				]
			]);
			$result = Router::process($request);
			$this->assertEqual($expected, $result->params);
			$this->assertTrue($request->is('get'));
			$this->assertFalse($request->is('put'));
			$this->assertFalse($request->is('post'));
			$this->assertFalse($request->is('delete'));
		}
	}

	/**
	 * POST    /program/:program/modules    Programs::create    [1.1.4]
	 */
	public function testProgramsModulesPostRoute() {
		$expected = ['controller' => 'Programs', 'action' => 'create', 'program' => 'Graphic-Design'];
		$request = new Request([
			'url' => "/program/Graphic-Design/modules",
			'env' => [
				'REQUEST_METHOD' => 'POST'
			]
		]);
		$result = Router::process($request);
		$this->assertEqual($expected, $result->params);
		$this->assertFalse($request->is('get'));
		$this->assertFalse($request->is('put'));
		$this->assertTrue($request->is('post'));
		$this->assertFalse($request->is('delete'));
	}

	/**
	 * GET    /program/:program/modules    Programs::index    [1.1.4]
	 */
	public function testProgramsModulesGetRoute() {
		$expected = ['controller' => 'Programs', 'action' => 'index', 'program' => 'Graphic-Design'];
		$request = new Request([
			'url' => "/program/Graphic-Design/modules",
			'env' => [
				'REQUEST_METHOD' => 'GET'
			]
		]);
		$result = Router::process($request);
		$this->assertEqual($expected, $result->params);
		$this->assertTrue($request->is('get'));
		$this->assertFalse($request->is('put'));
		$this->assertFalse($request->is('post'));
		$this->assertFalse($request->is('delete'));
	}

	/**
	 * POST    /semester/:semester/program/:program/classes    Classes::create    [1.1.1]
	 */
	public function testSemesterProgramClassesPostRoute() {
		$expected = ['controller' => 'Classes', 'action' => 'create', 'semester' => '1401', 'program' => 'Graphic-Design'];
		$request = new Request([
			'url' => "/semester/1401/program/Graphic-Design/classes",
			'env' => [
				'REQUEST_METHOD' => 'POST'
			]
		]);
		$result = Router::process($request);
		$this->assertEqual($expected, $result->params);
		$this->assertFalse($request->is('get'));
		$this->assertFalse($request->is('put'));
		$this->assertTrue($request->is('post'));
		$this->assertFalse($request->is('delete'));
	}

	/**
	 * GET    /semester/:semester/program/:program/classes    Classes::index    [1.1.1]
	 */
	public function testSemesterProgramClassesGetRoute() {
		$expected = ['controller' => 'Classes', 'action' => 'index', 'semester' => '1401', 'program' => 'Graphic-Design'];
		$request = new Request([
			'url' => "/semester/1401/program/Graphic-Design/classes",
			'env' => [
				'REQUEST_METHOD' => 'GET'
			]
		]);
		$result = Router::process($request);
		$this->assertEqual($expected, $result->params);
		$this->assertTrue($request->is('get'));
		$this->assertFalse($request->is('put'));
		$this->assertFalse($request->is('post'));
		$this->assertFalse($request->is('delete'));
	}







	/**
	 * POST    /semester/:semester/program/:program/class/:class/assignments    Assignments::create    [1.1.1.1]
	 * POST    /semester/:semester/program/:program/class/:class/feedback    Feedback::create    [1.1.1.3]
	 * POST    /semester/:semester/program/:program/class/:class/documentation    Documentation::create    [1.1.1.4]
	 */
	public function testSemesterProgramClassPostRoute() {
		$expected = ['controller' => 'Classes', 'action' => 'create', 'semester' => '1401', 'program' => 'Graphic-Design'];
		$request = new Request([
#			'url' => "/semester/1401/program/Graphic-Design/class/",
			'env' => [
				'REQUEST_METHOD' => 'POST'
			]
		]);
		$result = Router::process($request);
		$this->assertEqual($expected, $result->params);
		$this->assertFalse($request->is('get'));
		$this->assertFalse($request->is('put'));
		$this->assertTrue($request->is('post'));
		$this->assertFalse($request->is('delete'));
	}

	/**
	 * GET    /semester/:semester/program/:program/class/:class/assignments    Assignments::index    [1.1.1.1]
	 * GET    /semester/:semester/program/:program/class/:class/feedback    Feedback::index    [1.1.1.3]
	 * GET    /semester/:semester/program/:program/class/:class/documentation    Documentation::index    [1.1.1.4]
	 */
	public function testSemesterProgramClassGetRoute() {
		$expected = ['controller' => 'Classes', 'action' => 'index', 'semester' => '1401', 'program' => 'Graphic-Design'];
		$request = new Request([
#			'url' => "/semester/1401/program/Graphic-Design/class",
			'env' => [
				'REQUEST_METHOD' => 'GET'
			]
		]);
		$result = Router::process($request);
		$this->assertEqual($expected, $result->params);
		$this->assertTrue($request->is('get'));
		$this->assertFalse($request->is('put'));
		$this->assertFalse($request->is('post'));
		$this->assertFalse($request->is('delete'));
	}















	// GET  /api/users/1.json  view
	public function _________testViewWithApiRoute() {
		$expected = [
			'controller' => 'Users',
			'action' => 'view',
			'api' => 'api',
			'id' => 1,
			'type' => 'json'
		];
		$request = new Request(['url' => '/api/users/1.json', 'env' => [
			'REQUEST_METHOD' => 'GET'
		]]);
/**
 * MAKE SURE OUTPUT IS JSON BY DOING json_decode() AND THEN json_encode() AND TRY TO ACCESS PROPERTIES.
 */
		$result = Router::process($request);
		$this->assertEqual($expected, $result->params);
		$this->assertTrue($request->is('get'));
		$this->assertFalse($request->is('put'));
		$this->assertFalse($request->is('post'));
		$this->assertFalse($request->is('delete'));
	}


/*
	// GET  /users/create  Users::create
	public function testCreateGetRoute() {
		$method = 'GET';
		$expected = ['controller' => 'Users', 'action' => 'create'];
		$request = new Request(['url' => '/users/create', 'env' => [
			'REQUEST_METHOD' => $method
		]]);
		$result = Router::process($request);
		$this->assertEqual($expected, $result->params);
		$this->assertTrue($request->is('get'));
		$this->assertFalse($request->is('put'));
		$this->assertFalse($request->is('post'));
		$this->assertFalse($request->is('delete'));
	}

	// POST  /users  Users::create
	public function testCreatePostRoute() {
		$method = 'POST';
		$expected = ['controller' => 'Users', 'action' => 'create'];
		$request = new Request(['url' => '/users', 'env' => [
			'REQUEST_METHOD' => $method
		]]);
		$result = Router::process($request);
		$this->assertEqual($expected, $result->params);
		$this->assertFalse($request->is('get'));
		$this->assertFalse($request->is('put'));
		$this->assertTrue($request->is('post'));
		$this->assertFalse($request->is('delete'));
	}

	// DELETE  /users/1  destroy
	public function testDestroyRoute() {
		$method = 'DELETE';
		$expected = ['controller' => 'Users', 'action' => 'destroy', 'id' => 1];
		$request = new Request(['url' => '/users/1', 'env' => [
			'REQUEST_METHOD' => $method
		]]);
		$result = Router::process($request);
		$this->assertEqual($expected, $result->params);
		$this->assertFalse($request->is('get'));
		$this->assertFalse($request->is('put'));
		$this->assertFalse($request->is('post'));
		$this->assertTrue($request->is('delete'));
	}

	// GET  /users/1/edit  edit
	public function testEditGetRoute() {
		$method = 'GET';
		$expected = ['controller' => 'Users', 'action' => 'edit', 'id' => 1];
		$request = new Request(['url' => '/users/1/edit', 'env' => [
			'REQUEST_METHOD' => $method
		]]);
		$result = Router::process($request);
		$this->assertEqual($expected, $result->params);
		$this->assertTrue($request->is('get'));
		$this->assertFalse($request->is('put'));
		$this->assertFalse($request->is('post'));
		$this->assertFalse($request->is('delete'));
	}

	// PUT  /users/1  edit
	public function testEditPutRoute() {
		$method = 'PUT';
		$expected = ['controller' => 'Users', 'action' => 'edit', 'id' => 1];
		$request = new Request(['url' => '/users/1', 'env' => [
			'REQUEST_METHOD' => $method
		]]);
		$result = Router::process($request);
		$this->assertEqual($expected, $result->params);
		$this->assertFalse($request->is('get'));
		$this->assertTrue($request->is('put'));
		$this->assertFalse($request->is('post'));
		$this->assertFalse($request->is('delete'));
	}

	// GET  /users  index
	public function testIndexWithoutImpliedIndexRoute() {
		$method = 'GET';
		$expected = ['controller' => 'Users', 'action' => 'index'];
		$request = new Request(['url' => '/users', 'env' => [
			'REQUEST_METHOD' => $method
		]]);
		$result = Router::process($request);
		$this->assertEqual($expected, $result->params);
		$this->assertTrue($request->is('get'));
		$this->assertFalse($request->is('put'));
		$this->assertFalse($request->is('post'));
		$this->assertFalse($request->is('delete'));
	}

	// GET  /users/index  index
	public function testIndexWithExplicitIndexRoute() {
		$method = 'GET';
		$expected = ['controller' => 'Users', 'action' => 'index'];
		$request = new Request(['url' => '/users/index', 'env' => [
			'REQUEST_METHOD' => $method
		]]);
		$result = Router::process($request);
		$this->assertEqual($expected, $result->params);
		$this->assertTrue($request->is('get'));
		$this->assertFalse($request->is('put'));
		$this->assertFalse($request->is('post'));
		$this->assertFalse($request->is('delete'));
	}

	// GET  /users/index.html  index
	public function testIndexWithExplicitIndexAndHtmlExtensionRoute() {
		$method = 'GET';
		$expected = ['controller' => 'Users', 'action' => 'index', 'type' => 'html'];
		$request = new Request(['url' => '/users/index.html', 'env' => [
			'REQUEST_METHOD' => $method
		]]);
		$result = Router::process($request);
		$this->assertEqual($expected, $result->params);
		$this->assertTrue($request->is('get'));
		$this->assertFalse($request->is('put'));
		$this->assertFalse($request->is('post'));
		$this->assertFalse($request->is('delete'));
	}

	// GET  /users/index.json  index
	public function testIndexWithExplicitIndexAndJsonExtensionRoute() {
		$method = 'GET';
		$expected = ['controller' => 'Users', 'action' => 'index', 'type' => 'json'];
		$request = new Request(['url' => '/users/index.json', 'env' => [
			'REQUEST_METHOD' => $method
		]]);
		$result = Router::process($request);
		$this->assertEqual($expected, $result->params);
		$this->assertTrue($request->is('get'));
		$this->assertFalse($request->is('put'));
		$this->assertFalse($request->is('post'));
		$this->assertFalse($request->is('delete'));
	}

	// GET  /users/1  view
	public function testViewRoute() {
		$method = 'GET';
		$expected = ['controller' => 'Users', 'action' => 'view', 'id' => 1];
		$request = new Request(['url' => '/users/1', 'env' => [
			'REQUEST_METHOD' => $method
		]]);
		$result = Router::process($request);
		$this->assertEqual($expected, $result->params);
		$this->assertTrue($request->is('get'));
		$this->assertFalse($request->is('put'));
		$this->assertFalse($request->is('post'));
		$this->assertFalse($request->is('delete'));
	}

	// GET  /users/1.html  view
	public function testViewWithHtmlExtensionRoute() {
		$method = 'GET';
		$expected = ['controller' => 'Users', 'action' => 'view', 'id' => 1, 'type' => 'html'];
		$request = new Request(['url' => '/users/1.html', 'env' => [
			'REQUEST_METHOD' => $method
		]]);
		$result = Router::process($request);
		$this->assertEqual($expected, $result->params);
		$this->assertTrue($request->is('get'));
		$this->assertFalse($request->is('put'));
		$this->assertFalse($request->is('post'));
		$this->assertFalse($request->is('delete'));
	}

	// GET  /users/1.json  view
	public function testViewWithJsonExtensionRoute() {
		$method = 'GET';
		$expected = ['controller' => 'Users', 'action' => 'view', 'id' => 1, 'type' => 'json'];
		$request = new Request(['url' => '/users/1.json', 'env' => [
			'REQUEST_METHOD' => $method
		]]);
		$result = Router::process($request);
		$this->assertEqual($expected, $result->params);
		$this->assertTrue($request->is('get'));
		$this->assertFalse($request->is('put'));
		$this->assertFalse($request->is('post'));
		$this->assertFalse($request->is('delete'));
	}
*/
}
?>