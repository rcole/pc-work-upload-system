<?php

namespace app\tests\cases\models;

use app\models\Classes;
use app\models\Semesters;
use app\tests\mocks\data\MockClasses;
use app\fixtures\ClassesFixture;
use app\tests\mocks\data\MockSemesters;

class ClassesTest extends \lithium\test\Unit {

	public function setUp() {
		MockSemesters::remove();
		foreach (MockSemesters::find('all') as $data) {
			$semester = MockSemesters::create();
			$semester->save($data);
		}

		MockClasses::remove();
		foreach (ClassesFixture::load() as $data) {
			$class = MockClasses::create();
			$class->save($data);
		}
	}

	public function tearDown() {
		MockSemesters::remove();
		MockClasses::remove();
	}

	// Tests that `slug` valid based on `title`
	public function testSlugIsFromTitleOnCreate() {
		$data = [
			'module_id' => 1,
			'semester_id' => 1,
			'title' => 'My Cool Class',
			'begins' => 1367107200,
			'ends' => 1374969600
		];
		$class = Classes::create();
		$class->save($data);
		$this->assertEqual('My-Cool-Class', $class->slug);

		$data = [
			'module_id' => 1,
			'semester_id' => 1,
			'title' => 'Intro to Foo 101',
			'begins' => 1377648000,
			'ends' => 1382918400
		];
		$class = Classes::create();
		$class->save($data);
		$this->assertEqual('Intro-to-Foo-101', $class->slug);
	}

	// Tests that `slug` valid based on `title` on update()
	public function testSlugIsFromTitleOnUpdate() {
		$class = Classes::find(1);
		$class->title = 'Tracing Foo Rasters 101';
		$class->save();
		$this->assertEqual('Tracing-Foo-Rasters-101', $class->slug);
	}

	public function testClassInheritsSemesterDates() {
		$semester = Semesters::create();
		$semester->save([
			'id' => 100,
			'year' => 2020,
			'semester' => 3,
			'begins' => '1377648000',
			'ends' => '1382918400'
		]);

		$data = [
			'module_id' => 1,
			'semester_id' => 100,
			'title' => 'My first class'
		];
		$class = Classes::create();
		$class->save($data);
		$this->assertEqual('1377648000', $class->begins);
		$this->assertEqual('1382918400', $class->ends);

		$data = [
			'module_id' => 2,
			'semester_id' => 100,
			'title' => 'My Second Class',
			'begins' => '1377648111',
			'ends' => '1382918111'
		];
		$class = Classes::create();
		$class->save($data);
		$this->assertEqual('1377648111', $class->begins);
		$this->assertEqual('1382918111', $class->ends);

		$semester->delete();
	}
}

?>