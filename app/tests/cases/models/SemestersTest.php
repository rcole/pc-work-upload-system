<?php

namespace app\tests\cases\models;

use app\models\Semesters;
use app\tests\mocks\data\MockSemesters;

class SemestersTest extends \lithium\test\Unit {

	public function setUp() {}

	public function tearDown() {}

	// Tests that `title` is created properly
	public function testTitleIsValid() {
		$data = [
			'year' => 2015,
			'semester' => 3,
			'begins' => 1377648000,
			'ends' => 1382918400
		];
		$semester = Semesters::create();
		$semester->save($data);
		$this->assertEqual(1503, $semester->title);
	}
}

?>