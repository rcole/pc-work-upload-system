<?php

namespace app\tests\cases\models;

use app\models\ProgramLeaders;
use app\tests\mocks\data\MockProgramLeaders;
use app\tests\mocks\data\MockUsers;
use app\fixtures\UsersFixture;

class ProgramLeadersTest extends \lithium\test\Unit {

	/**
	 * Loads records from fixtures into test database.
	 */
	public function setUp() {
		MockProgramLeaders::remove();
		foreach (UsersFixture::load() as $data) {
			$users = MockUsers::create();
			$users->save($data);
		}
	}

	/**
	 * Removes records from test database.
	 */
	public function tearDown() {
		MockUsers::remove();
	}

/*
	public function testFindOnlyProgramLeaderUsers() {
		$programLeaders = ProgramLeaders::find('all');
		$result = $programLeaders->count();
		$this->assertEqual(2, $result);
	}
*/
}

?>