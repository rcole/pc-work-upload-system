<?php

namespace app\tests\cases\controllers;

use lithium\action\Request;
use app\fixtures\RolesFixture;
use app\tests\mocks\data\MockRoles;
use app\fixtures\UsersFixture;
use app\tests\mocks\data\MockUsers;
use app\fixtures\RolesUsersFixture;
use app\tests\mocks\data\MockRolesUsers;
use app\models\Sessions;
use app\controllers\SessionsController;
use lithium\security\Auth;

class SessionsControllerTest extends \lithium\test\Unit {

	public function setUp() {
		MockRoles::remove();
		foreach (RolesFixture::load() as $rolesData) {
			$roles = MockRoles::create();
			$roles->save($rolesData);
		}

		MockUsers::remove();
		foreach (UsersFixture::load() as $usersData) {
			$users = MockUsers::create();
			$users->save($usersData);
		}

		MockRolesUsers::remove();
		foreach (RolesUsersFixture::load() as $rolesUsersData) {
			$rolesUsers = MockRolesUsers::create();
			$rolesUsers->save($rolesUsersData);
		}
	}

	public function tearDown() {
		MockRolesUsers::remove();
		MockUsers::remove();
		MockRoles::remove();
		Auth::clear('default');
		Sessions::clear();
	}

	public function testLogin() {
		$fixture = UsersFixture::load(0);
		$data = [
			'email' => $fixture['email'],
			'password' => $fixture['password'],
		];
        $request = new Request(['env' => ['REQUEST_METHOD' => 'POST']]);
        $request->data = $data;
        $controller = new SessionsController(['request' => $request]);
        $result = is_array($controller->create());
        $this->assertFalse($result);

		$data2 = [
			'email' => $fixture['email'],
			'password' => 'badpassword'
		];
        $request2 = new Request(['env' => ['REQUEST_METHOD' => 'POST']]);
        $request2->data = $data2;
        $controller2 = new SessionsController(['request' => $request2]);
        $result2 = is_array($controller2->create());
        $this->assertTrue($result2);

        $this->assertTrue(is_numeric(Sessions::id()));
	}
}

?>