<?php

namespace app\tests\mocks\data;

use lithium\data\entity\Record;
use lithium\data\collection\RecordSet;

class MockPrograms extends \app\models\Programs {

	protected $_meta = [
		'source' => 'programs',
		'name' => 'Programs'
	];

	private static $_data = [
		[
			'id' => 1,
			'program_leaders_id' => 20,
			'title' => 'Graphic Design'
		],
		[
			'id' => 2,
			'program_leaders_id' => 21,
			'title' => 'User Interfaces'
		]
	];

	public static function find($type = 'all', array $options = array()) {
		if (is_numeric($type)) {
			return new Record([
				'data' => static::$_data[$type]
			]);
		}

		switch ($type) {
			case 'first':
				return new Record([
					'data' => static::$_data[0]
				]);
			break;

			default:
			case 'all':
				return new RecordSet([
					'data' => static::$_data
				]);
			break;
		}
	}
}

?>