<?php

namespace app\tests\mocks\data;

use lithium\data\entity\Record;
use lithium\data\collection\RecordSet;

class MockClasses extends \app\models\Classes {

	protected $_meta = [
		'source' => 'classes',
		'name' => 'Classes'
	];

	private static $_data = [
		[
			'id' => 1,
			'module_id' => 1,
			'semester_id' => 1,
			'title' => 'Intro to Rasters',
			'slug' => 'Intro-to-Rasters',
			'begins' => 1357430400,
			'ends' => 1364428800
		],
		[
			'id' => 2,
			'module_id' => 1,
			'semester_id' => 1,
			'title' => 'Tracing Rasters',
			'slug' => 'Tracing-Rasters',
			'begins' => 1357430400,
			'ends' => 1364428800
		],
		[
			'id' => 3,
			'module_id' => 1,
			'semester_id' => 2,
			'title' => 'Intro to Rasters',
			'slug' => 'Intro-to-Rasters',
			'begins' => 1367107200,
			'ends' => 1374969600
		],
		[
			'id' => 4,
			'module_id' => 1,
			'semester_id' => 2,
			'title' => 'Photoshop 101',
			'slug' => 'Photoshop-101',
			'begins' => 1367107200,
			'ends' => 1374969600
		],
		[
			'id' => 5,
			'module_id' => 2,
			'semester_id' => 1,
			'title' => 'Intro to Vectors',
			'slug' => 'Intro-to-Vectors',
			'begins' => 1357430400,
			'ends' => 1364428800
		]
	];

	public static function find($type = 'all', array $options = array()) {
		if (is_numeric($type)) {
			return new Record([
				'data' => static::$_data[$type]
			]);
		}

		switch ($type) {
			case 'first':
				return new Record([
					'data' => static::$_data[0]
				]);
			break;

			default:
			case 'all':
				return new RecordSet([
					'data' => static::$_data
				]);
			break;
		}
	}
}

?>