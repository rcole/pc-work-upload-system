// adminModule.js Starts here 

var adminApp = angular.module('adminApp' , [
  'globals',
  'semesterProgramModule',
  'semesterProgramClassModule',
  'semestersModule',
  'programsModule',
  'extcodesModule',
  'myclassesModule',
  'usersModule',
  'modulesModule',
  'flow',
  'MessageCenterModule',
  'uservoiceModule',
  'analyticsModule',
  'submissionsListModule'
]);

adminApp.config(['$stateProvider','$urlRouterProvider','$locationProvider',function($stateProvider, $urlRouterProvider, $locationProvider){
    
  $urlRouterProvider.otherwise("/programs");
  $locationProvider.html5Mode(false);
    
}]);
// globalModule.js Starts here 

var globals = angular.module('globals', ['ui.router', 'ngQuickDate', 'MessageCenterModule']);

globals.config(['$stateProvider','$urlRouterProvider','$locationProvider', '$provide', '$httpProvider', '$sceDelegateProvider', function($stateProvider, $urlRouterProvider, $locationProvider, $provide, $httpProvider, $sceDelegateProvider){
    
  $urlRouterProvider.otherwise("/");
  $locationProvider.html5Mode(false);
  
  var profile = {
    name: "profile",
    url: "/profile",
    templateUrl: "app/webroot/tpl/profileEdit.tpl.html",
    controller: "profileController as profile"
  },
  logout = {
      name: "logout",
      url: "/logout",
      template: "",
      controller: "logoutController as logout"
  };
  
  $stateProvider.state(profile);
  $stateProvider.state(logout);

  var dateProperties = ['created','updated','begins','ends','expires_date','due'];

  var propsToDelete = ['created','updated'];

    $httpProvider.interceptors.push('httpInterceptor');

    $httpProvider.defaults.transformResponse.push(function transformRes(res){

        // Unix to Date

        var checker = function r(obj) {
                      
            for (var key in obj) {

                if (typeof obj[key] == "object") {

                    r(obj[key]);
                
                } else if (typeof obj[key] != "function"){

                    if (dateProperties.indexOf(key) > -1){

                        obj[key] = new Date(obj[key]*1000);

                    }

                }
        
            }
            
        };

        if (typeof res == "object") {

            // So we don't process html and other non-json responses. 
            // Since this goes after the built-ins we'll have an object in place of json at this point.

            checker(res);

        }

        return res;

    });

    $httpProvider.defaults.transformRequest.unshift(function transformReq(req){

        // Date to unix

        var checker = function r(obj) {
                    
            for (var key in obj) {

                if (propsToDelete.indexOf(key) > -1){

                    delete obj[key];

                } else {
                    
                    if (typeof obj[key] == "object" && !obj[key] instanceof Date) {

                        r(obj[key]);
                    
                    } else if (typeof obj[key] != "function" && obj[key] !== null){

                        if (dateProperties.indexOf(key) > -1){

                            console.log(key, obj[key]);

                            obj[key] = obj[key].valueOf() / 1000;

                        }
            
                    }
                }
        
            }
        };

        checker(req);

        return req;

    });

    // this is for the file viewer module
    $sceDelegateProvider.resourceUrlWhitelist([
    'self',
    'http://*.google.com/**'
  ]);
    
}]);
// start globals.js

globals.controller('globalsController', ['$scope', '$location', '$stateParams', '$state', 'globalsFactory','semesterFactory', 'programsFactory', 'usersFactory', 'profileFactory', 'uvFactory', 'analyticsFactory', function($scope, $location, $stateParams, $state, globalsFactory, semesterFactory, programsFactory, usersFactory, profileFactory, uvFactory, analyticsFactory){
    
    console.log('globals ctrl');

    var ctrl = this;

    // still need this until all instances of old select2 plugin are removed.
    ctrl.selectOptions = {allowClear:true };

    ctrl.selectedSemester = semesterFactory.selectedSemester; 

    var semesterUpdater = function(){

        // this gets called from the semester factory whenever the selected sem changes. 

        ctrl.selectedSemester = semesterFactory.selectedSemester; 

        // if the selected semester does not match the one in the current route, then update the view

        if($stateParams.semester && $stateParams.semester != semesterFactory.selectedSemester) {

            $state.go($state.$current, {semester: semesterFactory.selectedSemester});

        }

    },

    initSemester = function(){

        if($stateParams.semester){

            semesterFactory.setSelectedSemester($stateParams.semester);

        } else {

            semesterFactory.setSelectedSemester(ctrl.currentSemester); 

        }

    };

    semesterFactory.registerObserverCallback(semesterUpdater); // register semesterUpdater to get called when the selected semester changes

    ctrl.changeSemester = function(selection){

        semesterFactory.setSelectedSemester(selection);

    };

    semesterFactory.getCurrentSemester().then(function(data){

        if(data.semester){

            ctrl.currentSemester = data.semester.title;

            // redundant if I can update the title-only references throughout the app.
            ctrl.currentSemesterObj = data.semester;

        } else {

            ctrl.currentSemester = "No Current Semester";

        }

        initSemester();
    
    }, function(error){
        
    });
    
    ctrl.userInfo = {};

    profileFactory.get().then(function(data){

        // find out if the current user is a teacher

        angular.forEach(data.user.roles_users, function(value, key){
    
            if (value.role_id == 3) {

                data.user.isTeacher = true;

            }
        
        });

        // sets up user voice user ID
        uvFactory(data.user);

        analyticsFactory.identify(data.user);

        ctrl.userInfo = data.user;

    }, function(error){
    
        console.log('profile error: ' + error);
    
    });

    ctrl.state = {

        userToolsVisible : false,
    
        toggleUserTools : function(){
  
            this.userToolsVisible = !this.userToolsVisible;
    
        }
    };

    // init on page load only when we have params. Otherwise current semester is not ready yet.

    if($stateParams.semester){

        initSemester();

    }

    $scope.$on('$locationChangeSuccess', function () {

        initSemester();

        ctrl.state.userToolsVisible = false;

    });

    ctrl.semesterList = [];

    semesterFactory.query().then(function(data){

        ctrl.semesterList = data.semesters;

    }, function (error){
  
        console.log('ruh roh');
  
    });

    ctrl.teachers = [];

    usersFactory.teachers().then(function (data){

      ctrl.teachers = data.roles;
    
    }, function (error){
    
      console.log('ruh roh');
    
    });

}]);

// Start nav.js

globals.controller ('navController', ['$scope','$q','$state','$stateParams','programsFactory','globalsFactory','semesterFactory', function($scope, $q, $state, $stateParams, programsFactory, globalsFactory, semesterFactory) {

    console.log('navController instantiated');  

    var ctrl = this;
 
    ctrl.programsListOpen = false;

    ctrl.toggleProgramsList = function(){
 
        ctrl.programsListOpen = !ctrl.programsListOpen; 
  
    };

    // populates nav items
    
    ctrl.programList = [];

    programsFactory.query().then(function (data){

        ctrl.programList = data.programs;

    }, function (error){

        console.log('ruh roh');

    });

}]);
// start profile.js

globals.controller('profileController', ['$scope', '$q', '$timeout', 'profileFactory', 'validatorFactory', 'roleClassesFactory', function ($scope, $q, $timeout, profileFactory, validatorFactory, roleClassesFactory) {
  
    var ctrl = this;

    ctrl.validating = false;

    ctrl.message = '';

    var time, 
        running, 
        fetchDataTimer;

    ctrl.myProfile = {};
  
    profileFactory.get().then(function (data){

        ctrl.myProfile = data.user;

        ctrl.original = angular.copy(ctrl.myProfile);

        getClasses(data.user.id);

    }, function (error){

        console.log('ruh roh');

    });

    function getClasses(id){

        roleClassesFactory.studentClasses({id: id}).then(function(data){

            ctrl.studentClasses = data.classes;

        });

        roleClassesFactory.teacherClasses({id: id}).then(function(data){

            ctrl.teacherClasses = data.classes;

        });

    }    

    ctrl.validateEmail = function(){

        var mailToCheck = ctrl.myProfile.email;

        $timeout.cancel(fetchDataTimer);

        $scope.myProfileEditForm.email_address.$setValidity('unique', true);
        
        if (mailToCheck){ 

            ctrl.validating = true;

            fetchDataTimer = $timeout(function () {

                validatorFactory('users', 'email', {validate: mailToCheck})

                .then(function(data){

                    // show validation message here with ctrl.emailInvalid == true if not good
                    // set form invalid if mail isn't valid

                    if(data.success === true) {

                        $scope.myProfileEditForm.email_address.$setValidity('unique', true);

                    } else {

                        $scope.myProfileEditForm.email_address.$setValidity('unique', false);

                    }

                    ctrl.validating = false;

                }, function (error){
                
                    console.log(error);

                    ctrl.validating = false;
                
                });
            
            }, 800);
            
        }

    };

    ctrl.cancelEdit = function(){ // untested
    
        ctrl.myProfile = angular.copy(ctrl.original);
    
    };

    ctrl.saveProfile = function(){

        profileFactory.update(ctrl.myProfile).then(function (data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving yor info.", { timeout: 6000 });

            } else {

                ctrl.passwordEdit = false;

                ctrl.myProfile.password = '';

                ctrl.myProfile.password_again = '';

                ctrl.message = 'Profile Updated';

                $timeout(function(){

                    ctrl.message = '';

                }, 1500);

            }

        }, function (error){

            console.log('ruh roh');

        });
        
    };

}]);
// start globals.js

globals.controller('studentGlobalsController', ['$scope', '$location', '$stateParams', '$state', 'globalsFactory','semesterFactory', 'programsFactory', 'usersFactory', 'profileFactory', 'uvFactory', 'analyticsFactory', function($scope, $location, $stateParams, $state, globalsFactory, semesterFactory, programsFactory, usersFactory, profileFactory, uvFactory, analyticsFactory){
    
    console.log('studentGlobalsController');

    var ctrl = this;

    ctrl.selectOptions = {allowClear:true };

    ctrl.selectedSemester = semesterFactory.selectedSemester; 

    var initSemester = function(){

      if($stateParams.semester){

        semesterFactory.setSelectedSemester($stateParams.semester);

      } else {

        semesterFactory.setSelectedSemester(ctrl.currentSemester); 

      }

    };

    semesterFactory.getCurrentSemester().then(function(data){

      if(data.semester){

        ctrl.currentSemester = data.semester.title;

      } else {

        ctrl.currentSemester = "No Current Semester";

      }

      initSemester();
    
    }, function(error){
        
    });
    

    ctrl.userInfo = {};

    profileFactory.get().then(function(data){

        uvFactory(data.user);

        analyticsFactory.identify(data.user);

        ctrl.userInfo = data.user;

    }, function(error){
    
      console.log('profile error: ' + error);
    
    });

    ctrl.state = {

        userToolsVisible : false,
    
        toggleUserTools : function(){
  
          this.userToolsVisible = !this.userToolsVisible;
    
        }
    };

    // init on page load only when we have params. Otherwise current semester is not ready yet.

    if($stateParams.semester){

      initSemester();

    }

    $scope.$on('$locationChangeSuccess', function () {

      initSemester();

      ctrl.state.userToolsVisible = false;

    });
    

    ctrl.semesterList = [];

    semesterFactory.query().then(function(data){

      ctrl.semesterList = data.semesters;

    }, function (error){
  
      console.log('ruh roh');
  
    });

}]);

globals.directive('back', function factory($window) {
 
      return {
 
        restrict   : 'EA',
   
        link: function (scope, element, attrs) {

            element.on('click', function(){

                $window.history.back();

            });
  
        }
 
      };
 
    });
globals.directive('checkSame', ["$document", function ($document) {

	// check-same="lang1 false"
	// first > field to compare with by ID
	// second > true : should match , false : should not match
	
	return {
	
		require: 'ngModel',
	
		link: function (scope, elem, attrs, ctrl) {
	
			var expression = attrs.checkSame,
				firstField = '#' + expression.split(' ')[0],
				match = expression.split(' ')[1];

			var $firstField = angular.element($document[0].querySelector(firstField));
	
			function checkIt(){

				scope.$apply(function () {
		
					var v = elem.val() === $firstField.val();
					
					v = match === 'true' ? v : !v;

					ctrl.$setValidity('match', v);
		
				});

			}

			elem.on('input propertychange', function () {
			
				checkIt();
			
			});

			$firstField.on('input propertychange', function () {
			
				checkIt();
			
			});

		}

	};

}]);
// :p
globals.directive('hideUntilLoaded', ['usersFactory', '$document', function (usersFactory, $document) {
        return {
                restrict: 'A',

                scope: {},

                transclude: true,

                template:   '<div class="loadingSpinner"></div>' +
                            '<ng-transclude class="ng-transclude"></ng-transclude>'
        };
}]);
globals.directive("loader", ['$rootScope', '$timeout', function ($rootScope, $timeout) {
   
    return function ($scope, element, attrs) {

        var time, 
        running, 
        fetchDataTimer;

        var $icon = element.find('path');

        $scope.$on("loader_show", function () {

            fetchDataTimer = $timeout(function () {

                element.addClass('loading');

                $scope.endLoader();
            
            }, 800);
   
        });
   
        $scope.$on("loader_hide", function () {

            $icon.bind("animationiteration webkitAnimationIteration MSAnimationIteration oAnimationIteration", function(){
                
                $icon.unbind();

                element.removeClass('loading');
            
            });

            $timeout.cancel(fetchDataTimer);
      
        });

        $scope.endLoader = function(){

            $timeout(function () {

                $timeout.cancel(fetchDataTimer);

                $icon.bind("animationiteration webkitAnimationIteration MSAnimationIteration oAnimationIteration", function(){
                
                    $icon.unbind();

                    element.removeClass('loading');

                });
                        
            }, 10000);

        };
   
    };

}]);
globals.directive('modalBack', ['$window', '$state', '$document', function factory($window, $state, $document) {
 
      return {
 
        restrict   : 'EA',
   
        link: function (scope, element, attrs) {

            // This is not possible due to no click event on : after!!!!

            var elsewhere = true;

            var clickElsewhere = function() {

                console.log('clicked elsewhere');
            
                if (elsewhere) {
            
                    // go back here

                    $state.go('^');
            
                    $document.off('click', clickElsewhere);
            
                }
            
                elsewhere = true;
            
            };
        
            element.on('click', function(e){

                console.log('clicked', elsewhere);

                // catch clicks here as to not close the modal.
        
                elsewhere = false;
            
                $document.off('click', clickElsewhere);
            
                $document.on('click', clickElsewhere);
        
            });
  
        }
 
      };
 
    }]);
globals.directive('personMenu', ['usersFactory', '$document', function (usersFactory, $document) {
		return {
				restrict: 'A',

				scope: {pID : '=personMenu'},
				
				link: function (scope, element, attrs) {

					var elsewhere = true;

					var clickElsewhere = function() {
					
						if (elsewhere) {
					
							element.find('.personMenu').removeClass('personMenuOpen');
					
							$document.off('click', clickElsewhere);
					
						}
					
						elsewhere = true;
					
					};
				
					element.on('click', function(e){
				
						element.find('.personMenu').addClass('personMenuOpen');

						elsewhere = false;
					
						$document.off('click', clickElsewhere);
					
						$document.on('click', clickElsewhere);
				
					});

					usersFactory.get(scope.pID).then(function(result){

						if(result){

							scope.fullName = result.user.first_name + ' ' + result.user.last_name;

							scope.email = result.user.email;

						} else {

							element.off('click');

							scope.fullName = scope.pID;

							scope.email = '';

						}

					});
				
				},

				templateUrl: "app/webroot/tpl/part.personMenu.tpl.html"
		};
}]);
// start extensionsCodesFactories.js

globals.factory('extensioncodesFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {

  var extcodesResource = $resource('api/extensioncodes.json', {}, 

    {

      'query': {method: 'GET', isArray: false},

      'create': {method: 'POST'}

    });

  var extcodeResource = $resource('api/extensioncode/:id.json',  {id:'@id'},

    {

      'get': {method: 'GET', isArray: false},

      'update': {method: 'POST'},

      'remove': {method: 'POST'}

    }); 

  var factory = {
    
    // add promises
    query : function () {

      var deferred = $q.defer();

      extcodesResource.query({},

        function (resp) {

          deferred.resolve(resp);

        });

      return deferred.promise;

    },

    get : function (params) {
  
      var deferred = $q.defer();
  
      extcodeResource.get({id: params.id},
  
        function (resp) {
  
          deferred.resolve(resp);
  
        });
  
      return deferred.promise;
  
    },
  
    create : function (payload) {

      console.log(payload);
  
      var deferred = $q.defer();
  
      extcodesResource.create(payload, function(resp){
  
        deferred.resolve(resp);
  
      });

      return deferred.promise;
  
    },
  
    update : function (params, payload) {
  
      payload._method = 'put';

      delete payload.id;
      
      delete payload.created;
      
      delete payload.updated;

      var deferred = $q.defer();
    
      extcodeResource.update({id: params.id}, payload, function(resp){
  
        deferred.resolve(resp);
  
      });

      return deferred.promise;
  
    },
  
    remove : function (params) {
  
      var deferred = $q.defer();

      var payload = { _method: 'delete' };
  
      extcodeResource.remove({id: params.id}, payload, function(resp){
  
        deferred.resolve(resp);
  
      });

      return deferred.promise;
  
    }
  
  };

  return factory;

}]);
globals.factory('globalsFactory', [ function () {

    // this is mostly a collection of rando util type functions for use globally
    // will probably split this up eventually
    
    var factory = {

      // takes a program or class slug and strips out dashes
      slugCleaner : function(slug){

        var clean;
        
        if(slug) {

          clean = slug.replace(/-/g, " ");

        } else {

          clean = "";

        }
     
        return clean;
     
      },

      // could go into the users factory automatically on query or as middleware in type builders
      makeFullName : function(data){
     
        angular.forEach(data.users, function(value, key){
     
          value.full_name = value.first_name + ' ' + value.last_name;
     
        });
     
      }
    
    };

    return factory;
}]);
globals.factory('httpInterceptor', ['$q', '$rootScope', '$log', function ($q, $rootScope, $log) {

    var numLoadings = 0;

    return {
        request: function (request) {
            // Show loader
            
            numLoadings++;
            
            $rootScope.$broadcast("loader_show");
            
            return request || $q.when(request);

        },
        response: function (response) {

            if ((--numLoadings) === 0) {
                // Hide loader
            
                $rootScope.$broadcast("loader_hide");
            }

            return response || $q.when(response);

        },
        responseError: function (response) {

            if ((--numLoadings) === 0) {
                // Hide loader
             
                $rootScope.$broadcast("loader_hide");
            
            }

            return $q.reject(response);
        }
    };
}]);

globals.factory('profileFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {
  

  var profileResource = $resource('api/profile.json', {}, 

  {
  
    'get': {method: 'GET', isArray: false, cache: true},

    'update': {method: 'POST'}
  
  });


  var factory = {

    // add promises
    get : function () {

      var deferred = $q.defer();

      profileResource.get({},

        function (resp) {

          deferred.resolve(resp);

        });

      return deferred.promise;

    },

    update : function (payload) {

      payload._method = 'put';

      var deferred = $q.defer();

      profileResource.update({}, payload, function(resp){

        console.log(resp);

        deferred.resolve(resp);

      });

      return deferred.promise;

    }
  
  };

  return factory;

}]);
// programsFactory.js

globals.factory('programsFactory', ['$http','$resource','$q', function($http, $resource, $q) {
  

  var programsResource = $resource('api/programs.json', {},
    
    { 
    
      'query': {method: 'GET', isArray: false, cache: false},

      'create': {method: 'POST'} 
    
    });


  var programResource = $resource('api/program/:program.json', {program: '@program'},
    
    {
    
      'get': {method: 'GET'},
    
      'update': {method: 'POST'},
    
      'remove': {method: 'DELETE'} 
    
    });

  var authorizationsResource = $resource('api/programauthorizations.json',

    {},

    {

      'add': {method: 'POST'},

      'remove': {method: 'POST'} 

    });


  var factory = {

    query : function () {

      var deferred = $q.defer();

      programsResource.query({},

        function (resp) {

          deferred.resolve(resp);

        });

      return deferred.promise;

    },

    get : function (params) {

      var deferred = $q.defer();

      programResource.get({program: params.program},

        function (resp) {

          deferred.resolve(resp);

        });

      return deferred.promise;

    },

    create : function (payload) {

      var deferred = $q.defer();

      programsResource.create(payload, 

        function(resp){

          deferred.resolve(resp);

           console.log(resp);

        });

      return deferred.promise;

    },

    update : function (params, payload) {

      payload._method = 'put';

      var deferred = $q.defer();

      programResource.update({program: params.program}, payload, function(resp){

        deferred.resolve(resp);

      });

      return deferred.promise;

    },

    remove : function (params) {

      var deferred = $q.defer();

      var payload = { _method: 'delete' };

      programResource.remove({program: params.program}, payload, function(resp){

        deferred.resolve(resp);

      });

      return deferred.promise;

    },
    addAuth : function (payload) {

      payload._method = 'put';

      var deferred = $q.defer();

      authorizationsResource.add({}, payload, function(resp){

        deferred.resolve(resp);

      });

      return deferred.promise;

    },
    removeAuth : function (payload) {

      payload._method = 'delete';

      var deferred = $q.defer();

      authorizationsResource.remove({}, payload, function(resp){

        deferred.resolve(resp);

      });

      return deferred.promise;

    }

  };
    
  return factory;

}]);
// start semesterFactory.js

globals.factory('semesterFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {
  

  var semestersResource = $resource('api/semesters.json', {},
    
    {

      'query': { method: 'GET', isArray: false, cache: false },

      'create': {method: 'POST'} 

    });


  var semesterResource = $resource('api/semester/:semester.json', {semester:'@semester'},

    {

      'get': {method: 'GET'},

      'update': {method: 'POST'},

      'remove': {method: 'POST'}

    }); 


  var currentSemesterResource = $resource('api/semesters/current.json', {}, 

    {'query': {method: 'GET', isArray: false, cache: true} });


  var observerCallbacks = [],


  //call this when you know the semester has been changed
  notifyObservers = function(){
    
    angular.forEach(observerCallbacks, function(callback){
    
      callback();
    
    });
  
  },

  setSelectedSemester = function(s){
  
    factory.selectedSemester = s;

    notifyObservers();
  
  },

  selectCurrentSemester = function(){

    factory.selectedSemester = factory.getCurrentSemester()
    
    .then(function(data){

      notifyObservers();

      return data.title;

    });

  },

  factory = {
    
    //register an observer from a controller
    registerObserverCallback : function(callback){
    
      observerCallbacks.push(callback);
    
    },
    
    query : function () {
    
      // returns list of all semesters
    
      var deferred = $q.defer();
    
      semestersResource.query({}, function(resp) {
    
          deferred.resolve(resp);
    
        });
    
      return deferred.promise;
    
    },
   
    get : function (params) {
 
      var deferred = $q.defer();
 
      semesterResource.get({semester: params.semester},
 
        function (resp) {
 
          deferred.resolve(resp);
 
        });
 
      return deferred.promise;
 
    },
 
    create : function (payload) {

      var deferred = $q.defer();
  
      semestersResource.create(payload, function(resp){
 
        deferred.resolve(resp);
 
      });

      return deferred.promise;
 
    },
 
    update : function (params, payload) {

      var deferred = $q.defer();

      payload._method = 'put';
   
      semesterResource.update({semester: params.semester}, payload, function(resp){
 
        deferred.resolve(resp);
 
      });

      return deferred.promise;
 
    },
 
    remove : function (params) {

      var deferred = $q.defer();

      var payload = { _method: 'delete' };
  
      semesterResource.remove({semester: params.semester}, payload, function(resp){
 
        deferred.resolve(resp);
 
      });

      return deferred.promise;
    
    },

    getCurrentSemester : function(){
   
      // returns currently active semester
   
      var deferred = $q.defer();
   
      currentSemesterResource.query({},
   
        function (resp) {
   
          deferred.resolve(resp);
   
        });
   
      return deferred.promise;
   
    },

    selectedSemester : "",

    setSelectedSemester : setSelectedSemester,

    selectCurrentSemester : selectCurrentSemester
  
  };

  // add methods for getting and setting selected semester
  
  return factory;

}]);
// usersFactories.js

globals.factory('usersFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {
  
  var usersResource = $resource('api/users.json', {},
   
    { 
      
      'query' : {method: 'GET', isArray: false},

      'create': {method: 'POST'} 

    });

  var userResource = $resource('api/user/:id.json', {id: '@id'},
    
    {
    
      'get': {method: 'GET', isArray: false, cache: true},      
    
      'update': {method: 'POST'},
    
      'remove': {method: 'POST'}
    
    });

  var roleResource = $resource('api/role/:role.json', {role: '@role'},
   
    { 
      
      'query' : {method: 'GET', isArray: false}

    });


  var factory = {

    query : function () {
    
      var deferred = $q.defer();
    
      usersResource.query({},
    
        function (resp) {
    
          deferred.resolve(resp);
    
        });
    
      return deferred.promise;
    
    },
    
    get: function (params) {

      var deferred = $q.defer();

      // this is to allow for either only an ID string or an object containing an ID property to call the factory

      var ID;
      
      if (params.hasOwnProperty('user')) {

        ID = params.user;

      } else {

        ID = params;

      }

      userResource.get({id: ID},

      function (resp) {

        deferred.resolve(resp);

      });

      return deferred.promise;
  
    },
  
    create : function (payload) {
  
      var deferred = $q.defer();
  
      usersResource.create(payload, function(resp){
  
        deferred.resolve(resp);
  
      });

      return deferred.promise;
  
    },
  
    update : function (params, payload) {
  
      payload._method = 'put';
  
      var deferred = $q.defer();
  
      userResource.update({id: params.user}, payload, function(resp){
  
        deferred.resolve(resp);

      });

      return deferred.promise;
  
    },
  
    remove : function (params) {
  
      var payload = { _method: 'delete' };

      var deferred = $q.defer();
  
      userResource.remove({id: params.user}, payload, function(resp){
  
        deferred.resolve(resp);
  
      });

      return deferred.promise;
  
    },

    teachers : function(){

      var deferred = $q.defer();
  
      roleResource.query({role: 'teacher'}, function(resp){
  
        deferred.resolve(resp);
  
      });

      return deferred.promise;

    },

    programLeaders : function(){

      var deferred = $q.defer();
  
      roleResource.query({role: 'prgLead'}, function(resp){
  
        deferred.resolve(resp);
  
      });

      return deferred.promise;

    },

    admins : function(){

      var deferred = $q.defer();
  
      roleResource.query({role: 'admin'}, function(resp){
  
        deferred.resolve(resp);
  
      });

      return deferred.promise;

    },

    students : function(){

      var deferred = $q.defer();
  
      roleResource.query({role: 'student'}, function(resp){
  
        deferred.resolve(resp);
  
      });

      return deferred.promise;

    },

    academicServices : function(){

      var deferred = $q.defer();
  
      roleResource.query({role: 'acdServ'}, function(resp){
  
        deferred.resolve(resp);
  
      });

      return deferred.promise;

    },

    externalVerifier : function(){

      var deferred = $q.defer();
  
      roleResource.query({role: 'extVer'}, function(resp){
  
        deferred.resolve(resp);
  
      });

      return deferred.promise;

    },

    studentRecords : function(){

      var deferred = $q.defer();
  
      roleResource.query({role: 'studRec'}, function(resp){
  
        deferred.resolve(resp);
  
      });

      return deferred.promise;
    
    }

  };


  function buildUserLists(callback){

    listsBuilt = true;

    factory.query().then(function (data){
  
      angular.forEach(data.users, function(value, key){

        var rolesForAUser = [];

        angular.forEach(value.roles_users, function(value, key){

          // pack all roles for each user into an array
          rolesForAUser.push(value.role_id);

        });
        
        // check array for each role ID and push it into the correct array list.
        if(rolesForAUser.indexOf("1") != -1) admins.push(value);
        if(rolesForAUser.indexOf("2") != -1) programLeaders.push(value);
        if(rolesForAUser.indexOf("3") != -1) teachers.push(value);
        if(rolesForAUser.indexOf("4") != -1) academicServices.push(value);
        if(rolesForAUser.indexOf("5") != -1) externalVerifier.push(value);
        if(rolesForAUser.indexOf("6") != -1) studentRecords.push(value);
        if(rolesForAUser.indexOf("7") != -1) students.push(value);
    
      });
      
      return callback;
  
    }, function (error){
  
      console.log('ruh roh');
  
    });

  }

  return factory;

}]);
// start validatorFactories.js

globals.factory('validatorFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {
  
  var validatorResource = $resource('api/:collection/validate/:field.json',

    {

      collection : '@collection', 

      field : '@field' 
    
    },
    
    {

      'validate': {method: 'POST'} 

    });

  var factory = function (col, fld, payload) {
    
      // validate some data
    
      var deferred = $q.defer();
    
      validatorResource.validate({collection : col, field : fld}, payload, function(resp) {
    
          deferred.resolve(resp);
    
        });
    
      return deferred.promise;
         
  };

  // add methods for getting and setting selected semester
  
  return factory;

}]);
// globalConfig.js

globals.constant('globalConfig', {

    ver: '0', 

    longDateFormat: 'DD, d  MM, yy',

    shortDateFormat: ''

  });

// modulesModule.js Starts here 

var modulesModule = angular.module('modulesModule', []);

modulesModule.config(['$stateProvider','$urlRouterProvider','$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider){    

    var programModules = {
        name: "programModules",
        parent: 'programView',
        url: "/modules",
        templateUrl: "app/webroot/tpl/program.moduleList.tpl.html",
        controller: "prgModulesController as prgModules"
    },
    moduleEdit = {
        name: "moduleEdit",
        parent: 'programModules',
        url: "/:module/edit",
        templateUrl: "app/webroot/tpl/moduleForm.tpl.html",
        controller: "moduleEditController as modForm"
    },
    moduleCreate = {
        name: "moduleCreate",
        parent: 'programModules',
        url: "/create",
        templateUrl: "app/webroot/tpl/moduleForm.tpl.html",
        controller: "moduleCreateController as modForm"
    };

    $stateProvider
        .state(programModules)
        .state(moduleEdit)
        .state(moduleCreate);

}]);


modulesModule.controller('moduleCreateController', ['$scope','$q','$timeout','$stateParams','$state','modulesFactory', 'messageCenterService', function($scope, $q, $timeout, $stateParams, $state, modulesFactory, messageCenterService) {
  
    console.log('moduleCreateController');
    
    var ctrl = this;

    ctrl.mode = 'create';

    ctrl.modal = true; 

    ctrl.message = '';

    ctrl.createItem = function(){

        var prgs = $scope.$parent.prgModules.programList;

        for (var i = prgs.length - 1; i >= 0; i--) {
          
            if(prgs[i].slug == $stateParams.program){

                ctrl.item.program_id = prgs[i].id;
                
            }
      
        }
  
        modulesFactory.create($stateParams, ctrl.item)

        .then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the program.", { timeout: 6000 });

            } else {

                ctrl.message = 'Module Created';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);

            }

            $scope.$parent.prgModules.loadList();

        },

        function(error){

            console.log(error);

        });

    };
  
}]);
modulesModule.controller('moduleEditController', ['$scope','$q','$stateParams','$state','$timeout','modulesFactory', 'messageCenterService', function($scope, $q, $stateParams, $state, $timeout, modulesFactory, messageCenterService) {
  
  console.log('moduleEditController');

  var ctrl = this;

    ctrl.mode = 'edit';

    ctrl.modal = true; 

    ctrl.deleting = false;

    ctrl.message = '';

    ctrl.toggleDelete = function(){

        ctrl.deleting = !ctrl.deleting;

    };

    ctrl.deleteItem = function(){
  
        modulesFactory.remove($stateParams)

        .then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while deleting the module.", { timeout: 6000 });

                console.log(data);

            } else {
                
                ctrl.message = 'Module Deleted';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);
            }
        },

        function(error){

            console.log(error);

        });

    };
    
    ctrl.updateItem = function(){
  
        modulesFactory.update($stateParams, ctrl.item)

        .then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the module.", { timeout: 6000 });

                console.log(data);

            } else {

                ctrl.message = 'Module Updated';

                $timeout(function(){

                    $state.go('moduleEdit', {module: data.module.slug});

                    ctrl.message = '';

                }, 1500);

            }

        },

        function(error){

            console.log(error);

        });

    };

    modulesFactory.get($stateParams).then(function(data){

        if (data._error.code == 1) {

            messageCenterService.add('danger', "Something went wrong while retrieving the module.", { timeout: 6000 });

        } else {

            ctrl.item = data.module;

        }

    },

    function(error){

        console.log(error);

    });

}]);
modulesModule.controller('prgModulesController', ['$scope', '$q', '$stateParams', 'modulesFactory', 'programsFactory', 'messageCenterService', function ($scope, $q, $stateParams, modulesFactory, programsFactory, messageCenterService) {

    console.log('prgModuleController instantiated');

    var ctrl = this;

    ctrl.loaded = false;

    ctrl.loadList = function(){

        modulesFactory.query($stateParams).then(function (data){

            ctrl.loaded = true;

            ctrl.programModuleList = data.modules;

        }, function (error){

            console.log('ruh roh');

        });

    };

    ctrl.loadList();

    programsFactory.query().then(function (data){

        ctrl.programList = data.programs;

    }, function (error){
        
        console.log('ruh roh');

    });
    
}]);
// modulesFactory.js

modulesModule.factory('modulesFactory', ['$http','$resource','$q', function($http, $resource, $q) {

  // deprecated endpoint
  var allModulesResource = $resource('api/modules.json', {},

    {

      'query' : {method: 'GET', isArray: false, cache: true}

    });

  var modulesResource = $resource('api/program/:prg/modules.json', {prg: '@prg'},

    {

      'query' : {method: 'GET'},

      'create': {method: 'POST'}

    });

  var moduleResource = $resource('api/program/:prg/module/:module.json', {prg: '@prg', module: '@module'},

    {

      'get': {method: 'GET'},      

      'update': {method: 'POST'},

      'remove': {method: 'POST'} 

    });


  var factory = {

    // deprecated
    queryAll : function () {

      var deferred = $q.defer();

      allModulesResource.query({},

        function (resp) {

          deferred.resolve(resp);

        });

      return deferred.promise;

    },

    query : function (params) {

      var deferred = $q.defer();

      modulesResource.query({prg: params.program},

        function (resp) {

          deferred.resolve(resp);

        });

      return deferred.promise;

    },

    get: function (params) {

      var deferred = $q.defer();

      moduleResource.get({prg: params.program, module: params.module},

        function (resp) {

          deferred.resolve(resp);

        });

      return deferred.promise;

    },

    create : function (params, payload) {

      var deferred = $q.defer();

      modulesResource.create({prg: params.program}, payload, function(resp){

        deferred.resolve(resp);

      });

      return deferred.promise;

    },

    update : function (params, payload) {

      payload._method = 'put';

      var deferred = $q.defer();

      moduleResource.update({prg: params.program, module: params.module}, payload, function(resp){

        deferred.resolve(resp);

      });

      return deferred.promise;

    },

    remove : function (params) {

      var deferred = $q.defer();

      var payload = { _method: 'delete' };

      moduleResource.remove({prg: params.program, module: params.module}, payload, function(resp){

        deferred.resolve(resp);

      });

      return deferred.promise;

    }

  };

  return factory;

}]);
// myclassesModule.js Starts here 

var myclassesModule = angular.module('myclassesModule', ['smart-table','ngResource']);

myclassesModule.config(['$stateProvider','$urlRouterProvider','$locationProvider',function($stateProvider, $urlRouterProvider, $locationProvider){
    
  $urlRouterProvider.when('/semester/{semester}/myclasses', '/semester/{semester}/myclasses/classes');
  
  $urlRouterProvider.when('/semester/{semester}/myclasses/class/{class}', '/semester/{semester}/myclasses/class/{class}/assignments');

  // $state.go('myClasses', {semester: $scope.$parent.globals.currentSemester});

  // routes for the myclasses group of pages
  var mcRoot = { // fugly!
    name: "myRoot",
    url: "/myclasses",
    templateUrl: "app/webroot/tpl/mc.tabFrame.tpl.html",
    controller: "mcRootController"
  },
  myClassesSemesterView = {
    name: "myClassesSemesterView",
    url: "/semester/:semester",
    controller: "semesterController as sem",
    abstract: true,
    template: '<ui-view/>'
  }, 
  myClasses = {
    name: "myClasses",
    parent: 'myClassesSemesterView',
    url: "/myclasses",
    templateUrl: "app/webroot/tpl/mc.tabFrame.tpl.html",
    controller: "myClassesController as mc"
  },
  myClassesSubmissions = {
    name: "myClassesSubmissions",
    parent: 'myClasses',
    url: "/submissions",
    templateUrl: "app/webroot/tpl/mc.submissions.tpl.html",
    controller: "myClassesSubmissionsController as mcSubs"
  },
  myClassesClasses = {
    name: "myClassesClasses",
    parent: 'myClasses',
    url: "/classes",
    templateUrl: "app/webroot/tpl/mc.classes.tpl.html",
    controller: "myClassesClassesController as mcClasses"
  },
  myClassesClassView = {
    name: "myClassesClassView",
    parent: 'myClasses',
    url: "/class/:class",
    templateUrl: "app/webroot/tpl/mc.class.tabFrame.tpl.html",
    controller: "myClassesClassController as mcClass"
  },
  myClassesStudentSubmissions = {
    name: "myClassesStudentSubmissions",
    parent: 'myClasses',
    url: "/submissions/:id",
    templateUrl: "app/webroot/tpl/mc.student.submissions.tpl.html",
    controller: "myClassesStudentSubmissionsController as mcStuSubs"
  },
  myClassesClassSubmissions = {
    name: "myClassesClassSubmissions",
    parent: 'myClassesClassView',
    url: "/submissions",
    templateUrl: "app/webroot/tpl/mc.class.submissions.tpl.html",
    controller: "myClassesClassSubmissionsController as mcClsSubs"
  },
  myClassesClassAssignments = {
    name: "myClassesClassAssignments",
    parent: 'myClassesClassView',
    url: "/assignments",
    templateUrl: "app/webroot/tpl/mc.class.assignments.tpl.html",
    controller: "myClassesClassAssignmentsController as mcClsAssign"
  },
  myClassesClassAssignmentEdit = {
    name: "myClassesClassAssignmentEdit",
    parent: 'myClassesClassAssignments',
    url: "/:assg/edit",
    templateUrl: "app/webroot/tpl/mc.class.assignmentForm.tpl.html",
    controller: "mcAssignmentEditController as mcAssignForm"
  },
  myClassesClassAssignmentCreate = {
    name: "myClassesClassAssignmentCreate",
    parent: 'myClassesClassAssignments',
    url: "/create",
    templateUrl: "app/webroot/tpl/mc.class.assignmentForm.tpl.html",
    controller: "mcAssignmentCreateController as mcAssignForm"
  },
  myClassesClassEnrollment = {
    name: "myClassesClassEnrollment",
    parent: 'myClassesClassView',
    url: "/enrollment",
    templateUrl: "app/webroot/tpl/mc.class.enrollment.tpl.html",
    controller: "myClassesClassEnrollmentController as mcClsEnroll"
  },
  myClassesClassFeedback = {
    name: "myClassesClassFeedback",
    parent: 'myClassesClassView',
    url: "/feedback",
    templateUrl: "app/webroot/tpl/mc.class.feedback.tpl.html",
    controller: "myClassesClassFeedbackController as mcClsFeed"
  },
  myClassesClassDocumentation = {
    name: "myClassesClassDocumentation",
    parent: 'myClassesClassView',
    url: "/documentation",
    templateUrl: "app/webroot/tpl/mc.class.documentation.tpl.html",
    controller: "myClassesClassDocumentationController as mcClsDoc"
  };

  $stateProvider
    .state(mcRoot)
    .state(myClassesSemesterView)
    .state(myClasses)
    .state(myClassesSubmissions)
    .state(myClassesClasses)
    .state(myClassesClassView)
    .state(myClassesClassAssignments)
    .state(myClassesClassSubmissions)
    .state(myClassesClassEnrollment)
    .state(myClassesClassFeedback)
    .state(myClassesClassDocumentation)
    .state(myClassesStudentSubmissions)
    .state(myClassesClassAssignmentEdit)
    .state(myClassesClassAssignmentCreate);
}]);
// semesterProgramClassAssignmentController.js

myclassesModule.controller('mcAssignmentCreateController', ['$scope', '$q', '$stateParams', '$state', '$timeout', 'myClassesAssignmentFactory', 'messageCenterService', function ($scope, $q, $stateParams, $state, $timeout, myClassesAssignmentFactory, messageCenterService) {
  
    console.log('mcAssignmentCreateController instantiated');

    var ctrl = this;

    ctrl.mode = 'create';

    ctrl.modal = true; 

    ctrl.message = '';

    ctrl.createItem = function(){

        ctrl.item.class_id = $scope.$parent.mcClass.classSelection.id;

        ctrl.item.semester_id = $scope.$parent.sem.semesterSelection.id;

        myClassesAssignmentFactory.create($stateParams, ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the user.", { timeout: 6000 });

                console.log(data);

            } else {

                ctrl.message = 'User Created';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);

            }

            $scope.$parent.mcClsAssign.loadList();

        },

        function(error){

            console.log(error);

        });

    };

}]);
// semesterProgramClassAssignmentController.js

myclassesModule.controller('mcAssignmentEditController', ['$scope', '$q', '$stateParams', '$state', '$timeout', 'myClassesAssignmentFactory', 'messageCenterService', function ($scope, $q, $stateParams, $state, $timeout, myClassesAssignmentFactory, messageCenterService) {
  
    console.log('mcAssignmentEditController instantiated');

    var ctrl = this;

    ctrl.mode = 'edit';

    ctrl.modal = true; 

    ctrl.deleting = false;

    ctrl.message = '';

    ctrl.toggleDelete = function(){

        ctrl.deleting = !ctrl.deleting;

    };

    ctrl.deleteItem = function(){
  
        myClassesAssignmentFactory.remove($stateParams).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while deleting the assignment.", { timeout: 6000 });

                console.log(data);

            } else {
                
                ctrl.message = 'Assignment Deleted';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);
            }

            $scope.$parent.mcClsAssign.loadList();

        },

        function(error){

            console.log(error);

        });

    };
    
    ctrl.updateItem = function(){
  
        myClassesAssignmentFactory.update($stateParams, ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the assignment.", { timeout: 6000 });

                console.log(data);

            } else {

                ctrl.message = 'Assignment Updated';

                $timeout(function(){

                    ctrl.message = '';

                }, 1500);

            }

            $scope.$parent.mcClsAssign.loadList();

        },

        function(error){

            console.log(error);

        });

    };

    myClassesAssignmentFactory.get($stateParams).then(function(data){

        if (data._error.code == 1) {

            messageCenterService.add('danger', "Something went wrong while retrieving the assignment.", { timeout: 6000 });

        } else {

            ctrl.item = data.assignment;

        }

    },

    function(error){

        console.log(error);

    });

}]);
// semesterProgramClassAssignmentController.js

myclassesModule.controller('mcAssignmentBlockController', ['$scope', '$q', '$stateParams', 'myClassesAssignmentFactory', 'messageCenterService', function ($scope, $q, $stateParams, myClassesAssignmentFactory, messageCenterService) {
  
    console.log('mcAssignmentBlockController instantiated');

    var ctrl = this;

    ctrl.viewingSubs = false;

    ctrl.loading = false;

    ctrl.selectedForDownload = [];

    ctrl.toggleSubs = function(assign){
 
        ctrl.viewingSubs = !ctrl.viewingSubs;
 
        ctrl.selectedForDownload = [];

        getAssignmentData(assign);
 
    };

    ctrl.manyToDownload = function(){

        // do this in the template instead
 
        if(ctrl.selectedForDownload.length > 1) return true;
 
        return false;
  
    };

    function getAssignmentData(assign){

        if(assign) {

            ctrl.loading = true;

            $stateParams.assg = assign;

            myClassesAssignmentFactory.get($stateParams)
            
            .then(function (data){

                if (data._error.code == 1) {

                    messageCenterService.add('danger', "Something went wrong while saving the program.", { timeout: 6000 });

                } else {

                    ctrl.assignmentSubsGrid = data.submissions;

                    ctrl.assignmentSubsDisplayList = [].concat(ctrl.assignmentSubsGrid);

                    ctrl.loading = false;
                }

            }, 

            function (error){

                console.log('Error');

                console.log(error);

            });
        }

    }

}]);
// start mcRootController.js

myclassesModule.controller('mcRootController', ['$scope', '$state', '$timeout', function ($scope, $state, $timeout) {
  
    console.log('mcRootController instantiated');

    $timeout(function(){

        $state.go('myClasses', {semester: $scope.$parent.globals.currentSemester});

    }, 1500);   

}]);
// myClassesClassAssignmentsController.js

myclassesModule.controller('myClassesClassAssignmentsController', ['$scope', '$q', '$stateParams', 'myClassesAssignmentFactory', function ($scope, $q, $stateParams, myClassesAssignmentFactory) {
  
    console.log('myClassesClassAssignmentsController instantiated');

    var ctrl = this;

    ctrl.loaded = false;

    ctrl.assignmentList = [];

    ctrl.loadList = function(){

        myClassesAssignmentFactory.query($stateParams).then(function (data){

            ctrl.assignmentList = data.assignments;

            ctrl.loaded = true;

        }, function (error){

            console.log('ruh roh');

        });

    };

    ctrl.loadList();
  
}]);
// myClassesClassController.js

myclassesModule.controller('myClassesClassController', ['$scope', '$q', 'myClassesFactory', 'globalsFactory', '$stateParams', '$state', function ($scope, $q, myClassesFactory, globalsFactory, $stateParams, $state) {
 
    console.log('myClassesClassController instantiated');
  
    var ctrl = this;

    ctrl.classSelection = {};

    myClassesFactory.get($stateParams)

    .then(function (data){

        ctrl.classSelection = data.classes[0];
    
    }, function (error){
    
        console.log('ruh roh');
  
    });

    ctrl.classSwitcher = function($item, $model){

        page = $state.current.name;

        $state.go(page, {class: $item.slug});

    };

}]);

// myClassesClassDocumentationController.js

myclassesModule.controller('myClassesClassDocumentationController', ['$scope', '$q', '$stateParams', 'myClassesDocumentationFactory', function ($scope, $q, $stateParams, myClassesDocumentationFactory) {

  console.log('myClassesClassDocumentationController instantiated');

  var ctrl = this;

  myClassesDocumentationFactory.query($stateParams)

    .then(function (data){

        // if (data.classes && data.classes.length > 0) ctrl.myClassesExist = true; 

        // ctrl.classList =  data.classes;

    }, function (error){

        console.log('ruh roh: error in myclasses controller');

    });
  
  // single class stuff here.

}]);
// myClassesClassEnrollmentController.js

myclassesModule.controller('myClassesClassEnrollmentController', ['$scope', '$q', '$stateParams', 'myClassesEnrollmentFactory', function ($scope, $q, $stateParams, myClassesEnrollmentFactory) {

  console.log('myClassesClassEnrollmentController instantiated');

  var ctrl = this;

  ctrl.studentList = [];

  ctrl.displayList = [];


  myClassesEnrollmentFactory.query($stateParams)

    .then(function (data){

        ctrl.studentList =  data.students;

        ctrl.displayList =  data.students;

    }, function (error){

        console.log('ruh roh: error in myclasses controller');

    });

}]);
// myClassesClassFeedbackController.js

myclassesModule.controller('myClassesClassFeedbackController', ['$scope', '$q', '$stateParams', 'myClassesFeedbackFactory', function ($scope, $q, $stateParams, myClassesFeedbackFactory) {

    console.log('myClassesClassFeedbackController instantiated');

    var ctrl = this;

    myClassesFeedbackFactory.query($stateParams)

    .then(function (data){

        // console.log('feedback', data);

        // if (data.classes && data.classes.length > 0) ctrl.myClassesExist = true; 

        // ctrl.classList =  data.classes;

    }, function (error){

        console.log('ruh roh: error in myclasses controller');

    });

    // single class stuff here.

}]);
// myClassesClassSubmissionsController.js

myclassesModule.controller('myClassesClassSubmissionsController', ['$scope', '$q', '$stateParams', 'myClassesSubmissionsFactory', function ($scope, $q, $stateParams, myClassesSubmissionsFactory) {

    console.log('myClassesClassSubmissionsController instantiated');

    var ctrl = this;

    ctrl.subsList = [];
  
    ctrl.displayList = [];

    ctrl.loaded = false;

    myClassesSubmissionsFactory.classSubs($stateParams)

    .then(function (data){

      if (data._error != 1) {

        ctrl.subsList = data.submissions;

      } else {

        // error here

      }

      ctrl.loaded = true;

    }, function (error){

      console.log('ruh roh');

});

}]);
// myClassesClassesController.js

myclassesModule.controller('myClassesClassesController', ['$scope', '$q', 'myClassesFactory', '$stateParams', function ($scope, $q, myClassesFactory, $stateParams) {

  console.log('myClassesClassesController instantiated');

  var ctrl = this;

  ctrl.myClassesExist = false;

  ctrl.classList = [];

  // pass in semester from "selectedSemester"
  myClassesFactory.query($stateParams)

    .then(function (data){

      if (data.classes && data.classes.length > 0) ctrl.myClassesExist = true; 

      ctrl.classList =  data.classes;

    }, function (error){

      console.log('ruh roh: error in myclasses controller');

    });

}]);
// start myClassesControllers.js

myclassesModule.controller('myClassesController', ['$scope', '$q', '$state', function ($scope, $q, $state) {
  
  console.log('myClassesController instantiated');

  var ctrl = this;

}]);
// myClassesClassSubmissionsController.js

myclassesModule.controller('myClassesStudentSubmissionsController', ['$scope', '$q', '$stateParams', 'myClassesSubmissionsFactory', function ($scope, $q, $stateParams, myClassesSubmissionsFactory) {

    console.log('myClassesStudentSubmissionsController instantiated');

    var ctrl = this;

    ctrl.loaded = false;

    ctrl.subsList =  [];
    
    ctrl.displayList = [];

    ctrl.submissionsExist = false;

    myClassesSubmissionsFactory.student($stateParams)

    .then(function (data){

        if (data.submissions && data.submissions.length > 0) ctrl.submissionsExist = true; 

        ctrl.subsList =  data.submissions;

        ctrl.displayList =  data.submissions;

        if(data.submissions.length > 0) {

            // ghetto get user info
            
            ctrl.student = data.submissions[0].student;

        }

        ctrl.loaded = true;

    }, function (error){

        console.log('ruh roh: error in myclasses controller');

    });

    // single class stuff here.

}]);
// myClassesSubmissionsController.js

myclassesModule.controller('myClassesSubmissionsController', ['$scope', '$q', '$stateParams', 'myClassesSubmissionsFactory', function ($scope, $q, $stateParams, myClassesSubmissionsFactory) {

  console.log('myClassesSubmissionsController instantiated');

  var ctrl = this;

  ctrl.loaded = false;

  ctrl.subsList = [];
  
  ctrl.displayList = [];

  myClassesSubmissionsFactory.all($stateParams).then(function (data){
  
      if (data._error != 1) {

        ctrl.subsList = data.submissions;

        ctrl.displayList = data.submissions;

      } else {

        // error here

      }

      ctrl.loaded = true;
  
    }, function (error){
  
      console.log('ruh roh');
  
    });

}]);
// really not done. Break this up, flatten the methods

myclassesModule.factory('myClassesAssignmentFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {

  var mcAssignmentResource = $resource('api/semester/:semester/myclasses/:class/assignment/:assg.json', 

    { semester: '@semester', class: '@class', assg: '@assg'},
    
    {

      'get': {method: 'GET', isArray: false},

      'update': {method: 'POST', isArray: false},

      'remove': {method: 'POST', isArray: false} 

    });

   var mcAssignmentsResource = $resource('api/semester/:semester/myclasses/:class/assignments.json', 

    { semester: '@semester', class: '@class'},
    
    {

      'query': {method: 'GET', isArray: false},

      'create': {method: 'POST', isArray: false}

    });


  var factory = {

    get : function(params) {
   
      var deferred = $q.defer();

      mcAssignmentResource.get({semester: params.semester, class: params.class, assg: params.assg},
   
        function (resp) {
   
          deferred.resolve(resp);
   
        });

      return deferred.promise;
   
    },
   
    query : function(params) {
   
      var deferred = $q.defer();

      mcAssignmentsResource.query({semester: params.semester, class: params.class},
   
        function (resp) {
   
          deferred.resolve(resp);
   
        });

      return deferred.promise;
   
    },

    update : function(params, payload){

      var deferred = $q.defer();

      payload._method = 'put';

      mcAssignmentResource.update({semester: params.semester, class: params.class, assg: params.assg}, payload,
   
        function (resp) {
   
          deferred.resolve(resp);
   
        });

      return deferred.promise;

    },

    create : function(params, payload){

      var deferred = $q.defer();

      mcAssignmentsResource.create({semester: params.semester, class: params.class}, payload, 
   
        function (resp) {
   
          deferred.resolve(resp);
   
        });

      return deferred.promise;

    },

    remove : function(params){

      var deferred = $q.defer();

      var payload = { _method: 'delete' };

      mcAssignmentResource.remove({semester: params.semester, class: params.class, assg: params.assg}, payload, function (resp) {
   
          deferred.resolve(resp);
   
        });

      return deferred.promise;

    }
  
  };
  
  return factory;

}]);
// really not done. Break this up, flatten the methods

myclassesModule.factory('myClassesDocumentationFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {

  var mcDocResource = $resource('api/semester/:semester/myclasses/:class/documentation/:id.json', 

    { semester: '@semester', class: '@class', id: '@id' },
    
    {

      'get': {method: 'GET', isArray: false, cache: true},

      'update': {method: 'POST'},

      'remove': {method: 'POST'} 

    });

   var mcDocsResource = $resource('api/semester/:semester/myclasses/:class/documentation.json', 

    { semester: '@semester', class: '@class'},
    
    {

      'query': {method: 'GET', isArray: false, cache: true},

      'create': {method: 'POST'}

    });


  var factory = {

    get : function(params) {
   
      var deferred = $q.defer();

      mcDocResource.query({semester: params.semester, class: params.class, id: params.id},
   
        function (resp) {
   
          deferred.resolve(resp);
   
        });

      return deferred.promise;
   
    },
   
    query : function(params) {
   
      var deferred = $q.defer();

      mcDocsResource.query({semester: params.semester, class: params.class},
   
        function (resp) {
   
          deferred.resolve(resp);
   
        });

      return deferred.promise;
   
    },

    update : function(params, payload){

      var deferred = $q.defer();

      payload._method = 'put';

      mcDocResource.query({semester: params.semester, class: params.class, id: params.id},
   
        function (resp) {
   
          deferred.resolve(resp);
   
        });

      return deferred.promise;

    },

    create : function(params, payload){

      var deferred = $q.defer();

      mcDocsResource.query({semester: params.semester, class: params.class}, payload, 
   
        function (resp) {
   
          deferred.resolve(resp);
   
        });

      return deferred.promise;

    },

    remove : function(params){

      var deferred = $q.defer();

      var payload = { _method: 'delete' };

      mcDocResource.remove({semester: params.semester, class: params.class, id: params.id}, payload, function (resp) {
   
          deferred.resolve(resp);
   
        });

      return deferred.promise;

    }
  
  };
  
  return factory;

}]);
// really not done. Break this up, flatten the methods

myclassesModule.factory('myClassesEnrollmentFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {

   var mcEnrolledResource = $resource('api/semester/:semester/myclasses/:class/enrollment.json', 

    { semester: '@semester', class: '@class'},
    
    {

      'query': {method: 'GET', isArray: false, cache: true}

    });


  var factory = {
   
    query : function(params) {
   
      var deferred = $q.defer();

      mcEnrolledResource.query({semester: params.semester, class: params.class},
   
        function (resp) {
   
          deferred.resolve(resp);
   
        });

      return deferred.promise;
   
    }

  };
  
  return factory;

}]);
// really not done. Break this up, flatten the methods

myclassesModule.factory('myClassesFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {

  var myClassesResource = $resource('api/semester/:semester/myclasses.json', {semester: '@semester'},
    
    {'query': {method: 'GET', isArray: false, cache: true} });

  var myClassResource = $resource('api/semester/:semester/myclasses/:class.json',

    { semester: '@semester', prg: '@prg', class: '@class' },

    {

      'get': {method: 'GET'}

    });


  var factory = {
   
    query : function(params) {
   
      var deferred = $q.defer();

      myClassesResource.query({semester: params.semester},
   
        function (resp) {
   
          deferred.resolve(resp);
   
        });

      return deferred.promise;
   
    },

    get: function (params) {

      var deferred = $q.defer();

      myClassResource.get({semester: params.semester, class: params.class},

        function (resp) {

          deferred.resolve(resp);

        });

      return deferred.promise;

    }
  
  };
  
  return factory;

}]);
// really not done. Break this up, flatten the methods

myclassesModule.factory('myClassesFeedbackFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {

  var mcFeedbackResource = $resource('api/semester/:semester/myclasses/:class/feedback/:id.json', 

    { semester: '@semester', class: '@class', id: '@id' },
    
    {

      'get': {method: 'GET', isArray: false, cache: true},

      'update': {method: 'POST'},

      'remove': {method: 'POST'} 

    });

   var mcFeedbacksResource = $resource('api/semester/:semester/myclasses/:class/feedback.json', 

    { semester: '@semester', class: '@class'},
    
    {

      'query': {method: 'GET', isArray: false, cache: true},

      'create': {method: 'POST'}

    });


  var factory = {

    get : function(params) {
   
      var deferred = $q.defer();

      mcFeedbackResource.query({semester: params.semester, class: params.class, id: params.id},
   
        function (resp) {
   
          deferred.resolve(resp);
   
        });

      return deferred.promise;
   
    },
   
    query : function(params) {
   
      var deferred = $q.defer();

      mcFeedbacksResource.query({semester: params.semester, class: params.class},
   
        function (resp) {
   
          deferred.resolve(resp);
   
        });

      return deferred.promise;
   
    },

    update : function(params, payload){

      var deferred = $q.defer();

      payload._method = 'put';

      mcFeedbackResource.query({semester: params.semester, class: params.class, id: params.id},
   
        function (resp) {
   
          deferred.resolve(resp);
   
        });

      return deferred.promise;

    },

    create : function(params, payload){

      var deferred = $q.defer();

      mcFeedbacksResource.query({semester: params.semester, class: params.class}, payload, 
   
        function (resp) {
   
          deferred.resolve(resp);
   
        });

      return deferred.promise;

    },

    remove : function(params){

      var deferred = $q.defer();

      var payload = { _method: 'delete' };

      mcFeedbackResource.remove({semester: params.semester, class: params.class, id: params.id}, payload, function (resp) {
   
          deferred.resolve(resp);
   
        });

      return deferred.promise;

    }
  
  };
  
  return factory;

}]);
// really not done. Break this up, flatten the methods

myclassesModule.factory('myClassesSubmissionsFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {

    var mcAllSubsResource = $resource('api/semester/:semester/myclasses/submissions.json', 

    { semester: '@semester'},
    
    {

        'query': {method: 'GET'}

    });

    var mcClassSubsResource = $resource('api/semester/:semester/myclasses/:class/submissions.json', 

    { semester: '@semester', class: '@class' },
    
    {

        'query': {method: 'GET'}

    });

    var mcStuSubsResource = $resource('api/semester/:semester/myclasses/:class/submissions/:id.json', 

    { semester: '@semester', class: '@class', id: '@id' },
    
    {

        'query': {method: 'GET'}

    });


    var factory = {

        all : function(params) {
       
            var deferred = $q.defer();

            mcAllSubsResource.query({semester: params.semester},
       
            function (resp) {
       
                deferred.resolve(resp);
       
            });

            return deferred.promise;
       
        },
   
        classSubs : function(params) {
       
            var deferred = $q.defer();

            mcClassSubsResource.query({semester: params.semester, class: params.class},

            function (resp) {

                deferred.resolve(resp);

            });

            return deferred.promise;
       
        },

        student : function(params) {
       
            var deferred = $q.defer();

            mcStuSubsResource.query({semester: params.semester, class: params.class, id: params.id},

            function (resp) {

                deferred.resolve(resp);

            });

            return deferred.promise;
       
        }
  
    };
  
  return factory;

}]);
// APP.js Starts here 

// This file declares and sets all the module dependencies at the top level.
// Also declares all routes, by module. This is simplfy making modules per user type.

// define all modules
var programsModule = angular.module('programsModule', ['smart-table','ngResource']);

programsModule.config(['$stateProvider','$urlRouterProvider','$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider){
  
	var programs = {
		name: "programs",
		url: "/programs",
		templateUrl: "app/webroot/tpl/programList.tpl.html",
		controller: "programsController as programs"
    },
    programEdit = {
		name: "programEdit",
		parent: "programs",
		url: "/:program/edit",
		templateUrl: "app/webroot/tpl/programForm.tpl.html",
		controller: "programEditController as prgForm"
    },
    programCreate = {
		name: "programCreate",
		parent: "programs",
		url: "/create",
		templateUrl: "app/webroot/tpl/programForm.tpl.html",
		controller: "programCreateController as prgForm"
    };

    $stateProvider
		.state(programs)
		.state(programEdit)
		.state(programCreate);

}]);
programsModule.controller('programCreateController', ['$scope','$q','$timeout','$state','programsFactory', 'usersFactory', 'semesterFactory', 'messageCenterService', function($scope, $q, $timeout, $state, programsFactory, usersFactory, semesterFactory, messageCenterService) {
  
    console.log('programCreateController');
    
    var ctrl = this;

    ctrl.mode = 'create';

    ctrl.modal = true; 

    ctrl.message = '';

    ctrl.program_authorizations = [];

    ctrl.createItem = function(){

        ctrl.item.program_authorizations = [];

        for (var i = ctrl.program_authorizations.length - 1; i >= 0; i--) {

            var temp = {
            
                program_leader_id: ctrl.program_authorizations[i]
            
            };
            
            ctrl.item.program_authorizations.push(temp);
        
        }
  
        programsFactory.create(ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the program.", { timeout: 6000 });

            } else {

                ctrl.message = 'Program Created';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);

            }

            $scope.$parent.programs.loadList();

        },

        function(error){

            console.log(error);

        });

    };
    
}]);
programsModule.controller('programEditController', ['$scope','$q','$stateParams','$state','$timeout','programsFactory', 'usersFactory', 'semesterFactory', 'messageCenterService', function($scope, $q, $stateParams, $state, $timeout, programsFactory, usersFactory, semesterFactory, messageCenterService) {
  
  console.log('programEditController');

  var ctrl = this;

    ctrl.mode = 'edit';

    ctrl.modal = true; 

    ctrl.deleting = false;

    ctrl.message = '';

    ctrl.toggleDelete = function(){

        ctrl.deleting = !ctrl.deleting;

    };

        ctrl.onSelect = function($item, $model){

        ctrl.savingLeader = true;

        var auth = {
        
            program_authorizations : [

                {
                    program_id: ctrl.item.id,

                    program_leader_id: $model
                }

            ]
        
        };

        programsFactory.addAuth(auth).then(function(data){

            ctrl.savingLeader = false;

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while adding the program leader.", { timeout: 6000 });

            } 

            $scope.$parent.programs.loadList();

        },

        function(error){

            console.log(error);

        });

    };

    ctrl.onRemove = function($item, $model){

        ctrl.savingLeader = true;

        var auth = {
        
            program_authorizations : [

                {
                    program_id: ctrl.item.id,

                    program_leader_id: $model
                }

            ]
        
        };

        programsFactory.removeAuth(auth).then(function(data){

            ctrl.savingLeader = false;

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while removing the program leader.", { timeout: 6000 });

            }

            $scope.$parent.programs.loadList();

        },

        function(error){

            console.log(error);

        });

    };

    ctrl.deleteItem = function(){
  
        programsFactory.remove($stateParams).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while deleting the program.", { timeout: 6000 });

            } else {
                
                ctrl.message = 'Program Deleted';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);
            }

            $scope.$parent.programs.loadList();
        },

        function(error){

            console.error(error);

        });

    };
    
    ctrl.updateItem = function(){
  
        programsFactory.update($stateParams, ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the program.", { timeout: 6000 });

            } else {

                ctrl.message = 'Program Updated';

                $timeout(function(){

                    ctrl.message = '';

                    $state.go('programEdit', {program: data.program.slug});

                }, 1500);

            }

            $scope.$parent.programs.loadList();

        },

        function(error){

            console.error(error);

        });

    };

    programsFactory.get($stateParams).then(function(data){

        if (data._error.code == 1) {

            messageCenterService.add('danger', "Something went wrong while retrieving the program.", { timeout: 6000 });

        } else {

            ctrl.item = data.program;

            ctrl.program_authorizations = [];

            for (var i = ctrl.item.program_authorizations.length - 1; i >= 0; i--) {
          
                ctrl.program_authorizations.push(ctrl.item.program_authorizations[i].program_leader_id);
          
            }

        }

    },

    function(error){

        console.error(error);

    });

}]);
programsModule.controller('programsController', ['$scope','$q','programsFactory', 'usersFactory', 'semesterFactory', 'messageCenterService', function($scope, $q, programsFactory, usersFactory, semesterFactory, messageCenterService) {
  
    console.log('programsController');

    var ctrl = this;

    ctrl.loaded = false;

    ctrl.loadList = function(){

        programsFactory.query().then(function (data){

            ctrl.programList = data.programs;

            ctrl.loaded = true;

        }, function (error){

            console.log('ruh roh');

        }); 

    };

    ctrl.loadList();

    ctrl.programLeaders = [];

    usersFactory.programLeaders().then(function (data){

        ctrl.programLeaders = data.roles;
    
    }, function (error){
    
        console.log('ruh roh');
    
    });

}]);
// _semesterProgramModule.js 

var semesterProgramModule = angular.module('semesterProgramModule', ['ui.router','smart-table','ngResource','usersModule']);

// routes for semester > program > class pathways
semesterProgramModule.config(['$stateProvider','$urlRouterProvider','$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider){    
    
  $urlRouterProvider.when('/semester/{semester}/program/{program}', '/semester/{semester}/program/{program}/classes');

  var semesterView = {
    name: "semesterView",
    url: "/semester/:semester",
    controller: "semesterController as sem",
    abstract: true,
    template: '<ui-view/>'
  }, 
  programView = {
    name: "programView",
    parent: 'semesterView',
    url: "/program/:program",
    templateUrl: "app/webroot/tpl/program.tabFrame.tpl.html",
    controller: "programController as prg"
  },
  programSubmissions = {
    name: "programSubmissions",
    parent: 'programView',
    url: "/submissions",
    templateUrl: "app/webroot/tpl/program.submissions.tpl.html",
    controller: "prgSubmissionsController as prgSubs"
  },
  programClasses = {
    name: "programClasses",
    parent: 'programView',
    url: "/classes",
    templateUrl: "app/webroot/tpl/program.classList.tpl.html",
    controller: "prgClassesController as prgClasses"
  },
  classEdit = {
    name: "classEdit",
    parent: 'programClasses',
    url: "/:class/edit",
    templateUrl: "app/webroot/tpl/classForm.tpl.html",
    controller: "prgClassEditController as classForm"
  },
  classCreate = {
    name: "classCreate",
    parent: 'programClasses',
    url: "/create",
    templateUrl: "app/webroot/tpl/classForm.tpl.html",
    controller: "prgClassCreateController as classForm"
  },
  programEnrollment = {
    name: "programEnrollment",
    parent: 'programView',
    url: "/enrollment",
    templateUrl: "app/webroot/tpl/program.enrollment.tpl.html",
    controller: "prgEnrollmentController as prgEnroll"
  };

  $stateProvider
    .state(semesterView)
    .state(programView)
    .state(programSubmissions)
    .state(programClasses)
    .state(classEdit)
    .state(classCreate)
    .state(programEnrollment);
 }]);

// start semesterController.js

semesterProgramModule.controller('semesterController', ['$scope', '$q', '$stateParams', '$state', 'semesterFactory', function ($scope, $q, $stateParams, $state, semesterFactory) {

  console.log('semesterController instantiated');

  ctrl = this;

  ctrl.semesterSelection = {};

  semesterFactory.get($stateParams)

    .then(function (data){

        ctrl.semesterSelection = data.semester;
    
    }, function (error){
    
        console.log('ruh roh');
  
    });


}]);
semesterProgramModule.controller('prgClassesController', ['$scope', '$q', '$stateParams', 'classesFactory', 'usersFactory', 'messageCenterService', function ($scope, $q, $stateParams, classesFactory, usersFactory, messageCenterService) {

    // as prgClasses

    console.log('prgClassesController instantiated');

    var ctrl = this;

    ctrl.loaded = false;

    ctrl.classList = [];

    ctrl.loadList = function(){

        console.log('loading classes');

        classesFactory.query($stateParams).then(function(data){

            ctrl.loaded = true; 

            ctrl.classList =  data.classes;

        }, function (error){

            console.log('ruh roh');

        });

    };

    ctrl.loadList();

    usersFactory.teachers().then(function (data){

        ctrl.teachersList =  data;

    }, function (error){

        console.log('ruh roh');

    });
  
}]);
semesterProgramModule.controller('programController', ['$scope', '$q', '$stateParams', '$state', 'programsFactory', 'globalsFactory', function ($scope, $q, $stateParams, $state, programsFactory, globalsFactory) {
  
  // as prg
  
  console.log('programController');

  var ctrl = this;

  ctrl.programSelection = {};

  programsFactory.get($stateParams)

    .then(function (data){

        ctrl.programSelection = data.program;
    
    }, function (error){
    
        console.log('ruh roh');
  
    });
  
}]);
// start programSubControllers.js

semesterProgramModule.controller('prgEnrollmentController', ['$scope', '$q', '$stateParams', 'programEnrollmentFactory', function ($scope, $q, $stateParams, programEnrollmentFactory) {
 
  console.log('prgEnrollmentController instantiated');

  var ctrl = this;
 
  ctrl.enrollment = [];

  ctrl.displayList = [];

  programEnrollmentFactory.query($stateParams)

    .then(function (data){

      ctrl.enrollment = data.students;

      ctrl.displayList = [].concat(ctrl.enrollment);

    }, function (error){

      console.log('ruh roh');

  });

}]);
semesterProgramModule.controller('prgSubmissionsController', ['$scope', '$q', '$stateParams', 'programSubmissionsFactory', function ($scope, $q, $stateParams, programSubmissionsFactory) {

    console.log('prgSubmissionsController instantiated');

    var ctrl = this;

    ctrl.subsList =  [];

    ctrl.displayList = [];

    ctrl.submissionsExist = false;

    programSubmissionsFactory.query($stateParams)

    .then(function (data){

        if (data.submissions && data.submissions.length > 0) ctrl.submissionsExist = true; 
    
        ctrl.subsList = data.submissions;
  
        ctrl.displayList = data.submissions;
  
        return data;

    }, function (error){

        console.log('ruh roh');

    });

}]);
// usersFactories.js

semesterProgramModule.factory('programEnrollmentFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {
  
  var programEnrollmentResource = $resource('api/semester/:sem/program/:prg/enrollment.json', 

    { sem: '@sem', prg: '@prg' },
   
    { 
      
      'query' : {method: 'GET', isArray: false}

    });

  var factory = {

    // add promises
    query : function (params) {
    
      var deferred = $q.defer();
    
      programEnrollmentResource.query({sem: params.semester, prg: params.program},
    
        function (resp) {
    
          deferred.resolve(resp);
    
        });
    
      return deferred.promise;
    
    }

  };

  return factory;

}]);
// programSubmissionsFactory.js

semesterProgramModule.factory('programSubmissionsFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {
  
  var programSubmissionsResource = $resource('api/semester/:semester/program/:program/submissions.json', 

    { semester: '@semester', program: '@program' },
   
    { 
      
      'query' : {method: 'GET', isArray: false}

    });

  var factory = {

    query : function (params) {
    
      var deferred = $q.defer();
    
      programSubmissionsResource.query({semester: params.semester, program: params.program},
    
        function (resp) {
    
          deferred.resolve(resp);
    
        });
    
      return deferred.promise;
    
    }

  };

  return factory;

}]);
// semesterProgramClassModule.js Starts here 

// this module depends on semesterProgramModule. these routes will error with out it.

var semesterProgramClassModule = angular.module('semesterProgramClassModule', ['ui.router','smart-table','ngResource','ui.select','semesterProgramModule','usersModule']);

// routes for semester > program > class pathways
semesterProgramClassModule.config(['$stateProvider','$urlRouterProvider','$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider){    
    
  $urlRouterProvider.when('/semester/{semester}/program/{program}/class/{class}/', '/semester/{semester}/program/{program}/class/{class}/assignments');

  var classView = {
    name: "classView",
    parent: 'programView',
    url: "/class/:class",
    templateUrl: "app/webroot/tpl/class.tabFrame.tpl.html",
    controller: "classController as cls"
  },
  classAssignments = {
    name: "classAssignments",
    parent: 'classView',
    url: "/assignments",
    templateUrl: "app/webroot/tpl/class.assignmentsList.tpl.html",
    controller: "classAssignmentsController as clsAssign"
  },
  assignmentEdit = {
    name: "assignmentEdit",
    parent: 'classAssignments',
    url: "/:assg/edit",
    templateUrl: "app/webroot/tpl/assignmentForm.tpl.html",
    controller: "assignmentEditController as assignForm"
  },
  assignmentCreate = {
    name: "assignmentCreate",
    parent: 'classAssignments',
    url: "/create",
    templateUrl: "app/webroot/tpl/assignmentForm.tpl.html",
    controller: "assignmentCreateController as assignForm"
  },
  classEnrollment = {
    name: "classEnrollment",
    parent: 'classView',
    url: "/enrollment",
    templateUrl: "app/webroot/tpl/class.enrollment.tpl.html",
    controller: "classEnrollmentController as clsEnroll"
  },
  classEnrollmentEdit = {
    name: "classEnrollmentEdit",
    parent: 'classEnrollment',
    url: "/add",
    templateUrl: "app/webroot/tpl/class.enrollmentForm.tpl.html",
    controller: "classEnrollmentFormController as clsEnrollForm"
  },
  classFeedback = {
    name: "classFeedback",
    parent: 'classView',
    url: "/feedback",
    templateUrl: "app/webroot/tpl/class.feedback.tpl.html",
    controller: "classFeedbackController as clsFeed"
  },
  classDocumentation = {
    name: "classDocumentation",
    parent: 'classView',
    url: "/documentation",
    templateUrl: "app/webroot/tpl/class.documentation.tpl.html",
    controller: "classDocumentationController as clsDoc"
  };

  $stateProvider
      .state(classView)
      .state(classAssignments)
      .state(assignmentCreate)
      .state(assignmentEdit)
      .state(classEnrollment)
      .state(classEnrollmentEdit)
      .state(classFeedback)
      .state(classDocumentation);
 }]);
// semesterProgramClassAssignmentController.js

semesterProgramClassModule.controller('classAssignmentBlockController', ['$scope', '$q', '$stateParams', 'classAssignmentFactory', 'usersFactory', 'messageCenterService', function ($scope, $q, $stateParams, classAssignmentFactory, usersFactory, messageCenterService) {
  
    console.log('classAssignmentBlockController instantiated');

    var ctrl = this;

    ctrl.viewingSubs = false;

    ctrl.selectedForDownload = [];

    ctrl.toggleSubs = function(){
 
        ctrl.viewingSubs = !ctrl.viewingSubs;
 
        ctrl.selectedForDownload = [];
 
    };

    ctrl.manyToDownload = function(){
 
        if(ctrl.selectedForDownload.length > 1) return true;
 
        return false;
  
    };

    ctrl.downloadSubmission = function(){
 
        console.log('do downloady stuff here');
 
    };

    ctrl.assignmentSubsGrid = [
        {
            id: "1",
            student_id: "70",
            file: "submission1.pdf",
            receipt: "8369hd",
            created: "1414510836",
            updated: "1414510836"
        },
        {
            id: "2",
            student_id: "71",
            file: "submission2.pdf",
            receipt: "8sd9hd",
            created: "1414510836",
            updated: "1414510836"
        },
        {
            id: "3",
            student_id: "73",
            file: "submission3.pdf",
            receipt: "8369dw",
            created: "1414510836",
            updated: "1414510836"
        },
        {
            id: "4",
            student_id: "72",
            file: "submission14.pdf",
            receipt: "836sdhd",
            created: "1414510836",
            updated: "1414510836"
        }
    ];

    ctrl.assignmentSubsDisplayList = [].concat(ctrl.assignmentSubsGrid);

    // TODO: get submissions data somehow.

    // $scope.assignmentSubsList = classAssignmentFactory.query()
    
    //   .then(function (data){
    
    //     // add a fullname property for the grid to display
    //     globalsFactory.makeFullName(data); 
    
    //     return data.users;
    
    //   }, 
    
    //   function (error){
    
    //     console.log('Error');
    
    //     console.log(error);
    
    //   });
    
      // grid for subs here

}]);
// semesterProgramClassAssignmentController.js

semesterProgramClassModule.controller('assignmentCreateController', ['$scope', '$q', '$stateParams', '$state', '$timeout', 'classAssignmentFactory', 'programsFactory', 'messageCenterService', function ($scope, $q, $stateParams, $state, $timeout, classAssignmentFactory, programsFactory, messageCenterService) {
  
    console.log('assignmentCreateController instantiated');

    var ctrl = this;

    var localProgramList = [];

    ctrl.mode = 'create';

    ctrl.modal = true; 

    ctrl.message = '';

    ctrl.createItem = function(){

        for (var i = localProgramList.length - 1; i >= 0; i--) {
          
            if(localProgramList[i].slug == $stateParams.program){

                ctrl.item.program_id = localProgramList[i].id;
                
            }
      
        }

        ctrl.item.class_id = $scope.$parent.cls.classSelection.id;

        ctrl.item.semester_id = $scope.$parent.sem.semesterSelection.id;

        classAssignmentFactory.create($stateParams, ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the assignment.", { timeout: 6000 });

            } else {

                ctrl.message = 'Assignment Created';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);

            }

            $scope.$parent.clsAssign.loadList();

        },

        function(error){

            console.log(error);

        });

    };

    programsFactory.query().then(function (data){

        localProgramList = data.programs;

    }, function (error){

        console.log('ruh roh');

    });

}]);
// semesterProgramClassAssignmentController.js

semesterProgramClassModule.controller('assignmentEditController', ['$scope', '$q', '$stateParams', '$state', '$timeout', 'classAssignmentFactory', 'messageCenterService', function ($scope, $q, $stateParams, $state, $timeout, classAssignmentFactory, messageCenterService) {
  
    console.log('assignmentEditController instantiated');
    
    var ctrl = this;

    ctrl.mode = 'edit';

    ctrl.modal = true; 

    ctrl.deleting = false;

    ctrl.message = '';

    ctrl.toggleDelete = function(){

        ctrl.deleting = !ctrl.deleting;

    };

    ctrl.deleteItem = function(){
  
        classAssignmentFactory.remove($stateParams).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while deleting the assignment.", { timeout: 6000 });

            } else {
                
                ctrl.message = 'Assignment Deleted';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);
            }

            $scope.$parent.clsAssign.loadList();

        },

        function(error){

            console.log(error);

        });

    };
    
    ctrl.updateItem = function(){
  
        classAssignmentFactory.update($stateParams, ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the assignment.", { timeout: 6000 });

                console.log(data);

            } else {

                ctrl.message = 'Assignment Updated';

                $timeout(function(){

                    ctrl.message = '';

                    $state.go('assignmentEdit', {assg: data.assignment.slug});

                }, 1500);

            }

            $scope.$parent.clsAssign.loadList();

        },

        function(error){

            console.log(error);

        });

    };

    classAssignmentFactory.get($stateParams).then(function(data){

        if (data._error.code == 1) {

            messageCenterService.add('danger', "Something went wrong while retrieving the assignment.", { timeout: 6000 });

        } else {

            ctrl.item = data.assignment;

        }

    },

    function(error){

        console.log(error);

    });

}]);
// classAssignmentsController.js

semesterProgramClassModule.controller('classAssignmentsController', ['$scope', '$q', '$stateParams', 'classAssignmentFactory', 'usersFactory', 'messageCenterService', function ($scope, $q, $stateParams, classAssignmentFactory, usersFactory, messageCenterService) {
  
    console.log('classAssignmentsController instantiated');

    var ctrl = this;

    ctrl.loaded = false;

    ctrl.assignmentList = [];

    ctrl.loadList = function(){

        classAssignmentFactory.query($stateParams).then(function (data){

            ctrl.assignmentList = data.assignments;

            ctrl.loaded = true;

        }, function (error){

            console.log('ruh roh');

        });

    };

    ctrl.loadList();

}]);
// start classesControllers.js

semesterProgramModule.controller('prgClassCreateController', ['$scope', '$q', '$stateParams', '$state', '$timeout', 'classesFactory', 'modulesFactory', 'usersFactory', 'programsFactory', 'messageCenterService', function ($scope, $q, $stateParams, $state, $timeout, classesFactory, modulesFactory, usersFactory, programsFactory, messageCenterService) {

    console.log('prgClassCreateController instantiated');

    var ctrl = this;

    var localProgramList = [];

    ctrl.mode = 'create';

    ctrl.modal = true; 

    ctrl.message = '';

    ctrl.moduleList = [];

    ctrl.teachers = [];

    ctrl.createItem = function(){

        ctrl.item.teacher_authorizations = [];

        for (var j = ctrl.teachers.length - 1; j >= 0; j--) {

            var temp = {
            
                teacher_id: ctrl.teachers[j],

                semester_id: $scope.$parent.globals.currentSemesterObj.id
            
            };
            
            ctrl.item.teacher_authorizations.push(temp);
        
        }

        ctrl.item.semester_id = $scope.$parent.globals.currentSemesterObj.id;

        for (var i = localProgramList.length - 1; i >= 0; i--) {
          
            if(localProgramList[i].slug == $stateParams.program){

                ctrl.item.program_id = localProgramList[i].id;
                
            }
      
        }
  
        classesFactory.create($stateParams, ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the class.", { timeout: 6000 });

                console.log(data);

            } else {

                ctrl.message = 'Class Created';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);

            }

            $scope.$parent.prgClasses.loadList();

        },

        function(error){

            console.log(error);

        });

    };

    modulesFactory.query($stateParams).then(function (data){

        ctrl.moduleList = data.modules;

    }, function (error){

        console.log('ruh roh');

    });

    usersFactory.teachers().then(function (data){

        ctrl.teachers = data.roles;

    }, function (error){

        console.log('ruh roh');

    });

    programsFactory.query().then(function (data){

        localProgramList = data.programs;

    }, function (error){

        console.log('ruh roh');

    });

}]);
// start classesControllers.js

semesterProgramModule.controller('prgClassEditController', ['$scope', '$q', '$stateParams', '$state', '$timeout', 'classesFactory', 'modulesFactory', 'globalsFactory', 'messageCenterService', function ($scope, $q, $stateParams, $state, $timeout, classesFactory, modulesFactory, globalsFactory, messageCenterService) {

    console.log('prgClassEditController instantiated');
    
    var ctrl = this;
    
    ctrl.mode = 'edit';

    ctrl.modal = true; 

    ctrl.deleting = false;

    ctrl.message = '';

    ctrl.moduleList = [];

    ctrl.toggleDelete = function(){

        ctrl.deleting = !ctrl.deleting;

    };

    ctrl.onSelect = function($item, $model){

        ctrl.savingTeachers = true;

        var auth = {
        
            teacher_authorizations : [

                {
                    class_id: ctrl.item.id,

                    teacher_id: $model,

                    semester_id: $scope.$parent.globals.currentSemesterObj.id
                }

            ]
        
        };

        classesFactory.addAuth(auth).then(function(data){

            ctrl.savingTeachers = false;

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while adding the teacher.", { timeout: 6000 });

            } 

            $scope.$parent.prgClasses.loadList();

        },

        function(error){

            console.log(error);

        });

    };

    ctrl.onRemove = function($item, $model){

        ctrl.savingTeachers = true;

        var auth = {
        
            teacher_authorizations : [

                {
                    class_id: ctrl.item.id,

                    teacher_id: $model,

                    semester_id: $scope.$parent.globals.currentSemesterObj.id
                }

            ]
        
        };

        classesFactory.removeAuth(auth).then(function(data){

            ctrl.savingTeachers = false;

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while removing the teacher.", { timeout: 6000 });

            }

            $scope.$parent.prgClasses.loadList();

        },

        function(error){

            console.log(error);

        });

    };

    ctrl.deleteItem = function(){
  
        classesFactory.remove($stateParams).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while deleting the class.", { timeout: 6000 });

                console.log(data);

            } else {
                
                ctrl.message = 'Class Deleted';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);
            }

            $scope.$parent.prgClasses.loadList();

        },

        function(error){

            console.log(error);

        });

    };
    
    ctrl.updateItem = function(){

        classesFactory.update($stateParams, ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the class.", { timeout: 6000 });

                console.log(data);

            } else {

                ctrl.message = 'Class Updated';

                $timeout(function(){

                    ctrl.message = '';

                    $state.go('classEdit', {class: data.class.slug});

                }, 1500);

            }

            $scope.$parent.prgClasses.loadList();

        },

        function(error){

            console.log(error);

        });

    };

    classesFactory.get($stateParams).then(function(data){

        if (data._error.code == 1) {

            messageCenterService.add('danger', "Something went wrong while retrieving the class.", { timeout: 6000 });

        } else {

            ctrl.item = data.class;

            ctrl.teachers = [];

            for (var i = ctrl.item.teacher_authorizations.length - 1; i >= 0; i--) {
          
                ctrl.teachers.push(ctrl.item.teacher_authorizations[i].teacher_id);
          
            }

        }

    },

    function(error){

        console.log(error);

    });

    modulesFactory.query($stateParams).then(function (data){

        ctrl.moduleList = data.modules;

    }, function (error){

        console.log('ruh roh');

    });

}]);
// myClassesClassSubmissionsController.js

// The factory for this doesn't exist yet.

semesterProgramClassModule.controller('classStudentSubmissionsController', ['$scope', '$q', '$stateParams', 'myClassesSubmissionsFactory', function ($scope, $q, $stateParams, myClassesSubmissionsFactory) {

    console.log('classStudentSubmissionsController instantiated');

    var ctrl = this;

    ctrl.subsList =  [];
    
    ctrl.displayList = [];

    ctrl.submissionsExist = false;

    myClassesSubmissionsFactory.student($stateParams)

    .then(function (data){

        if (data.submissions && data.submissions.length > 0) ctrl.submissionsExist = true; 

        ctrl.subsList =  data.submissions;

        ctrl.displayList =  data.submissions;

        if(data.submissions.length > 0) {

            // ghetto get user info
            
            ctrl.student = data.submissions[0].student;

        }

    }, function (error){

        console.log('ruh roh: error in myclasses controller');

    });

    // single class stuff here.

}]);
// semesterProgramClassDocumentationController.js

semesterProgramClassModule.controller('classDocumentationController', ['$scope', '$q', '$stateParams', 'classesFactory', function ($scope, $q, $stateParams, classesFactory) {
 
  console.log('classDocumentationController instantiated');

  var ctrl = this;

  ctrl.classDocsExist = false;

  // TODO: build this factory and update 'exists'
  ctrl.classDocsListData = classesFactory.query($stateParams)
 
    .then(function (data){
  
      if (data.users && data.users.length > 0) ctrl.classDocsExist = true; // this will need to change to reflect actual data structure

      //console.log(data.users);
 
      return data.users;
 
    }, function (error){
 
      console.log('ruh roh');
 
    });

  // fix columns
  ctrl.classDocsList = { 
 
    data: 'classDocsListData',
 
    enableSorting: true,
 
    headerRowHeight: 30,
 
    headerClass: 'gridHeader',
 
    rowHeight: 40,
 
    columnDefs: [
 
      {field:'first_name', displayName:'First name'}, 
 
      {field:'last_name', displayName:'Last name'},
 
      {field:'email', displayName:'Email'}, 
 
      {field:'role_id', displayName:'Role'}, 
 
      {field:'id', displayName:'edit', cellTemplate: '<div><div class="ngCellText"><a href="users/{{row.getProperty(col.field)}}">view</a> - <a href="users/{{row.getProperty(col.field)}}/edit">edit</a></div></div>'}
 
    ]};
  
}]);
// semesterProgramClassEnrollmentController.js

semesterProgramClassModule.controller('classEnrollmentController', ['$scope', '$q', '$stateParams', 'classEnrollmentFactory', 'messageCenterService', function ($scope, $q, $stateParams, classEnrollmentFactory, messageCenterService) {
  
    console.log('classEnrollmentController instantiated');
  
    var ctrl = this;
      
    ctrl.classEnrollments = [];

    ctrl.loaded = false;

    ctrl.loadList = function(){
  
        classEnrollmentFactory.query($stateParams).then(function (data){
      
            ctrl.classEnrollments = data.students;

            ctrl.displayList = data.students;

            ctrl.loaded = true;

        }, function (error){

            console.log('ruh roh', error);

        });

    };

    ctrl.removeStudent = function(stuId){

        var enrollments = [{

            class_id: $scope.$parent.cls.classSelection.id,
      
            semester_id: $scope.$parent.sem.semesterSelection.id,
      
            student_id: stuId

        }];
        
        classEnrollmentFactory.remove($stateParams, {enrollments: enrollments}).then(function (data){
    
            if (data._error.code == 1) {
  
                messageCenterService.add('danger', "Something went wrong while removing the student.", { timeout: 6000 });
    
            } else {
          
                messageCenterService.add('success', "Student removed successfully.", { timeout: 6000 });

                // remove the student from the array here. not sure which will reflect in the table though...

            }

            ctrl.loadList();
  
        }, function (error){
  
            console.log('ruh roh');
  
        });

    };

    ctrl.loadList();
  
}]);
// semesterProgramClassEnrollmentController.js

semesterProgramClassModule.controller('classEnrollmentFormController', ['$scope', '$q', '$stateParams', '$timeout', '$state', 'messageCenterService', 'classEnrollmentFactory', 'usersFactory', function ($scope, $q, $stateParams, $timeout, $state, messageCenterService, classEnrollmentFactory, usersFactory) {
  
  console.log('classEnrollmentFormController instantiated');

  var ctrl = this;

  ctrl.modal = true;

  ctrl.message = '';
  
  ctrl.newEnrollments = [];

  ctrl.studentList = [];

  ctrl.addEnrollment = function(){

    var enrollments = [];

    for (var i = ctrl.newEnrollments.length - 1; i >= 0; i--) {

      var temp = {

        class_id: $scope.$parent.cls.classSelection.id,

        semester_id: $scope.$parent.sem.semesterSelection.id,

        student_id: ctrl.newEnrollments[i].id

      };
    
      enrollments.push(temp);
    
    }

    classEnrollmentFactory.create($stateParams, {enrollments: enrollments}).then(function (data){
    
      if (data._error.code == 1) {

          messageCenterService.add('danger', "Something went wrong while enrolling the students.", { timeout: 6000 });

          console.log(data);

      } else {
          
          ctrl.message = 'Enrollment Updated';

          $timeout(function(){

              $state.go('^', {reload:true});

          }, 1500);
      }

      $scope.$parent.clsEnroll.loadList();
  
    }, function (error){
  
      console.log('ruh roh');
  
    });

  };

  usersFactory.students().then(function (data){
    
      ctrl.studentList = data.roles;
  
    }, function (error){
  
      console.log('ruh roh');
  
    });
  
}]);
// semesterProgramClassFeedbackController.js

semesterProgramClassModule.controller('classFeedbackController', ['$scope', '$q', '$stateParams', 'classesFactory', function ($scope, $q, $stateParams, classesFactory) {
  
  console.log('classFeedbackController instantiated');

  var ctrl = this;

  ctrl.classFeedbackExist = false;

  // TODO: build this factory and update 'exists'
  ctrl.classFeedbackListData = classesFactory.query($stateParams)
  
    .then(function (data){

      if (data.users && data.users.length > 0) ctrl.classFeedbackExist = true; // this will need to change to reflect actual data structure
  
      //console.log(data.users);
  
      return data.users;
  
    }, function (error){
  
      console.log('ruh roh');
  
    });

  // fix columns
  ctrl.classFeedbackList = { 
  
    data: 'classFeedbackListData',
  
    enableSorting: true,
  
    headerRowHeight: 30,
  
    headerClass: 'gridHeader',
  
    rowHeight: 40,
  
    columnDefs: [
  
      {field:'first_name', displayName:'First name'}, 
  
      {field:'last_name', displayName:'Last name'},
  
      {field:'email', displayName:'Email'}, 
  
      {field:'role_id', displayName:'Role'}, 
  
      {field:'id', displayName:'edit', cellTemplate: '<div><div class="ngCellText"><a href="users/{{row.getProperty(col.field)}}">view</a> - <a href="users/{{row.getProperty(col.field)}}/edit">edit</a></div></div>'}
  
    ]};
  
}]);
// semesterProgramClassController.js

semesterProgramClassModule.controller('classController', ['$scope', '$q', 'classesFactory', 'globalsFactory', '$stateParams', '$state', 'messageCenterService', function ($scope, $q, classesFactory, globalsFactory, $stateParams, $state, messageCenterService) {
  
  console.log('class controller init', $stateParams.class);

    var ctrl = this;

    ctrl.classSelection = {};

    ctrl.classList = [];    

    ctrl.classSwitcher = function($item, $model){

        page = $state.current.name;

        $state.go(page, {class: $item.slug});
  
    };

    classesFactory.query($stateParams) 

    .then(function (data){

        if (data.classes && data.classes.length > 0) ctrl.classesExist = true; 

            ctrl.classList =  data.classes;

        }, function (error){

            console.log('ruh roh');

    });
    
    classesFactory.get($stateParams)

    .then(function (data){

        ctrl.classSelection = data.class;
    
    }, function (error){
    
        console.log('ruh roh');
  
    });

}]);
// start semesterProgramClassDirectives.js

semesterProgramClassModule.directive('assignmentBlock', ['globalsFactory', 'usersFactory', 'classAssignmentFactory', '$stateParams', function(globalsFactory, usersFactory, classAssignmentFactory, $stateParams) { 

  // not done yet
  return {
    restrict: 'A',
    templateUrl: "app/webroot/tpl/part.class.assignment.block.tpl.html",
    scope: {
      object: '=',
      currentsemester: '=',
      submissions: '='
    },
    link : function (scope, elem, attrs) {

      // Defines percentages of submissions in this order [subs, late, remaining]
      scope.bar = [];

      scope.state = $stateParams;
      scope.factory = classAssignmentFactory;
      scope.cleanSlug = globalsFactory.slugCleaner;
      scope.linker = globalsFactory.clsLinker; // calls linking methods in globalsFactory for building links to pages
      scope.original = angular.copy(scope.object);
      scope.selectedForDownload = scope.assignmentSubsGrid.selectedItems; // this needs to get a list of download links maybe

      scope.manyToDownload = function(){
        if(scope.selectedForDownload.length > 1){
          return true;
        } else {
          return false;
        }
      };

      scope.downloadSubmission = function(){
        console.log('do downloady stuff here');
      };

      scope.edit = {
        viewingSubs: false, // controls subs accordion
        editing : false, // controls the flip
        isNew : false, // controls edit button actions and text
        deleting : false,
        toggle : function(){
          this.editing = !this.editing;
          this.deleting = false;
          this.viewingSubs = false;
        },
        toggleSubs : function(){
          this.viewingSubs = !this.viewingSubs;
          scope.assignmentSubsGrid.selectAll(false);
        },
        toggleDeleting : function(){
          this.deleting = !this.deleting;
        },
        cancelEdit : function(){ // untested
          scope.object = angular.copy(scope.original);
        },
        canSave : function() {
          return scope.assignmentEditForm.$valid && !angular.equals(scope.original, scope.object);
        }
      };
    },

    controller : function($scope, $attrs){

      // TODO: not right data yet, 
      // this might go away if the data will come from the assignment item json itself
      $scope.assignmentSubsList = usersFactory.query()
        .then(function (data){

          // add a fullname property for the grid to display
          globalsFactory.makeFullName(data); 

          return data.users;
        }, 
        function (error){
          console.log('Error');
          console.log(error);
        }, 
        function(progress){
          console.log('progressBack');
          console.log(progress);
        });

      // TODO: add correct data 
      $scope.assignmentSubsGrid = { 
        data: 'assignmentSubsList',
        enableSorting: true,
        headerRowHeight: 30,
        selectedItems: [],
        headerClass: 'subGridHeader',
        rowHeight: 30,
        columnDefs: [
          {field:'full_name', displayName:'Student'}, 
          {field:'email', displayName:'Email'}, 
          {field:'submission_date', displayName: 'Sub. Date'},
          {field:'subs', displayName:'download', cellTemplate: '<div><div class="ngCellText"><a ng-click="downloadSubmission(); $event.stopPropagation()"><span>download</span></a></div></div>'}
        ]
      };

    }
  };
}]);
// usersFactories.js

semesterProgramClassModule.factory('classEnrollmentFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {
  
  var programEnrollmentResource = $resource('api/semester/:semester/program/:program/class/:class/students.json', 

    { semester: '@semester', program: '@program', class: '@class' },
   
    { 
      
      'query' : {method: 'GET', isArray: false},

      'create' : {method: 'POST'},

      'remove' : {method: 'POST'}

    });

  var factory = {

    // add promises
    query : function (params) {
    
      var deferred = $q.defer();
    
      programEnrollmentResource.query({semester: params.semester, program: params.program, class: params.class},
    
        function (resp) {
    
          deferred.resolve(resp);
    
        });
    
      return deferred.promise;
    
    },

    create : function (params, payload) {
    
      var deferred = $q.defer();
    
      programEnrollmentResource.create({semester: params.semester, program: params.program, class: params.class}, payload,
    
        function (resp) {
    
          deferred.resolve(resp);
    
        });
    
      return deferred.promise;
    
    },

    remove : function (params, payload) {

      payload._method = 'delete';

      var deferred = $q.defer();

      programEnrollmentResource.remove({semester: params.semester, program: params.program, class: params.class}, payload, 

        function(resp){

        deferred.resolve(resp);

      });

      return deferred.promise;

    }

  };

  return factory;

}]);
// semesterProgramClassAssignmentFactories.js

semesterProgramClassModule.factory('classAssignmentFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {


  var classAssignmentsResource = $resource('api/semester/:semester/program/:program/class/:class/assignments.json',

    { semester: '@semester', program: '@program', class: '@class' },

    {

      'query': {method: 'GET'},

      'create': {method: 'POST'}

    });


  var classAssignmentResource = $resource('api/semester/:semester/program/:program/class/:class/assignment/:assg.json',

    { semester: '@semester', program: '@program', class: '@class', assg: '@assg' },

    {

      'get': {method: 'GET'},

      'update': {method: 'POST'},

      'remove': {method: 'POST'} 

    });


  var factory = {

    query: function (params) {

      var deferred = $q.defer();

      classAssignmentsResource.query({semester: params.semester, program: params.program, class: params.class},

        function (resp) {

          deferred.resolve(resp);

        });

      return deferred.promise;

    },

    get: function (params) {

      var deferred = $q.defer();

      classAssignmentResource.get({semester: params.semester, program: params.program, class: params.class, assg: params.assg},

        function (resp) {

          deferred.resolve(resp);

        });

      return deferred.promise;

    },

    create : function (params, payload) {

      console.log('create asgn!');

      var deferred = $q.defer();

      classAssignmentsResource.create({semester: params.semester, program: params.program, class: params.class}, payload, function(resp){

        deferred.resolve(resp);

      });

      return deferred.promise;

    },

    update : function (params, payload) {

      payload._method = 'put';

      var deferred = $q.defer();

      classAssignmentResource.update({semester: params.semester, program: params.program, class: params.class, assg: params.assg}, payload, function(resp){

        deferred.resolve(resp);

      });

      return deferred.promise;

    },

    remove : function (params) {

      var payload = { _method: 'delete' };

      var deferred = $q.defer();

      classAssignmentResource.remove({semester: params.semester, program: params.program, class: params.class, assg: params.assg}, function(resp){

        deferred.resolve(resp);

      });

      return deferred.promise;

    }

  };

  return factory;

}]);
//w semesterProgramClassFactories.js

semesterProgramClassModule.factory('classesFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {
  
  // TODO: include program and semester here, this is all classes, but should be for a program
  // deprecated
  var allClassesResource = $resource('api/classes.json', 
  
    { semester: '@semester', program: '@program' },
    
    {'query': {method: 'GET', isArray: false} 

  });

  var programClassesResource = $resource('api/semester/:semester/program/:program/classes.json',

    { semester: '@semester', program: '@program' },

    {

      'query': {method: 'GET', isArray: false},

      'create': {method: 'POST'}

    });

  var programClassResource = $resource('api/semester/:semester/program/:program/class/:class.json',

    { semester: '@semester', program: '@program', class: '@class' },

    {

      'get': {method: 'GET', isArray: false},

      'update': {method: 'POST'},

      'remove': {method: 'POST'} 

    });

  var authorizationsResource = $resource('api/teacherauthorizations.json',

    {},

    {

      'add': {method: 'POST'},

      'remove': {method: 'POST'} 

    });


  var factory = {

    // add promises
    queryAll: function () {

      var deferred = $q.defer();

      allClassesResource.query({},

        function (resp) {

          deferred.resolve(resp);

        });

      return deferred.promise;

    },

    query: function (params) {

      var deferred = $q.defer();

      programClassesResource.query({semester: params.semester, program: params.program},

        function (resp) {

          deferred.resolve(resp);

        });

      return deferred.promise;

    },

    get: function (params) {

      var deferred = $q.defer();

      programClassResource.get({semester: params.semester, program: params.program, class: params.class},

        function (resp) {

          deferred.resolve(resp);

        });

      return deferred.promise;

    },

    create : function (params, payload) {

      console.log('create class!');

      var deferred = $q.defer();

      programClassesResource.create({semester: params.semester, program: params.program}, payload, function(resp){

        deferred.resolve(resp);

      });

      return deferred.promise;

    },

    update : function (params, payload) {

      delete payload.id;
      delete payload.created;
      delete payload.updated;

      console.log('called update');

      payload._method = 'put';

      var deferred = $q.defer();

      programClassResource.update({semester: params.semester, program: params.program, class: params.class}, payload, function(resp){

        deferred.resolve(resp);

      });

      return deferred.promise;

    },

    remove : function (params) {

      var deferred = $q.defer();

      var payload = { _method: 'delete' };

      programClassResource.remove({semester: params.semester, program: params.program, class: params.class}, payload, function(resp){

        deferred.resolve(resp);

      });

      return deferred.promise;

    },
    addAuth : function (payload) {

      payload._method = 'put';

      var deferred = $q.defer();

      authorizationsResource.add({}, payload, function(resp){

        deferred.resolve(resp);

      });

      return deferred.promise;

    },

    removeAuth : function (payload) {

      payload._method = 'delete';

      var deferred = $q.defer();

      authorizationsResource.remove({}, payload, function(resp){

        deferred.resolve(resp);

      });

      return deferred.promise;

    }

  };

  return factory;

}]);
// semestersModule.js Starts here 

// move semester routes from classnest and myclassesnest here.

var semestersModule = angular.module('semestersModule', ['smart-table','ngResource']);

semestersModule.config(['$stateProvider','$urlRouterProvider','$locationProvider',function($stateProvider, $urlRouterProvider, $locationProvider){
  
  var semesters = {
    name: "semesters",
    url: "/semesters",
    templateUrl: "app/webroot/tpl/semesterList.tpl.html",
    controller: "semestersController as semesters"
  }, 
  semesterEdit = {
    name: "semesterEdit",
    parent: 'semesters',
    url: "/:semester/edit",
    templateUrl: "app/webroot/tpl/semesterForm.tpl.html",
    controller: "semesterEditController as semForm"
  },
  semesterCreate = {
    name: "semesterCreate",
    parent: 'semesters',
    url: "/create",
    templateUrl: "app/webroot/tpl/semesterForm.tpl.html",
    controller: "semesterCreateController as semForm"
  };

  $stateProvider
	.state(semesters)
	.state(semesterEdit)
	.state(semesterCreate);

}]);
// start semestersController.js

semestersModule.controller('semesterCreateController', ['$scope', '$q', '$timeout', '$state', 'semesterFactory', 'messageCenterService', function ($scope, $q, $timeout, $state, semesterFactory, messageCenterService) {
  
    console.log('semesterCreateController instantiated');

    var ctrl = this;

    ctrl.mode = 'create';

    ctrl.modal = true; 

    ctrl.message = '';

    ctrl.createItem = function(){
  
        semesterFactory.create(ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the semester.", { timeout: 6000 });

                console.log(data);

            } else {

                ctrl.message = 'Semester Created';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);

            }

            $scope.$parent.semesters.loadList();

        },

        function(error){

            console.log(error);

        });

    };

}]);

// start semestersController.js

semestersModule.controller('semesterEditController', ['$scope', '$q', '$stateParams', '$state', '$timeout', 'semesterFactory', 'messageCenterService', function ($scope, $q, $stateParams, $state, $timeout, semesterFactory, messageCenterService) {
  
    console.log('semesterEditController instantiated');

    // get semester
    // map to form model
    // flatten to only scope methods

    var ctrl = this;    
    
    ctrl.mode = 'edit';

    ctrl.modal = true; 

    ctrl.deleting = false;

    ctrl.message = '';

    ctrl.toggleDelete = function(){

        ctrl.deleting = !ctrl.deleting;

    };

    ctrl.deleteItem = function(){
  
        semesterFactory.remove($stateParams).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while deleting the semester.", { timeout: 6000 });

                console.log(data);

            } else {
                
                ctrl.message = 'Semester Deleted';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);
            }

            $scope.$parent.semesters.loadList();

        },

        function(error){

            console.log(error);

        });

    };
    
    ctrl.updateItem = function(){
  
        semesterFactory.update($stateParams, ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the semester.", { timeout: 6000 });

                console.log(data);

            } else {

                ctrl.message = 'Semester Updated';

                $timeout(function(){

                    $state.go('semesterEdit', {semester: data.semester.slug});

                }, 1500);

            }

            $scope.$parent.semesters.loadList();

        },

        function(error){

            console.log(error);

        });

    };

    semesterFactory.get($stateParams).then(function(data){

        if (data._error.code == 1) {

            messageCenterService.add('danger', "Something went wrong while retrieving the semester.", { timeout: 6000 });

        } else {

            ctrl.item = data.semester;

        }

    },

    function(error){

        console.log(error);

    });
  
}]);

// start semestersController.js

semestersModule.controller('semestersController', ['$scope', '$q', 'semesterFactory', 'messageCenterService', function ($scope, $q, semesterFactory, messageCenterService) {
  
    console.log('semestersController instantiated');

    var ctrl = this;

    ctrl.loaded = false;

    // semester list is also done in globals since we use it app wide basically, but also here so we can reload it rationally.

    ctrl.semesterList = [];

    ctrl.loadList = function(){

        semesterFactory.query().then(function(data){

            ctrl.semesterList = data.semesters;

            ctrl.loaded = true;

        }, function (error){
      
            console.log('ruh roh');
      
        });

    };

    ctrl.loadList();

}]);

// usersModule.js Starts here 

var usersModule = angular.module('usersModule', ['smart-table','ngResource']);

usersModule.config(['$stateProvider','$urlRouterProvider','$locationProvider',function($stateProvider, $urlRouterProvider, $locationProvider){
    
    var users = {
        name: "users",
        url: "/users",
        templateUrl: "app/webroot/tpl/userList.tpl.html",
        controller: "usersController as users"
    },
    user = {
        name: "user",
        url: "/user/:user",
        templateUrl: "app/webroot/tpl/part.editUserForm.tpl.html",
        controller: "userController as user",
        parent: users
    },
    editUser = {
        name: "editUser",
        url: "/:user/edit",
        templateUrl: "app/webroot/tpl/userForm.tpl.html",
        controller: "userEditController as userForm",
        parent: 'users'
    },
    createUser = {
        name: "createUser",
        url: "/create",
        templateUrl: "app/webroot/tpl/userForm.tpl.html",
        controller: "userCreateController as userForm",
        parent: 'users'
    },
    importUsers = {
        name: "importUsers",
        url: "/import",
        templateUrl: "app/webroot/tpl/importForm.tpl.html",
        controller: "usersImportController as usersImport",
        parent: 'users'
    };

    $stateProvider
        .state(users)
        .state(user)
        .state(editUser)
        .state(createUser)
        .state(importUsers);

}]);

usersModule.constant('userRoles', [
    {   
        role_id: '1',
        role_title: 'Admin'
    },
    {
        role_id: '2',
        role_title: 'Program Leader'
    },
    {
        role_id: '3',
        role_title: 'Teacher'
    },
    {
        role_id: '4',
        role_title: 'Adacemic Services'
    },
    {
        role_id: '5',
        role_title: 'External Verifier'
    },
    {
        role_id: '6',
        role_title: 'Student Records'
    },
    {
        role_id: '7',
        role_title: 'Student'
    }
]);

usersModule.constant('userStatus', [
    
    { 
        status_id : '1',
        status_title: 'Active'
    },
    { 
        status_id : '2',
        status_title:'Inactive'
    }
    
]);



usersModule.controller('userController', ['ctrl','$q','usersFactory','$stateParams','$state', function (ctrl, $q, usersFactory, $stateParams, $state) {
  
  console.log('singleUserController instantiated');
  

}]);
// usersControllers.js

usersModule.controller('userCreateController', ['$scope', '$state', '$timeout', 'usersFactory', 'messageCenterService', 'userRoles', function ($scope, $state, $timeout, usersFactory, messageCenterService, userRoles) {
  
  console.log('userCreateController instantiated');

  var ctrl = this;

    ctrl.mode = 'create';

    ctrl.modal = true; 

    ctrl.message = '';

    ctrl.passwordEdit = true;

    ctrl.rolesList = userRoles;

    ctrl.createItem = function(){

        ctrl.item.roles_users = [];

        for (var i = ctrl.roles.length - 1; i >= 0; i--) {

            var temp = {
            
                role_id: ctrl.roles[i]
            
            };
            
            ctrl.item.roles_users.push(temp);
        
        }
  
        usersFactory.create(ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the user.", { timeout: 6000 });

                console.log(data);

            } else {

                ctrl.message = 'User Created';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);

            }

            $scope.$parent.users.loadList();

        },

        function(error){

            console.log(error);

        });

    };

}]);
// usersControllers.js

usersModule.controller('userEditController', ['$scope', '$stateParams', '$state', '$timeout', 'usersFactory', 'messageCenterService', 'userRoles', function ($scope, $stateParams, $state, $timeout, usersFactory, messageCenterService, userRoles) {
  
    console.log('userEditController instantiated');

    var ctrl = this;

    ctrl.mode = 'edit';

    ctrl.modal = true; 

    ctrl.deleting = false;

    ctrl.message = '';

    ctrl.rolesList = userRoles;

    ctrl.toggleDelete = function(){

        ctrl.deleting = !ctrl.deleting;

    };

    ctrl.deleteItem = function(){
  
        usersFactory.remove($stateParams).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while deleting the user.", { timeout: 6000 });

                console.log(data);

            } else {
                
                ctrl.message = 'User Deleted';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);
            }

            $scope.$parent.users.loadList();

        },

        function(error){

            console.log(error);

        });

    };
    
    ctrl.updateItem = function(){

        ctrl.item.roles_users = [];

        for (var i = ctrl.roles.length - 1; i >= 0; i--) {

            var temp = {
            
                role_id: ctrl.roles[i],

                user_id: ctrl.item.id
            
            };
            
            ctrl.item.roles_users.push(temp);
        
        }
  
        usersFactory.update($stateParams, ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the user.", { timeout: 6000 });

                console.log(data);

            } else {

                ctrl.message = 'User Updated';

                $timeout(function(){

                    ctrl.message = '';

                }, 1500);

            }

            $scope.$parent.users.loadList();

        },

        function(error){

            console.log(error);

        });

    };

    usersFactory.get($stateParams).then(function(data){

        if (data._error.code == 1) {

            messageCenterService.add('danger', "Something went wrong while retrieving the user.", { timeout: 6000 });

        } else {

            ctrl.item = data.user;

            ctrl.roles = [];

            for (var i = ctrl.item.roles_users.length - 1; i >= 0; i--) {
          
                ctrl.roles.push(ctrl.item.roles_users[i].role_id);
          
            }

        }

    },

    function(error){

        console.log(error);

    });

}]);
// usersControllers.js

usersModule.controller('usersController', ['$scope', 'usersFactory', 'messageCenterService', function ($scope, usersFactory, messageCenterService) {
  
    console.log('usersController instantiated');

    var ctrl = this;

    ctrl.usersList = [];

    ctrl.displayList = [];

    ctrl.loaded = false;

    ctrl.loadList = function(){

        usersFactory.query().then(function (data){
    
            ctrl.usersList = data.users;

            ctrl.displayList = [].concat(ctrl.usersList);

            ctrl.loaded = true;
      
        }, function (error){
      
            console.log('ruh roh');
      
        });

    };

    ctrl.loadList();

}]);
// usersControllers.js

usersModule.controller('usersImportController', ['$scope', 'usersFactory', 'messageCenterService', function ($scope, usersFactory, messageCenterService) {
  
  console.log('usersImportController instantiated');

  var ctrl = this;

  var toInject;

  ctrl.checkJson = function(){

    ctrl.jsonStatus = '';
    
    try {
    
        var o = JSON.parse(ctrl.jsonText);

        // Handle non-exception-throwing cases:
        // Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
        // but... JSON.parse(null) returns 'null', and typeof null === "object", 
        // so we must check for that, too.
    
        if (o && typeof o === "object" && o !== null) {

          toInject = o;

          ctrl.jsonStatus = 'Valid';

          console.log(toInject);
        
        }
    
    }
    
    catch (e) { 

      console.log(e);

      ctrl.jsonStatus = 'Invalid';

    }

  };

  ctrl.importAsJson = function(){

    ctrl.status = [];

    // for mass importing, pareses text to json and imports

    async.each(toInject.users, function(file, callback) {

      // Perform operation on file here.

      ctrl.status.push('Processing user: ' + file.email);

      console.log('Processing user ' + file.email);

      usersFactory.create(file)

      .then(function(data){

        console.log('data: ', data);

          if (data._error.code == 1) {

            ctrl.status.push('ERROR: ' + file.email);

              callback(data._error);

          } else {

              console.log('user processed');

              ctrl.status.push('User processed: ' + file.email);
    
              callback(); 

          }

      },
      function(error){
        console.log(error);
      });
      
    }, function(err){
      
        if( err ) {

          // One of the iterations produced an error.
          
          // All processing will now stop.

          console.log('Something went wrong', err);
       
        } else {
        
          console.log('All files have been processed successfully');

          ctrl.status.push('All users have been imported successfully');
       
        }
    
    });

  };

}]);
// roleClassesFactories.js

usersModule.factory('roleClassesFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {
  
    var teacherResource = $resource('api/teacher/:id/classes.json', {id: '@id'},
   
        { 'get': {method: 'GET', isArray: false, cache: true} });


    var studentResource = $resource('api/student/:id/classes.json', {id: '@id'},
    
        { 'get': {method: 'GET', isArray: false, cache: true} });


    var factory = {

        teacherClasses: function (params) {
      
            var deferred = $q.defer();
        
            var ID;
        
            if (params.id !== undefined) {
        
                ID = params.id;
        
            } else {
        
                ID = params;
        
            }
      
            teacherResource.get({id: ID},
        
                function (resp) {
            
                    deferred.resolve(resp);
            
                });
        
            return deferred.promise;
      
        },
        
        studentClasses: function (params) {
      
            var deferred = $q.defer();
        
            var ID;

            console.log('params', params);
        
            if (typeof(params.id) !== undefined) {
        
                ID = params.id;
        
            } else {
        
                ID = params;
        
            }

            console.log('infactory', ID);
      
            studentResource.get({id: ID},
        
                function (resp) {
            
                    deferred.resolve(resp);
            
                });
        
            return deferred.promise;

        }
      
    };

    return factory;

}]);
// usersRolesFilter.js

usersModule.filter('makeUserRole', ['userRoles', function(userRoles) {

    return function(input) {

        for (var i = userRoles.length - 1; i >= 0; i--) {
    
            if (userRoles[i].role_id == input) {
        
                return userRoles[i].role_title;

            }
            
        }

    };

}]);


usersModule.filter('makeUserStatus', ['userStatus', function(userStatus) {

    return function(input) {

        for (var i = userStatus.length - 1; i >= 0; i--) {
    
            if (userStatus[i].status_id == input) {
        
                return userStatus[i].status_title;

            }
            
        }

    };

}]);
// extcodesModule.js Starts here 

var extcodesModule = angular.module('extcodesModule', ['smart-table','ngResource']);

extcodesModule.config(['$stateProvider','$urlRouterProvider','$locationProvider',function($stateProvider, $urlRouterProvider, $locationProvider){

  var extCodes = {
      name: "extCodes",
      url: "/extensioncodes",
      templateUrl: "app/webroot/tpl/extensioncodeList.tpl.html",
      controller: "extensioncodesController as extCodes"
    },
    extCodeEdit = {
      name: "extCodeEdit",
      parent: "extCodes",
      url: "/:id/edit",
      templateUrl: "app/webroot/tpl/extensioncodeForm.tpl.html",
      controller: "extensioncodeEditController as extForm"
    },
    extCodeCreate = {
      name: "extCodeCreate",
      parent: "extCodes",
      url: "/create",
      templateUrl: "app/webroot/tpl/extensioncodeForm.tpl.html",
      controller: "extensioncodeCreateController as extForm"
    };

  $stateProvider
	.state(extCodes)
	.state(extCodeEdit)
	.state(extCodeCreate);

}]);
// start extensionsCodesControllers.js

extcodesModule.controller('extensioncodeCreateController', ['$scope', '$q', '$timeout', '$stateParams', '$state', 'roleClassesFactory', 'extensioncodesFactory', 'usersFactory', 'messageCenterService', function ($scope, $q, $timeout, $stateParams, $state, roleClassesFactory, extensioncodesFactory, usersFactory, messageCenterService) {

    console.log('extensioncodeCreateController instantiated');

    console.log($scope.$parent);

    var ctrl = this;
    
    var currentSemester = ctrl.currentSemester; 
    // to get this loaded in time... janky, but otherwise would need nested promises in loadXXX functions
    
    ctrl.mode = 'create';

    ctrl.modal = true; 

    ctrl.message = '';

    ctrl.studentList = [];

    ctrl.prgList = [];

    ctrl.studentAssgns = [];

    ctrl.item = {};

    usersFactory.students().then(function (data){

        ctrl.studentList = data.roles;

    }, function (error){

        console.log('ruh roh');

    });

    ctrl.loadAssignments = function(item){

        ctrl.item.assignment_id = '';

        roleClassesFactory.studentClasses({id: item}).then(function(data){

            var classes =  data.classes;

            var result = [];

            for (var a = classes.length - 1; a >= 0; a--) {
                
                for (var b = classes[a].assignments.length - 1; b >= 0; b--) {
                    
                    result.push(classes[a].assignments[b]);
                
                }

            }

            ctrl.studentAssgns = result;

        },

        function(error){

            console.log(error);

        });

    };
  
    ctrl.createItem = function(){
  
        extensioncodesFactory.create(ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the extension code.", { timeout: 6000 });

                console.log(data);

            } else {

                ctrl.message = 'Extension Code Created';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);

            }

            $scope.$parent.extCodes.loadList();

        },

        function(error){

            console.log(error);

        });

    };
  
}]);
// start extensionsCodesControllers.js

extcodesModule.controller('extensioncodeEditController', ['$scope', '$q', '$timeout', '$stateParams', '$state', 'extensioncodesFactory', 'semesterFactory', 'usersFactory', 'roleClassesFactory', 'messageCenterService', function ($scope, $q, $timeout, $stateParams, $state, extensioncodesFactory, semesterFactory, usersFactory, roleClassesFactory, messageCenterService) {

    console.log('extensioncodeEditController instantiated');

    var ctrl = this;

    var currentSemester = ctrl.currentSemester; // to get this loaded in time... janky, but otherwise would need nested promises in loadXXX functions

    ctrl.extList = [];

    ctrl.displayList = [];
    
    ctrl.studentList = [];

    ctrl.mode = 'edit';

    ctrl.modal = true; 

    ctrl.deleting = false;

    ctrl.message = '';

    ctrl.studentAssgns = [];

    ctrl.item = {};

    ctrl.toggleDelete = function(){

        ctrl.deleting = !ctrl.deleting;

    };

    ctrl.deleteItem = function(){
  
        extensioncodesFactory.remove($stateParams).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while deleting the code.", { timeout: 6000 });

            } else {
                
                ctrl.message = 'Extension Code Deleted';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);
            }

            $scope.$parent.extCodes.loadList();

        },

        function(error){

            console.log(error);

        });

    };
    
    ctrl.updateItem = function(){
  
        extensioncodesFactory.update($stateParams, ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the code.", { timeout: 6000 });

            } else {

                ctrl.message = 'Extension Code Updated';

                $timeout(function(){

                    ctrl.message = '';

                }, 1500);

            }

            $scope.$parent.extCodes.loadList();

        },

        function(error){

            console.log(error);

        });

    };

    extensioncodesFactory.get($stateParams).then(function(data){

        if (data._error.code == 1) {

            messageCenterService.add('danger', "Something went wrong while retrieving the extension code.", { timeout: 6000 });

        } else {

            ctrl.item = data.extensionCode;

            console.log('in assgn get', data.extensionCode.student_id);

            ctrl.loadAssignments(data.extensionCode.student_id);

        }

    },

    function(error){

        console.log(error);

    });

    usersFactory.students().then(function (data){

        ctrl.studentList = data.roles;

    }, function (error){
    
        console.log('ruh roh');
    
    });

    ctrl.loadAssignments = function(item){

        ctrl.item.assignment_id = '';

        roleClassesFactory.studentClasses({id: item}).then(function(data){

            var classes =  data.classes;

            var result = [];

            for (var a = classes.length - 1; a >= 0; a--) {
                
                for (var b = classes[a].assignments.length - 1; b >= 0; b--) {
                    
                    result.push(classes[a].assignments[b]);
                
                }

            }

            ctrl.studentAssgns = result;

            console.log('asign for ', ctrl.studentAssgns);

        },

        function(error){

            console.log(error);

        });

    };
      
}]);
// start extensionsCodesControllers.js

extcodesModule.controller('extensioncodesController', ['$scope', '$q', 'extensioncodesFactory', 'programsFactory', 'classesFactory', 'semesterFactory', 'usersFactory', 'classAssignmentFactory', 'messageCenterService', function ($scope, $q, extensioncodesFactory, programsFactory, classesFactory, semesterFactory, usersFactory, classAssignmentFactory, messageCenterService) {

    console.log('extensioncodesController instantiated');

    var ctrl = this;

    var currentSemester = ctrl.currentSemester; // to get this loaded in time... janky, but otherwise would need nested promises in loadXXX functions
    
    ctrl.extList = [];

    ctrl.displayList = [];

    ctrl.loaded = false;

    ctrl.loadList = function(){

        extensioncodesFactory.query().then(function(data){

            ctrl.loaded = true;

            ctrl.extList = data.extensionCodes;

            ctrl.displayList = data.extensionCodes;

        }, function (error){

            console.log('ruh roh');

        });

    };

    ctrl.loadList();

}]);
// usersModule.js Starts here 

var uservoiceModule = angular.module('uservoiceModule', []);

uservoiceModule.config(['$stateProvider','$urlRouterProvider','$locationProvider',function($stateProvider, $urlRouterProvider, $locationProvider){
    
    console.log('user voice facotry called.');

    UserVoice=window.UserVoice||[];

    (function(){

      var uv=document.createElement('script');

      uv.type='text/javascript';

      uv.async=true;

      uv.src='//widget.uservoice.com/PfyxW3kgH950qRwRdgXMeQ.js';

      var s=document.getElementsByTagName('script')[0];

      s.parentNode.insertBefore(uv,s);

    })();

    UserVoice.push(['set', {
      accent_color: '#6aba2e',
      trigger_color: 'white',
      trigger_background_color: '#6aba2e'
    }]);

    UserVoice.push(['addTrigger', { mode: 'contact', trigger_position: 'bottom-left' }]);

    // Autoprompt for Satisfaction and SmartVote (only displayed under certain conditions)
    // UserVoice.push(['autoprompt', {}]);

    // the user info gets filled in when the profile endpoint is called in globals controller by the uvFactory.

}]);
uservoiceModule.factory('uvFactory', [function() {    

    factory = function(profile){

        UserVoice.push(['identify', {
    
            email: profile.email,
    
            name: profile.first_name + ' ' + profile.last_name,
    
            id: profile.id,
    
            type: profile.roles_users[0].role_id
    
        }]);

    };

    return factory;
    
}]);
// usersModule.js Starts here 

var uservoiceModule = angular.module('analyticsModule', []);

uservoiceModule.config(['$stateProvider','$urlRouterProvider','$locationProvider',function($stateProvider, $urlRouterProvider, $locationProvider){
    
  // maybe do setup here. Right now Heap setup is on each template in the head element. I could probably move it here.

}]);
uservoiceModule.factory('analyticsFactory', [function() {    

    factory = {
        
        identify: function(profile){

            heap.identify({
                name: profile.first_name + ' ' + profile.last_name,
                syloId: profile.id,
                userRole: profile.roles_users[0].role_id,
                email: profile.email
            });

        },

        uploadErrors : function(report){

            console.log('error report sent', report);

            heap.track('Upload Errors', report);

        },

        uploadSuccess : function(report){

            console.log('success report sent', report);

            heap.track('Upload Success', report);

        }

    };

    return factory;
    
}]);
// submissionsListModule.js Starts here 

var submissionsListModule = angular.module('submissionsListModule', ['smart-table','ngResource']);
submissionsListModule.directive('fileviewer', ['ngModalDefaults', '$sce', '$location', function(ngModalDefaults, $sce, $location) {
      
      return {
      
        restrict: 'E',

        require: '^submissionList',
      
        scope: {
      
          show: '=',
            
          onClose: '&?',

          subsArray: '=',

          subIndex: '='
      
        },
      
        replace: true,
      
        transclude: true,
      
        link: function(scope, element, attrs) {

          var webNativeFiles = ['jpeg','jpg','png','bmp','gif','tiff','svg','mp3','mpeg','wav','ogg','mp4'];

          var setupCloseButton, setupStyle;

          scope.dialogTitle = '';

          scope.pathToFile = '';

          scope.fileViewerPath = function(){

            var activeSub = scope.subsArray[scope.subIndex];

            var fe = activeSub.name.split('.').pop().toLowerCase();

            if(webNativeFiles.indexOf(fe) >= 0){

              scope.pathToFile = $location.$$protocol + '://' + $location.$$host + '/uploads/' + activeSub.path + activeSub.name;

            } else {

              scope.pathToFile = 'http://drive.google.com/viewer?url=' + $location.$$protocol + '://' + $location.$$host + '/uploads/' + activeSub.path + activeSub.name + '&embedded=true';

            }

          };
      
          setupCloseButton = function() {

            scope.closeButtonHtml = $sce.trustAsHtml(ngModalDefaults.closeButtonHtml);
      
            return scope.closeButtonHtml;
      
          };
      
          setupStyle = function() {
      
            scope.dialogStyle = {};
      
            if (attrs.width) {
      
              scope.dialogStyle.width = attrs.width;
      
            }
      
            if (attrs.height) {

                scope.dialogStyle.height = attrs.height;
      
                return scope.dialogStyle.height;
      
            }
      
          };
      
          scope.hideModal = function() {

            scope.show = false;
      
            return scope.show;
      
          };
      
          scope.$watch('show', function(newVal, oldVal) {
      
            if (newVal && !oldVal) {

              // on

              scope.fileViewerPath();
      
              document.getElementsByTagName("body")[0].style.overflow = "hidden";
      
            } else {

              // off
      
              document.getElementsByTagName("body")[0].style.overflow = "";
      
            }
      
            if ((!newVal && oldVal) && (scope.onClose !== null)) {

              // off 

              scope.pathToFile = '';
      
              return scope.onClose();
      
            }
      
          });
      
          setupCloseButton();
      
          return setupStyle();
      
        },
      
        templateUrl: 'tpl/fileviewer.tpl.html'
      
      };

    }

  ]);
submissionsListModule.directive('submissionList', ['downloadFactory', function (downloadFactory) {

    return {
        
        restrict: "A",

        scope: {
            list: '=',
            displayAs: '@',
            loaded: '='
        },

        replace: true,

        templateUrl: 'tpl/subList.tpl.html',

        controller: function($scope){

            var unopenableFiles = ['rar','zip','gzip','psd','ai','indd','mov','tar'];

            $scope.subToDownload = {};

            $scope.modalOpen = false;

            $scope.activeSubIndex = '0';

            $scope.showModal = function(index){

                $scope.modalOpen = true;

                $scope.activeSubIndex = index;

            };

            $scope.displayList = [].concat($scope.list);

            $scope.groupByStudent = function() {

                // nothing yet

            };

            $scope.groupByAssignment = function() {

                // nothing yet

            };

            $scope.groupByClass = function() {

                // nothing yet

            };

            $scope.testIfViewable = function(name){

                var fe = name.split('.').pop().toLowerCase();

                if(unopenableFiles.indexOf(fe) >= 0){

                    return true;

                } else {

                    return false;

                }

            };

            $scope.downloadGroup = function(){

                var arr = _.values($scope.subToDownload);

                _.pull(arr, "");

                downloadFactory.download(arr).then(function(data){

                    console.log(data);

                }, function(error){
                
                    console.log('bulk download error: ' + error);
                
                });

            };

        },

        link: function (scope, elem, attrs, ctrl) {

    
        }

    };

}]);
// submissions list module

submissionsListModule.factory("downloadFactory", ['$http', '$resource', '$q', function ($http, $resource, $q) {

  var downloadGroupResource = $resource('api/download/submissions.json', {},
    
    {

      'download': {method: 'POST'} 

    });


  var factory = {
    
    download : function (downloadList) {
      // this should be passed an array of submission IDs
 
      var deferred = $q.defer();
 
      downloadGroupResource.download({submissions_to_download: downloadList},
 
        function (resp) {
 
          deferred.resolve(resp);
 
        });
 
      return deferred.promise;
 
    }

  };
  
  return factory;

}]);
submissionsListModule.provider("ngModalDefaults", function() {

    return {

      options: {

        closeButtonHtml: "<span class='ng-modal-close-x'>X</span>"

      },

      $get: function() {

        return this.options;

      },

      set: function(keyOrHash, value) {

        var k, v, _results;

        if (typeof keyOrHash === 'object') {

          _results = [];

          for (k in keyOrHash) {

            v = keyOrHash[k];

            _results.push(this.options[k] = v);

          }

          return _results;

        } else {

          this.options[keyOrHash] = value;

          return this.options[keyOrHash];

        }

      }

    };

  });