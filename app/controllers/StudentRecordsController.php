<?php

namespace app\controllers;

use app\models\StudentRecords;
use lithium\g11n\Message;

use app\extensions\helper\Debug;

class StudentRecordsController extends \app\extensions\action\Controller {

	/**
	 * [ short description of PREFIXDestroy action ]
	 *
	 * [ long description of PREFIXDestroy action ]
	 *
	 * @todo   Incomplete
	 * @url    DELETE /studentRecords/:id
	 */
	public function PREFIXDestroy() {
		extract(Message::aliases());
		$studentRecord = StudentRecords::find($this->request->id);

		if ($studentRecord->delete()) {
			$this->flashSuccess($t('The studentRecord was deleted.'));
			return $this->redirect(['StudentRecords::index']);
		}

		$this->flashError($t('The studentRecord could not be deleted.'));
		return $this->redirect($this->request->referer());
	}

	/**
	 * [ short description of PREFIXEdit action ]
	 *
	 * [ long description of PREFIXEdit action ]
	 *
	 * @todo   Incomplete
	 * @url    GET /studentRecords/:id/edit
	 * @url    PUT /studentRecords/:id/edit
	 */
	public function PREFIXEdit() {
		$studentRecord = StudentRecords::find($this->request->id);
		if ($this->request->is('put')) {
			extract(Message::aliases());

			if ($studentRecord->save($this->request->data)) {
				$this->flashSuccess($t('The studentRecord was updated.'));
				return $this->redirect([
					'StudentRecords::view',
					'id' => $studentRecord->id
				]);
			}
			$this->flashError($t('The studentRecord could not be updated.'));
		}
		return compact('studentRecord');
	}

	/**
	 * [ short description of PREFIXIndex action ]
	 *
	 * [ long description of PREFIXIndex action ]
	 *
	 * @todo   Incomplete
	 * @url    GET /studentRecords
	 * @url    GET /studentRecords/index
	 */
	public function PREFIXIndex() {
		$limit = 10;
		$page  = $this->request->page ?: 1;

		$studentRecords = StudentRecords::all([
			'limit' => $limit,
			'page'  => $page,
			'order' => ['id' => 'ASC']
		]);

		$total = StudentRecords::find('count');
		return compact('studentRecords', 'total', 'limit', 'page');
	}

	/**
	 * [ short description of PREFIXCreate action ]
	 *
	 * [ long description of PREFIXCreate action ]
	 *
	 * @todo   Incomplete
	 * @url    GET  /studentRecords/create
	 * @url    POST /studentRecords/create
	 */
	public function PREFIXCreate() {
		$studentRecord = StudentRecords::create();

		if ($this->request->is('post')) {
			extract(Message::aliases());

			if ($studentRecord->save($this->request->data)) {
				$this->flashSuccess($t('The studentRecord was added.'));
				return $this->redirect([
					'StudentRecords::view',
					'id' => $studentRecord->id
				]);
			}
			$this->flashError($t('The studentRecord could not be added.'));
		}

		return compact('studentRecord');
	}

	/**
	 * Allows users with role `PREFIX` to search within this model.
	 *
	 * @todo   Incomplete
	 * @see    app\models\StudentRecords
	 * @url    GET /studentRecords/search/q=<search term>&<field_1>=&<field_2>...
	 */
	public function PREFIXSearch() {
		return $this->search();
	}

	/**
	 * [ short description of PREFIXView action ]
	 *
	 * [ long description of PREFIXView action ]
	 *
	 * @todo   Incomplete
	 * @url GET /studentRecords/1
	 */
	public function PREFIXView() {
		$studentRecord = StudentRecords::find($this->request->id);
		return compact('studentRecord');
	}
}

?>