<?php

namespace app\controllers;

use app\models\ExternalVerifiers;
use lithium\g11n\Message;

use app\extensions\helper\Debug;

class ExternalVerifiersController extends \app\extensions\action\Controller {

	/**
	 * [ short description of PREFIXDestroy action ]
	 *
	 * [ long description of PREFIXDestroy action ]
	 *
	 * @todo   Incomplete
	 * @url    DELETE /externalVerifiers/:id
	 */
	public function PREFIXDestroy() {
		extract(Message::aliases());
		$externalVerifier = ExternalVerifiers::find($this->request->id);

		if ($externalVerifier->delete()) {
			$this->flashSuccess($t('The externalVerifier was deleted.'));
			return $this->redirect(['ExternalVerifiers::index']);
		}

		$this->flashError($t('The externalVerifier could not be deleted.'));
		return $this->redirect($this->request->referer());
	}

	/**
	 * [ short description of PREFIXEdit action ]
	 *
	 * [ long description of PREFIXEdit action ]
	 *
	 * @todo   Incomplete
	 * @url    GET /externalVerifiers/:id/edit
	 * @url    PUT /externalVerifiers/:id/edit
	 */
	public function PREFIXEdit() {
		$externalVerifier = ExternalVerifiers::find($this->request->id);
		if ($this->request->is('put')) {
			extract(Message::aliases());

			if ($externalVerifier->save($this->request->data)) {
				$this->flashSuccess($t('The externalVerifier was updated.'));
				return $this->redirect([
					'ExternalVerifiers::view',
					'id' => $externalVerifier->id
				]);
			}
			$this->flashError($t('The externalVerifier could not be updated.'));
		}
		return compact('externalVerifier');
	}

	/**
	 * [ short description of PREFIXIndex action ]
	 *
	 * [ long description of PREFIXIndex action ]
	 *
	 * @todo   Incomplete
	 * @url    GET /externalVerifiers
	 * @url    GET /externalVerifiers/index
	 */
	public function PREFIXIndex() {
		$limit = 10;
		$page  = $this->request->page ?: 1;

		$externalVerifiers = ExternalVerifiers::all([
			'limit' => $limit,
			'page'  => $page,
			'order' => ['id' => 'ASC']
		]);

		$total = ExternalVerifiers::find('count');
		return compact('externalVerifiers', 'total', 'limit', 'page');
	}

	/**
	 * [ short description of PREFIXCreate action ]
	 *
	 * [ long description of PREFIXCreate action ]
	 *
	 * @todo   Incomplete
	 * @url    GET  /externalVerifiers/create
	 * @url    POST /externalVerifiers/create
	 */
	public function PREFIXCreate() {
		$externalVerifier = ExternalVerifiers::create();

		if ($this->request->is('post')) {
			extract(Message::aliases());

			if ($externalVerifier->save($this->request->data)) {
				$this->flashSuccess($t('The externalVerifier was added.'));
				return $this->redirect([
					'ExternalVerifiers::view',
					'id' => $externalVerifier->id
				]);
			}
			$this->flashError($t('The externalVerifier could not be added.'));
		}

		return compact('externalVerifier');
	}

	/**
	 * Allows users with role `PREFIX` to search within this model.
	 *
	 * @todo   Incomplete
	 * @see    app\models\ExternalVerifiers
	 * @url    GET /externalVerifiers/search/q=<search term>&<field_1>=&<field_2>...
	 */
	public function PREFIXSearch() {
		return $this->search();
	}

	/**
	 * [ short description of PREFIXView action ]
	 *
	 * [ long description of PREFIXView action ]
	 *
	 * @todo   Incomplete
	 * @url GET /externalVerifiers/1
	 */
	public function PREFIXView() {
		$externalVerifier = ExternalVerifiers::find($this->request->id);
		return compact('externalVerifier');
	}
}

?>