<?php

namespace app\controllers;

use app\models\Modules;
use lithium\g11n\Message;

use app\extensions\helper\Debug;

class ModulesController extends \app\extensions\action\Controller {

	/**
	 * A list of Modules in a Program [1.1.4]
	 *
	 * @url    GET /program/:program/modules
	 */
	protected function _index() {
		$modules = Modules::all([
			'order' => ['id' => 'ASC'],
			'with' => ['Programs'],
			'conditions' => ['Programs.slug' => $this->request->program]
		]);
		return compact('modules');
	}

	public function adminIndex() {
		return $this->_index();
	}

	public function prgLeadIndex() {
		return $this->_index();
	}

	public function acdServIndex() {
		return $this->_index();
	}

	/**
	 * @url    POST /program/:program/modules
	 */
	protected function _create() {
		$module = Modules::create();

		if ($this->request->is('post')) {
			$module->save($this->request->data);
		}

		return compact('module');
	}

	public function adminCreate() {
		return $this->_create();
	}

	public function prgLeadCreate() {
		return $this->_create();
	}

	public function acdServCreate() {
		return $this->_create();
	}

	/**
	 * View module :module that belongs to program :program [1.1.4a]
	 *
	 * @url    GET /program/:program/module/:module
	 */
	protected function _view() {
		$module = Modules::find('first', [
			'conditions' => ['slug' => $this->request->module]
		]);
		return compact('module');
	}

	public function adminView() {
		return $this->_view();
	}

	public function prgLeadView() {
		return $this->_view();
	}

	public function acdServView() {
		return $this->_view();
	}


	/**
	 * Edit module :module that belongs to program :program [1.1.4a]
	 *
	 * @url    GET /program/:program/module/:module
	 * @url    PUT /program/:program/module/:module
	 */
	protected function _edit() {
		$module = Modules::find('first', [
			'conditions' => ['slug' => $this->request->module]
		]);

		if ($this->request->is('put')) {
			$module->save($this->request->data);
		}

		return compact('module');
	}

	public function adminEdit() {
		return $this->_edit();
	}

	public function prgLeadEdit() {
		return $this->_edit();
	}

	public function acdServEdit() {
		return $this->_edit();
	}

	/**
	 * Delete module :module that belongs to program :program [1.1.4a]
	 *
	 * @url    DELETE /program/:program/module/:module
	 */
	protected function _destroy() {
		$module = Modules::find('first', [
			'conditions' => ['slug' => $this->request->module]
		]);

		$module->delete();

		return compact('module');
	}

	public function adminDestroy() {
		return $this->_destroy();
	}
}

?>