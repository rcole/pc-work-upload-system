<?php

namespace app\controllers;

use app\models\Documentation;
use lithium\g11n\Message;
use app\models\Sessions;

use app\extensions\helper\Debug;

class DocumentationController extends \app\extensions\action\Controller {

	/**
	 * Displays a list of documentation for a class [1.1.1.4]
	 *
	 * @url    GET /semester/:semester/program/:program/class/:class/documentation
	 */
	protected function _index() {
		$documentation = Documentation::all([
			'order' => ['id' => 'ASC'],
			'with' => ['Classes', 'Classes.Concurrences.Semesters', 'Classes.Modules.Programs'],
			'conditions' => [
				'Semesters.title' => $this->request->semester,
				'Programs.slug' => $this->request->program,
				'Classes.slug' => $this->request->class
			]
		]);

		return compact('documentation');
	}

	public function adminIndex() {
		return $this->_index();
	}

	public function prgLeadIndex() {
		return $this->_index();
	}

	public function acdServIndex() {
		return $this->_index();
	}

	/**
	 * Displays a list of documentation for a class a teacher is authorized for [1.6.4]
	 *
	 * @url    GET /semester/:semester/myclasses/:class/documentation
	 */
	public function teacherIndex() {
		$documentation = Documentation::all([
			'order' => ['id' => 'ASC'],
			'with' => ['Classes', 'Classes.Semesters', 'Classes.Authorizations.Teachers'],
			'conditions' => [
				'Semesters.title' => $this->request->semester,
				'Classes.slug' => $this->request->class,
				'Teachers.id' => Sessions::id()
			]
		]);

		return compact('documentation');
	}

	/**
	 * Create documentation for a class [1.1.1.4]
	 *
	 * @url    POST /semester/:semester/program/:program/class/:class/documentation
	 */
	protected function _create($id = null) {
		$documentation = Documentation::create();

		if ($this->request->is('post')) {
			if ($id) {
				$this->request->data['teacher_id'] = $id;
			}

			$documentation->save($this->request->data);
		}

		return compact('documentation');
	}

	public function adminCreate() {
		return $this->_create();
	}

	public function prgLeadCreate() {
		return $this->_create();
	}

	public function acdServCreate() {
		return $this->_create();
	}

	/**
	 * [1.6.4]
	 *
	 * @url    POST /semester/:semester/myclasses/:class/documentation
	 */
	public function teacherCreate() {
		return $this->_create(Sessions::id());
	}


	/**
	 * View documentation with id :id for the semester :semester for the program :program in class :class [1.1.1.3a]
	 *
	 * @url GET /semester/:semester/program/:program/class/:class/documentation/:id
	 */
	protected function _view() {
		$documentation = Documentation::find('first', [
			'order' => ['id' => 'ASC'],
			'with' => ['Classes', 'Classes.Concurrences.Semesters', 'Classes.Modules.Programs'],
			'conditions' => [
				'Semesters.title' => $this->request->semester,
				'Programs.slug' => $this->request->program,
				'Classes.slug' => $this->request->class,
				'Documentation.id' => $this->request->id,
			]
		]);
		return compact('documentation');
	}

	public function adminView() {
		return $this->_view();
	}

	public function prgLeadView() {
		return $this->_view();
	}

	public function acdServView() {
		return $this->_view();
	}

	/**
	 * View documentation with id :id for the semester :semester in class :class for the currently
	 * logged in teacher [1.6.4a]
	 *
	 * @url GET /semester/:semester/myclasses/:class/documentation/:id
	 */
	public function teacherView() {
		$documentation = Documentation::find('first', [
			'order' => ['id' => 'ASC'],
			'with' => ['Classes', 'Classes.Concurrences.Semesters', 'Classes.Modules.Programs', 'Teachers'],
			'conditions' => [
				'Semesters.title' => $this->request->semester,
				'Classes.slug' => $this->request->class,
				'Documentation.id' => $this->request->id,
				'Teachers.id' => Sessions::id()
			]
		]);
		return compact('documentation');
	}


	/**
	 * Edit documentation with id :id for the semester :semester for the program :program in class :class [1.1.1.3a]
	 *
	 * @url GET /semester/:semester/program/:program/class/:class/documentation/:id
	 */
	protected function _edit() {
		$documentation = Documentation::find('first', [
			'order' => ['id' => 'ASC'],
			'with' => ['Classes', 'Classes.Concurrences.Semesters', 'Classes.Modules.Programs'],
			'conditions' => [
				'Semesters.title' => $this->request->semester,
				'Programs.slug' => $this->request->program,
				'Classes.slug' => $this->request->class,
				'Documentation.id' => $this->request->id,
			]
		]);

		if ($this->request->is('put')) {
			$documentation->save($this->request->data);
		}

		return compact('documentation');
	}

	public function adminEdit() {
		return $this->_edit();
	}

	public function prgLeadEdit() {
		return $this->_edit();
	}

	public function acdServEdit() {
		return $this->_edit();
	}

	/**
	 * Edit documentation with id :id for the semester :semester in class :class for the currently
	 * logged in teacher [1.6.4a]
	 *
	 * @url GET /semester/:semester/myclasses/:class/documentation/:id
	 * @url PUT /semester/:semester/myclasses/:class/documentation/:id
	 */
	public function teacherEdit() {
		$documentation = Documentation::find('first', [
			'order' => ['id' => 'ASC'],
			'with' => ['Classes', 'Classes.Concurrences.Semesters', 'Classes.Modules.Programs', 'Teachers'],
			'conditions' => [
				'Semesters.title' => $this->request->semester,
				'Programs.slug' => $this->request->program,
				'Classes.slug' => $this->request->class,
				'Documentation.id' => $this->request->id,
				'Teachers.id' => Sessions::id()
			]
		]);

		if ($this->request->is('put')) {
			$documentation->save($this->request->data);
		}

		return compact('documentation');
	}


	/**
	 * Delete documentation with id :id for the semester :semester for the program :program in class :class [1.1.1.3a]
	 *
	 * @url DELETE /semester/:semester/program/:program/class/:class/documentation/:id
	 */
	protected function _destroy() {
		$documentation = Documentation::find('first', [
			'order' => ['id' => 'ASC'],
			'with' => ['Classes', 'Classes.Concurrences.Semesters', 'Classes.Modules.Programs'],
			'conditions' => [
				'Semesters.title' => $this->request->semester,
				'Programs.slug' => $this->request->program,
				'Classes.slug' => $this->request->class,
				'Documentation.id' => $this->request->id,
			]
		]);

		$documentation->delete();

		return compact('documentation');
	}

	public function adminDestroy() {
		return $this->_destroy();
	}

	public function prgLeadDestroy() {
		return $this->_destroy();
	}

	public function acdServDestroy() {
		return $this->_destroy();
	}

	/**
	 * Delete documentation with id :id for the semester :semester in class :class for the currently
	 * logged in teacher [1.6.4a]
	 *
	 * @url DELETE /semester/:semester/myclasses/:class/documentation/:id
	 */
	public function teacherDestroy() {
		$documentation = Documentation::find('first', [
			'order' => ['id' => 'ASC'],
			'with' => ['Classes', 'Classes.Concurrences.Semesters', 'Classes.Modules.Programs', 'Teachers'],
			'conditions' => [
				'Semesters.title' => $this->request->semester,
				'Programs.slug' => $this->request->program,
				'Classes.slug' => $this->request->class,
				'Documentation.id' => $this->request->id,
				'Teachers.id' => Sessions::id()
			]
		]);

		$documentation->delete();

		return compact('documentation');
	}
}

?>