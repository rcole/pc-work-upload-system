<?php

namespace app\controllers;

use app\models\TeacherAuthorizations;
use lithium\g11n\Message;

use app\extensions\helper\Debug;

class TeacherAuthorizationsController extends \app\extensions\action\Controller {

	/**
	 * Associates teacher to a class if one doesn't already exist.
	 *
	 * @url    POST /api/teacherauthorizations [#129]
	 */
	public function adminCreate() {
		$authorization = TeacherAuthorizations::create();

		if ($this->request->is('post')) {
			foreach ($this->request->data['teacher_authorizations'] as $authnData) {
				$authorized = TeacherAuthorizations::find('first', ['conditions' => [
					'class_id' => $authnData['class_id'],
					'semester_id' => $authnData['semester_id'],
					'teacher_id' => $authnData['teacher_id'],
				]]);
				if (!$authorized->exists()) {
					$authorization = TeacherAuthorizations::create();
					$authorization->save($authnData);
				}
			}
		}

		return compact('authorization');
	}

	/**
	 * Associates teacher to a class if one doesn't already exist.
	 *
	 * @url    DELETE /api/teacherauthorizations [#129]
	 */
	public function adminDestroy() {
		foreach ($this->request->data['teacher_authorizations'] as $authnData) {
			$authorized = TeacherAuthorizations::find('first', ['conditions' => [
				'class_id' => $authnData['class_id'],
				'semester_id' => $authnData['semester_id'],
				'teacher_id' => $authnData['teacher_id'],
			]]);

			$authorized->delete();
		}

		return compact('authorized');
	}

	/**
	 * [ short description of PREFIXEdit action ]
	 *
	 * [ long description of PREFIXEdit action ]
	 *
	 * @todo   Incomplete
	 * @url    GET /authorizations/:id/edit
	 * @url    PUT /authorizations/:id/edit
	 */
	public function PREFIXEdit() {
		$authorization = Authorizations::find($this->request->id);
		if ($this->request->is('put')) {
			extract(Message::aliases());

			if ($authorization->save($this->request->data)) {
				$this->flashSuccess($t('The authorization was updated.'));
				return $this->redirect([
					'TeacherAuthorizations::view',
					'id' => $authorization->id
				]);
			}
			$this->flashError($t('The authorization could not be updated.'));
		}
		return compact('authorization');
	}

	/**
	 * [ short description of PREFIXIndex action ]
	 *
	 * [ long description of PREFIXIndex action ]
	 *
	 * @todo   Incomplete
	 * @url    GET /authorizations
	 * @url    GET /authorizations/index
	 */
	public function PREFIXIndex() {
		$limit = 10;
		$page  = $this->request->page ?: 1;

		$authorizations = TeacherAuthorizations::all([
			'limit' => $limit,
			'page'  => $page,
			'order' => ['id' => 'ASC']
		]);

		$total = TeacherAuthorizations::find('count');
		return compact('authorizations', 'total', 'limit', 'page');
	}

	/**
	 * Allows users with role `PREFIX` to search within this model.
	 *
	 * @todo   Incomplete
	 * @see    app\models\TeacherAuthorizations
	 * @url    GET /authorizations/search/q=<search term>&<field_1>=&<field_2>...
	 */
	public function PREFIXSearch() {
		return $this->search();
	}

	/**
	 * [ short description of PREFIXView action ]
	 *
	 * [ long description of PREFIXView action ]
	 *
	 * @todo   Incomplete
	 * @url GET /authorizations/1
	 */
	public function PREFIXView() {
		$authorization = TeacherAuthorizations::find($this->request->id);
		return compact('authorization');
	}
}

?>