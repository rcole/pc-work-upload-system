<?php

namespace app\controllers;

use app\models\Resources;
use lithium\g11n\Message;

use app\extensions\helper\Debug;

class ResourcesController extends \app\extensions\action\Controller {
	/**
	 * [ short description of PREFIXDestroy action ]
	 *
	 * [ long description of PREFIXDestroy action ]
	 *
	 * @todo   Incomplete
	 * @url    DELETE /resources/:id
	 */
	public function PREFIXDestroy() {
		extract(Message::aliases());
		$resource = Resources::find($this->request->id);

		if ($resource->delete()) {
			$this->flashSuccess($t('The resource was deleted.'));
			return $this->redirect(['Resources::index']);
		}

		$this->flashError($t('The resource could not be deleted.'));
		return $this->redirect($this->request->referer());
	}

	/**
	 * [ short description of PREFIXEdit action ]
	 *
	 * [ long description of PREFIXEdit action ]
	 *
	 * @todo   Incomplete
	 * @url    GET /resources/:id/edit
	 * @url    PUT /resources/:id/edit
	 */
	public function PREFIXEdit() {
		$resource = Resources::find($this->request->id);
		if ($this->request->is('put')) {
			extract(Message::aliases());

			if ($resource->save($this->request->data)) {
				$this->flashSuccess($t('The resource was updated.'));
				return $this->redirect([
					'Resources::view',
					'id' => $resource->id
				]);
			}
			$this->flashError($t('The resource could not be updated.'));
		}
		return compact('resource');
	}

	/**
	 * [ short description of PREFIXIndex action ]
	 *
	 * [ long description of PREFIXIndex action ]
	 *
	 * @todo   Incomplete
	 * @url    GET /resources
	 * @url    GET /resources/index
	 */
	public function PREFIXIndex() {
		$limit = 10;
		$page  = $this->request->page ?: 1;

		$resources = Resources::all([
			'limit' => $limit,
			'page'  => $page,
			'order' => ['id' => 'ASC']
		]);

		$total = Resources::find('count');
		return compact('resources', 'total', 'limit', 'page');
	}

	/**
	 * [ short description of PREFIXCreate action ]
	 *
	 * [ long description of PREFIXCreate action ]
	 *
	 * @todo   Incomplete
	 * @url    GET  /resources/create
	 * @url    POST /resources/create
	 */
	public function PREFIXCreate() {
		$resource = Resources::create();

		if ($this->request->is('post')) {
			extract(Message::aliases());

			if ($resource->save($this->request->data)) {
				$this->flashSuccess($t('The resource was added.'));
				return $this->redirect([
					'Resources::view',
					'id' => $resource->id
				]);
			}
			$this->flashError($t('The resource could not be added.'));
		}

		return compact('resource');
	}

	/**
	 * Allows users with role `PREFIX` to search within this model.
	 *
	 * @todo   Incomplete
	 * @see    app\models\Resources
	 * @url    GET /resources/search/q=<search term>&<field_1>=&<field_2>...
	 */
	public function PREFIXSearch() {
		return $this->search();
	}

	/**
	 * [ short description of PREFIXView action ]
	 *
	 * [ long description of PREFIXView action ]
	 *
	 * @todo   Incomplete
	 * @url GET /resources/1
	 */
	public function PREFIXView() {
		$resource = Resources::find($this->request->id);
		return compact('resource');
	}
}

?>