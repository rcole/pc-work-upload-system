<?php

namespace app\controllers;

use app\models\Classes;
use app\models\Concurrences;
use app\models\TeacherAuthorizations;
use app\models\Semesters;
use app\models\Students;
use app\models\Users;
use app\models\Sessions;
use lithium\g11n\Message;

use app\extensions\helper\Debug;

class ClassesController extends \app\extensions\action\Controller {

	/**
	 * Should give a semesters object with all the classes a teacher is teaching for that semester [3.1]
	 *
	 * Should give a semesters object with all the classes enrolled by students for that semester [3.2]
	 *
	 * @url    GET /:role/:user_id/classes
	 */
	protected function _classes($userId = null) {
		$conditions = [
			'Semesters.begins' => ['<=' => time()],
			'Semesters.ends' => ['>=' => time()]
		];

		$with[] = 'Concurrences.Semesters';
		$with[] = 'Assignments';

		if (isset($this->request->params['role'])) {
			switch ($this->request->params['role']) {
				case 'student':
					$conditions['Students.id'] = (null === $userId) ? $this->request->user_id : $userId;
					$with[] = 'Enrollments.Students';
				break;

				case 'teacher':
					$conditions['Teachers.id'] = $this->request->user_id;
					$with[] = 'TeacherAuthorizations.Teachers';
				break;
			}
		}

		$classes = Classes::all([
			'with' => $with,
			'conditions' => $conditions
		]);

		return compact('classes');
	}

	public function adminClasses() {
		return $this->_classes();
	}

	public function prgLeadClasses() {
		return $this->_classes();
	}

	public function teacherClasses() {
		return $this->_classes();
	}

	public function acdServClasses() {
		return $this->_classes();
	}

	public function extVerClasses() {
		return $this->_classes();
	}

	public function studRecClasses() {
		return $this->_classes();
	}

	public function studentClasses() {
		return $this->_classes(Sessions::id());
	}


	/**
	 * A list of Students enrolled in an authorized Class [1.6.2]
	 *
	 * @url    GET /semester/:semester/myclasses/:class/enrollment
	 */
	public function teacherEnrollment() {
		$students = Students::all([
			'with' => ['Enrollments.Semesters', 'Enrollments.Classes', 'Enrollments.Classes.TeacherAuthorizations.Teachers'],
			'conditions' => [
				'Semesters.slug' => $this->request->semester,
				'Classes.slug' => $this->request->class,
				'Teachers.id' => Sessions::id()
			]
		]);

		return compact('students');
	}

	/**
	 * A list of Classes in a Program [1.1.1]
	 *
	 * @url    GET /semester/:semester/program/:program/classes
	 */
	protected function _index() {
		$classes = Classes::getClassesBySemesterAndProgram([
			'program' => $this->request->program,
			'semester' => $this->request->semester
		]);

		return compact('classes');
	}

	public function adminIndex() {
		return $this->_index();
	}

	public function prgLeadIndex() {
		return $this->_index();
	}

	public function acdServIndex() {
		return $this->_index();
	}

	/**
	 * A list of Classes that the current user is teaching [1.6]
	 *
	 * Returns data about a single class that the current teach is teaching [1.6a]
	 *
	 * @url    GET /semester/:semester/myclasses [1.6]
 	 * @url    GET /semester/:semester/myclasses/:class [1.6a]
	 */
	public function teacherIndex() {
		$fields = Users::safeFields([], 'Teachers');
		$fields[] = 'Concurrences.*';
		$fields[] = 'Semesters.*';
		$fields[] = 'Classes.*';
		$fields[] = 'TeacherAuthorizations.*';
		$fields[] = 'Modules.*';

		$conditions = [
			'Semesters.slug' => $this->request->semester,
			'Teachers.id' => Sessions::id()
		];

		if (isset($this->request->class)) {
			$conditions['Classes.slug'] = $this->request->class;
		}

		$classes = Classes::all([
			'with' => ['Concurrences.Semesters', 'TeacherAuthorizations.Teachers', 'Modules'],
			'conditions' => $conditions,
			'fields' => $fields
		]);

		return compact('classes');
	}

	/**
	 * A list of enrolled classes [2.5]
	 *
	 * @url    GET /semester/:semester/enrolledclasses
	 */
	public function studentIndex() {
		$classes = Classes::all([
			'with' => ['Concurrences.Semesters', 'Enrollments.Students'],
			'conditions' => [
				'Semesters.slug' => $this->request->semester,
				'Students.id' => Sessions::id()
			]
		]);

		return compact('classes');
	}


	/**
	 * Creates a class and associates it with `Semesters` by creating `Concurrences` [1.1.1]
	 *
	 * @url    POST /semester/:semester/program/:program/classes
	 */
	protected function _create() {
		$semester = Semesters::find('first', ['conditions' => ['slug' => $this->request->semester]]);
		$class = Classes::create();

		if ($this->request->is('post')) {
			$class->save($this->request->data);

			$concurrence = Concurrences::create([
				'class_id' => $class->id,
				'semester_id' => $semester->id
			]);
			$concurrence->save();

			foreach ((array) $this->request->data['teacher_authorizations'] as $teacher) {
				$teacher['class_id'] = $class->id;
				$currentAuthorizations = TeacherAuthorizations::find('first', [
					'conditions' => [
						'teacher_id' => $teacher['teacher_id'],
						'class_id' => $class->id,
						'semester_id' => $semester->id
					]
				]);

				if (!$currentAuthorizations->exists()) {
					$teacherAuthorizations = TeacherAuthorizations::create([
						'teacher_id' => $teacher['teacher_id'],
						'class_id' => $class->id,
						'semester_id' => $semester->id
					]);
					$teacherAuthorizations->save();
				}
			}
		}

		return compact('class');
	}

	public function adminCreate() {
		return $this->_create();
	}

	public function prgLeadCreate() {
		return $this->_create();
	}

	public function acdServCreate() {
		return $this->_index();
	}

	/**
	 * View the class :class in the Program :program for the Semester :semester [1.1.1a]
	 *
	 * GET /api/semester/:semester/program/:program/class/:class
	 */
	protected function _view() {
		$class = Classes::getClassBySemesterAndProgram([
			'program' => $this->request->program,
			'semester' => $this->request->semester,
			'class' => $this->request->class
		]);
		return compact('class');
	}

	public function adminView() {
		return $this->_view();
	}

	public function prgLeadView() {
		return $this->_view();
	}

	public function acdServView() {
		return $this->_view();
	}

	/**
	 * Edit the class :class in the Program :program for the Semester :semester [1.1.1a]
	 *
	 * GET /api/semester/:semester/program/:program/class/:class
	 * PUT /api/semester/:semester/program/:program/class/:class
	 */
	protected function _edit() {
		$class = Classes::getClassBySemesterAndProgram([
			'program' => $this->request->program,
			'semester' => $this->request->semester,
			'class' => $this->request->class
		]);

		if ($this->request->is('put')) {
			$class->save($this->request->data);
		}

		return compact('class');
	}

	public function adminEdit() {
		return $this->_edit();
	}

	public function prgLeadEdit() {
		return $this->_edit();
	}

	public function acdServEdit() {
		return $this->_edit();
	}

	/**
	 * Edit the class :class in the Program :program for the Semester :semester [1.1.1a]
	 *
	 * DELETE /api/semester/:semester/program/:program/class/:class
	 */
	protected function _destroy() {
		$class = Classes::getClassBySemesterAndProgram([
			'program' => $this->request->program,
			'semester' => $this->request->semester,
			'class' => $this->request->class
		]);


		$concurrence = Concurrences::find('first', ['class_id' => $class->id]);
		$teacherAuthorizations = TeacherAuthorizations::find('first', ['class_id' => $class->id]);

		$class->delete();
		$concurrence->delete();
		$teacherAuthorizations->delete();
		return compact('class');
	}

	public function adminDestroy() {
		return $this->_destroy();
	}

	public function prgLeadDestroy() {
		return $this->_destroy();
	}

	public function acdServDestroy() {
		return $this->_destroy();
	}
}

?>