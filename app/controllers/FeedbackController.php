<?php

namespace app\controllers;

use app\models\Feedback;
use app\models\Sessions;
use lithium\g11n\Message;

use app\extensions\helper\Debug;

class FeedbackController extends \app\extensions\action\Controller {

	/**
	 * Displays a list of feedback [1.1.1.3]
	 *
	 * @url    GET /semester/:semester/program/:program/class/:class/feedback
	 */
	protected function _index() {
		$feedbacks = Feedback::all([
			'order' => ['id' => 'ASC'],
			'with' => ['Classes', 'Classes.Concurrences.Semesters', 'Classes.Modules.Programs'],
			'conditions' => [
				'Semesters.title' => $this->request->semester,
				'Programs.slug' => $this->request->program,
				'Classes.slug' => $this->request->class
			]
		]);

		return compact('feedbacks');
	}

	public function adminIndex() {
		return $this->_index();
	}

	public function prgLeadIndex() {
		return $this->_index();
	}

	public function acdServIndex() {
		return $this->_index();
	}


	/**
	 * View feedback with id :id for the semester :semester for the program :program in class :class [1.1.1.3a]
	 *
	 * @url GET /semester/:semester/program/:program/class/:class/feedback/:id
	 */
	protected function _view() {
		$feedback = Feedback::find('first', [
			'order' => ['id' => 'ASC'],
			'with' => ['Classes', 'Classes.Concurrences.Semesters', 'Classes.Modules.Programs'],
			'conditions' => [
				'Semesters.title' => $this->request->semester,
				'Programs.slug' => $this->request->program,
				'Classes.slug' => $this->request->class,
				'Feedback.id' => $this->request->id,
			]
		]);
		return compact('feedback');
	}

	public function adminView() {
		return $this->_view();
	}

	public function prgLeadView() {
		return $this->_view();
	}

	public function acdServView() {
		return $this->_view();
	}

	/**
	 * View feedback with id :id for the semester :semester in class :class for the currently
	 * logged in teacher [1.6.3a]
	 *
	 * @url GET /semester/:semester/myclasses/:class/feedback/:id
	 */
	public function teacherView() {
		$feedback = Feedback::find('first', [
			'order' => ['id' => 'ASC'],
			'with' => ['Classes', 'Classes.Concurrences.Semesters', 'Classes.Modules.Programs', 'Teachers'],
			'conditions' => [
				'Semesters.title' => $this->request->semester,
				'Classes.slug' => $this->request->class,
				'Feedback.id' => $this->request->id,
				'Teachers.id' => Sessions::id()
			]
		]);
		return compact('feedback');
	}


	/**
	 * Edit feedback with id :id for the semester :semester for the program :program in class :class [1.1.1.3a]
	 *
	 * @url GET /semester/:semester/program/:program/class/:class/feedback/:id
	 */
	protected function _edit() {
		$feedback = Feedback::find('first', [
			'order' => ['id' => 'ASC'],
			'with' => ['Classes', 'Classes.Concurrences.Semesters', 'Classes.Modules.Programs'],
			'conditions' => [
				'Semesters.title' => $this->request->semester,
				'Programs.slug' => $this->request->program,
				'Classes.slug' => $this->request->class,
				'Feedback.id' => $this->request->id,
			]
		]);

		if ($this->request->is('put')) {
			$feedback->save($this->request->data);
		}

		return compact('feedback');
	}

	public function adminEdit() {
		return $this->_edit();
	}

	public function prgLeadEdit() {
		return $this->_edit();
	}

	public function acdServEdit() {
		return $this->_edit();
	}

	/**
	 * Edit feedback with id :id for the semester :semester in class :class for the currently
	 * logged in teacher [1.6.3a]
	 *
	 * @url GET /semester/:semester/myclasses/:class/feedback/:id
	 * @url PUT /semester/:semester/myclasses/:class/feedback/:id
	 */
	public function teacherEdit() {
		$feedback = Feedback::find('first', [
			'order' => ['id' => 'ASC'],
			'with' => ['Classes', 'Classes.Concurrences.Semesters', 'Classes.Modules.Programs', 'Teachers'],
			'conditions' => [
				'Semesters.title' => $this->request->semester,
				'Programs.slug' => $this->request->program,
				'Classes.slug' => $this->request->class,
				'Feedback.id' => $this->request->id,
				'Teachers.id' => Sessions::id()
			]
		]);

		if ($this->request->is('put')) {
			$feedback->save($this->request->data);
		}

		return compact('feedback');
	}


	/**
	 * Delete feedback with id :id for the semester :semester for the program :program in class :class [1.1.1.3a]
	 *
	 * @url DELETE /semester/:semester/program/:program/class/:class/feedback/:id
	 */
	protected function _destroy() {
		$feedback = Feedback::find('first', [
			'order' => ['id' => 'ASC'],
			'with' => ['Classes', 'Classes.Concurrences.Semesters', 'Classes.Modules.Programs'],
			'conditions' => [
				'Semesters.title' => $this->request->semester,
				'Programs.slug' => $this->request->program,
				'Classes.slug' => $this->request->class,
				'Feedback.id' => $this->request->id,
			]
		]);

		$feedback->delete();

		return compact('feedback');
	}

	public function adminDestroy() {
		return $this->_destroy();
	}

	public function prgLeadDestroy() {
		return $this->_destroy();
	}

	public function acdServDestroy() {
		return $this->_destroy();
	}

	/**
	 * Delete feedback with id :id for the semester :semester in class :class for the currently
	 * logged in teacher [1.6.3a]
	 *
	 * @url DELETE /semester/:semester/myclasses/:class/feedback/:id
	 */
	public function teacherDestroy() {
		$feedback = Feedback::find('first', [
			'order' => ['id' => 'ASC'],
			'with' => ['Classes', 'Classes.Concurrences.Semesters', 'Classes.Modules.Programs', 'Teachers'],
			'conditions' => [
				'Semesters.title' => $this->request->semester,
				'Programs.slug' => $this->request->program,
				'Classes.slug' => $this->request->class,
				'Feedback.id' => $this->request->id,
				'Teachers.id' => Sessions::id()
			]
		]);

		$feedback->delete();

		return compact('feedback');
	}











































	/**
	 * [ short description of PREFIXCreate action ]
	 *
	 * [ long description of PREFIXCreate action ]
	 *
	 * @todo   Incomplete
	 * @url    GET  /feedbacks/create
	 * @url    POST /feedbacks/create
	 */
	public function PREFIXCreate() {
		$feedback = Feedback::create();

		if ($this->request->is('post')) {
			extract(Message::aliases());

			if ($feedback->save($this->request->data)) {
				$this->flashSuccess($t('The feedback was added.'));
				return $this->redirect([
					'Feedback::view',
					'id' => $feedback->id
				]);
			}
			$this->flashError($t('The feedback could not be added.'));
		}

		return compact('feedback');
	}
}

?>