<?php

namespace app\controllers;

use app\models\ProgramAuthorizations;
use app\extensions\helper\Debug;

class ProgramAuthorizationsController extends \app\extensions\action\Controller {

	/**
	 * Delete associations between Program Leaders and Programs
	 *
	 * @url    DELETE /programauthorizations [#129]
	 */
	public function adminDestroy() {
		foreach ((array) $this->request->data['program_authorizations'] as $authorization) {
			$programAuthorization = ProgramAuthorizations::find('first', [
				'conditions' => [
					'program_id' => $authorization['program_id'],
					'program_leader_id' => $authorization['program_leader_id']
				]
			]);
			$programAuthorization->delete();
		}

		return compact('programAuthorization');
	}


	/**
	 * Adds a program authorization if one doesn't already exist.
	 *
	 * @url    POST /programauthorizations [#129]
	 */
	public function adminCreate() {
		$programAuthorization = ProgramAuthorizations::create();

		if ($this->request->is('post')) {
			foreach ((array) $this->request->data['program_authorizations'] as $prgDatum) {
				$currentProgram = ProgramAuthorizations::find('first', ['conditions' => [
					'program_leader_id' => $prgDatum['program_leader_id'],
					'program_id' => $prgDatum['program_id']
				]]);
				if (!$currentProgram->exists()) {
					$programAuthorization = ProgramAuthorizations::create();
					$programAuthorization->save($prgDatum);
				}
			}
		}

		return compact('programAuthorization');
	}

	/**
	 * [ short description of PREFIXEdit action ]
	 *
	 * [ long description of PREFIXEdit action ]
	 *
	 * @todo   Incomplete
	 * @url    GET /programAuthorizations/:id/edit
	 * @url    PUT /programAuthorizations/:id/edit
	 */
	public function PREFIXEdit() {
		$programAuthorization = ProgramAuthorizations::find($this->request->id);
		if ($this->request->is('put')) {
			extract(Message::aliases());

			if ($programAuthorization->save($this->request->data)) {
				$this->flashSuccess($t('The programAuthorization was updated.'));
				return $this->redirect([
					'ProgramAuthorizations::view',
					'id' => $programAuthorization->id
				]);
			}
			$this->flashError($t('The programAuthorization could not be updated.'));
		}
		return compact('programAuthorization');
	}

	/**
	 * [ short description of PREFIXIndex action ]
	 *
	 * [ long description of PREFIXIndex action ]
	 *
	 * @todo   Incomplete
	 * @url    GET /programAuthorizations
	 * @url    GET /programAuthorizations/index
	 */
	public function PREFIXIndex() {
		$limit = 10;
		$page  = $this->request->page ?: 1;

		$programAuthorizations = ProgramAuthorizations::all([
			'limit' => $limit,
			'page'  => $page,
			'order' => ['id' => 'ASC']
		]);

		$total = ProgramAuthorizations::find('count');
		return compact('programAuthorizations', 'total', 'limit', 'page');
	}

	/**
	 * Allows users with role `PREFIX` to search within this model.
	 *
	 * @todo   Incomplete
	 * @see    app\models\ProgramAuthorizations
	 * @url    GET /programAuthorizations/search/q=<search term>&<field_1>=&<field_2>...
	 */
	public function PREFIXSearch() {
		return $this->search();
	}

	/**
	 * [ short description of PREFIXView action ]
	 *
	 * [ long description of PREFIXView action ]
	 *
	 * @todo   Incomplete
	 * @url GET /programAuthorizations/1
	 */
	public function PREFIXView() {
		$programAuthorization = ProgramAuthorizations::find($this->request->id);
		return compact('programAuthorization');
	}
}

?>