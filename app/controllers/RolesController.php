<?php

namespace app\controllers;

use app\models\Roles;
use app\models\Users;
use lithium\g11n\Message;

use app\extensions\helper\Debug;

class RolesController extends \app\extensions\action\Controller {

	/**
	 * @url    GET /roles
	 * @url    GET /roles/index
	 */
	public function _index() {
		$limit = 10;
		$page  = $this->request->page ?: 1;

		$roles = Roles::all([
//			'limit' => $limit,
//			'page'  => $page,
			'order' => ['id' => 'ASC']
		]);

		$total = Roles::find('count');
		return compact('roles', 'total', 'limit', 'page');
	}

	public function adminIndex() {
		return $this->_index();
	}

	public function acdServIndex() {
		return $this->_index();
	}

	public function extVerIndex() {
		return $this->_index();
	}

	public function studRecIndex() {
		return $this->_index();
	}

	public function teacherIndex() {
		return $this->_index();
	}

	public function prgLeadIndex() {
		return $this->_index();
	}

	/**
	 * Endpoint to show all users for a particular role.
	 * Roles.label acts as the slug (:role).
	 *
	 * @url    GET /role/:role
	 */
	public function _users() {
		$limit = 10;
		$page  = $this->request->page ?: 1;

		$roles = Users::all([
			'fields' => Users::safeFields(),
			'with' => ['RolesUsers.Roles'],
//			'limit' => $limit,
//			'page'  => $page,
			'order' => ['id' => 'ASC'],
			'conditions' => ['Roles.label' => $this->request->role]
		]);

		$total = Roles::find('count');
		return compact('roles', 'total', 'limit', 'page');
	}

	public function adminUsers() {
		return $this->_users();
	}

	public function acdServUsers() {
		return $this->_users();
	}

	public function extVerUsers() {
		return $this->_users();
	}

	public function studRecUsers() {
		return $this->_users();
	}

	public function teacherUsers() {
		return $this->_users();
	}

	public function prgLeadUsers() {
		return $this->_users();
	}


	/**
	 * [ short description of PREFIXDestroy action ]
	 *
	 * [ long description of PREFIXDestroy action ]
	 *
	 * @todo   Incomplete
	 * @url    DELETE /roles/:id
	 */
	public function PREFIXDestroy() {
		extract(Message::aliases());
		$role = Roles::find($this->request->id);

		if ($role->delete()) {
			$this->flashSuccess($t('The role was deleted.'));
			return $this->redirect(['Roles::index']);
		}

		$this->flashError($t('The role could not be deleted.'));
		return $this->redirect($this->request->referer());
	}

	/**
	 * [ short description of PREFIXEdit action ]
	 *
	 * [ long description of PREFIXEdit action ]
	 *
	 * @todo   Incomplete
	 * @url    GET /roles/:id/edit
	 * @url    PUT /roles/:id/edit
	 */
	public function PREFIXEdit() {
		$role = Roles::find($this->request->id);
		if ($this->request->is('put')) {
			extract(Message::aliases());

			if ($role->save($this->request->data)) {
				$this->flashSuccess($t('The role was updated.'));
				return $this->redirect([
					'Roles::view',
					'id' => $role->id
				]);
			}
			$this->flashError($t('The role could not be updated.'));
		}
		return compact('role');
	}

	/**
	 * [ short description of PREFIXCreate action ]
	 *
	 * [ long description of PREFIXCreate action ]
	 *
	 * @todo   Incomplete
	 * @url    GET  /roles/create
	 * @url    POST /roles/create
	 */
	public function PREFIXCreate() {
		$role = Roles::create();

		if ($this->request->is('post')) {
			extract(Message::aliases());

			if ($role->save($this->request->data)) {
				$this->flashSuccess($t('The role was added.'));
				return $this->redirect([
					'Roles::view',
					'id' => $role->id
				]);
			}
			$this->flashError($t('The role could not be added.'));
		}

		return compact('role');
	}

	/**
	 * Allows users with role `PREFIX` to search within this model.
	 *
	 * @todo   Incomplete
	 * @see    app\models\Roles
	 * @url    GET /roles/search/q=<search term>&<field_1>=&<field_2>...
	 */
	public function PREFIXSearch() {
		return $this->search();
	}

	/**
	 * [ short description of PREFIXView action ]
	 *
	 * [ long description of PREFIXView action ]
	 *
	 * @todo   Incomplete
	 * @url GET /roles/1
	 */
	public function PREFIXView() {
		$role = Roles::find($this->request->id);
		return compact('role');
	}
}

?>