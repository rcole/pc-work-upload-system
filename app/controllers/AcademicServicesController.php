<?php

namespace app\controllers;

use app\models\AcademicServices;
use lithium\g11n\Message;

use app\extensions\helper\Debug;

class AcademicServicesController extends \app\extensions\action\Controller {

	/**
	 * [ short description of PREFIXDestroy action ]
	 *
	 * [ long description of PREFIXDestroy action ]
	 *
	 * @todo   Incomplete
	 * @url    DELETE /academicServices/:id
	 */
	public function PREFIXDestroy() {
		extract(Message::aliases());
		$academicService = AcademicServices::find($this->request->id);

		if ($academicService->delete()) {
			$this->flashSuccess($t('The academicService was deleted.'));
			return $this->redirect(['AcademicServices::index']);
		}

		$this->flashError($t('The academicService could not be deleted.'));
		return $this->redirect($this->request->referer());
	}

	/**
	 * [ short description of PREFIXEdit action ]
	 *
	 * [ long description of PREFIXEdit action ]
	 *
	 * @todo   Incomplete
	 * @url    GET /academicServices/:id/edit
	 * @url    PUT /academicServices/:id/edit
	 */
	public function PREFIXEdit() {
		$academicService = AcademicServices::find($this->request->id);
		if ($this->request->is('put')) {
			extract(Message::aliases());

			if ($academicService->save($this->request->data)) {
				$this->flashSuccess($t('The academicService was updated.'));
				return $this->redirect([
					'AcademicServices::view',
					'id' => $academicService->id
				]);
			}
			$this->flashError($t('The academicService could not be updated.'));
		}
		return compact('academicService');
	}

	/**
	 * [ short description of PREFIXIndex action ]
	 *
	 * [ long description of PREFIXIndex action ]
	 *
	 * @todo   Incomplete
	 * @url    GET /academicServices
	 * @url    GET /academicServices/index
	 */
	public function PREFIXIndex() {
		$limit = 10;
		$page  = $this->request->page ?: 1;

		$academicServices = AcademicServices::all([
			'limit' => $limit,
			'page'  => $page,
			'order' => ['id' => 'ASC']
		]);

		$total = AcademicServices::find('count');
		return compact('academicServices', 'total', 'limit', 'page');
	}

	/**
	 * [ short description of PREFIXCreate action ]
	 *
	 * [ long description of PREFIXCreate action ]
	 *
	 * @todo   Incomplete
	 * @url    GET  /academicServices/create
	 * @url    POST /academicServices/create
	 */
	public function PREFIXCreate() {
		$academicService = AcademicServices::create();

		if ($this->request->is('post')) {
			extract(Message::aliases());

			if ($academicService->save($this->request->data)) {
				$this->flashSuccess($t('The academicService was added.'));
				return $this->redirect([
					'AcademicServices::view',
					'id' => $academicService->id
				]);
			}
			$this->flashError($t('The academicService could not be added.'));
		}

		return compact('academicService');
	}

	/**
	 * Allows users with role `PREFIX` to search within this model.
	 *
	 * @todo   Incomplete
	 * @see    app\models\AcademicServices
	 * @url    GET /academicServices/search/q=<search term>&<field_1>=&<field_2>...
	 */
	public function PREFIXSearch() {
		return $this->search();
	}

	/**
	 * [ short description of PREFIXView action ]
	 *
	 * [ long description of PREFIXView action ]
	 *
	 * @todo   Incomplete
	 * @url GET /academicServices/1
	 */
	public function PREFIXView() {
		$academicService = AcademicServices::find($this->request->id);
		return compact('academicService');
	}
}

?>