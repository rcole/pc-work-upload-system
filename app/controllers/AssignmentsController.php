<?php

namespace app\controllers;

use app\models\Assignments;
use app\models\Users;
use lithium\analysis\Debugger;
use app\models\Sessions;

class AssignmentsController extends \app\extensions\action\Controller {

	/**
	 * A list of Assignments in a class for a semester [1.1.1.1]
	 *
	 * @url    GET /semester/:semester/program/:program/class/:class/assignments
	 */
	protected function _index() {
		$assignments = Assignments::all([
			'order' => ['id' => 'ASC'],
			'with' => ['Classes', 'Classes.Concurrences.Semesters', 'Classes.Modules.Programs'],
			'conditions' => [
				'Semesters.slug' => $this->request->semester,
				'Programs.slug' => $this->request->program,
				'Classes.slug' => $this->request->class
			]
		]);
		return compact('assignments');
	}

	public function adminIndex() {
		return $this->_index();
	}

	public function prgLeadIndex() {
		return $this->_index();
	}

	public function acdServIndex() {
		return $this->_index();
	}

	/**
	 * A list of Assignments in a class a teacher is authorized for [1.6.1]
	 *
	 * @url    GET /semester/:semester/myclasses/:class/assignments
	 */
	protected function teacherIndex() {
		$assignments = Assignments::all([
			'order' => ['id' => 'ASC'],
			'with' => ['Classes', 'Classes.Concurrences.Semesters', 'Classes.TeacherAuthorizations.Teachers'],
			'conditions' => [
				'Semesters.title' => $this->request->semester,
				'Classes.slug' => $this->request->class,
				'Teachers.id' => Sessions::id()
			]
		]);
		return compact('assignments');
	}

	/**
	 * Create Assignments for a class [1.1.1.1]
	 *
	 * @url    POST /semester/:semester/program/:program/class/:class/assignments
	 */
	protected function _create() {
		$assignment = Assignments::create();

		if ($this->request->is('post')) {
			$assignment->save($this->request->data);
		}

		return compact('assignment');
	}

	public function adminCreate() {
		return $this->_create();
	}

	public function prgLeadCreate() {
		return $this->_create();
	}

	public function acdServCreate() {
		return $this->_create();
	}

	/**
	 * [1.6.1]
	 *
	 * @url    POST /semester/:semester/myclasses/:class/assignments
	 */
	public function teacherCreate() {
		return $this->_create();
	}

	/**
	 * Displays an Assignment in a class :class for semester :semester for the program :program [1.1.1.1a] [1.1.1.5]
	 *
	 * @url    GET /semester/:semester/program/:program/class/:class/assignment/:assignment
	 */
	protected function _view() {
		$assignment = Assignments::find('first', [
			'order' => ['id' => 'ASC'],
			'with' => ['Classes', 'Classes.Concurrences.Semesters', 'Classes.Modules.Programs', 'Submissions'],
			'conditions' => [
				'Semesters.slug' => $this->request->semester,
				'Programs.slug' => $this->request->program,
				'Classes.slug' => $this->request->class,
				'Assignments.slug' => $this->request->assignment
			]
		]);
		return compact('assignment');
	}

	public function adminView() {
		return $this->_view();
	}

	public function prgLeadView() {
		return $this->_view();
	}

	public function acdServView() {
		return $this->_view();
	}

	/**
	 * Displays assignment :assignment class :class for semester :semester for the program :program
	 * for the current teacher that is logged in [1.1.1.1a] [1.1.1.5]
	 *
	 * @url    GET /semester/:semester/myclasses/:class/assignment/:assignment
	 */
	public function teacherView() {
		$fields = Users::safeFields([], 'Teachers');
		$fields[] = 'Assignments.*';
		$fields[] = 'Classes.*';
		$fields[] = 'Concurrences.*';
		$fields[] = 'Semesters.*';
		$fields[] = 'Modules.*';
		$fields[] = 'Programs.*';
		$fields[] = 'TeacherAuthorizations.*';

		$assignment = Assignments::find('first', [
			'fields' => $fields,
			'with' => ['Classes', 'Classes.Concurrences.Semesters', 'Classes.Modules.Programs', 'Classes.TeacherAuthorizations.Teachers'],
			'conditions' => [
				'Semesters.slug' => $this->request->semester,
				'Classes.slug' => $this->request->class,
				'Assignments.slug' => $this->request->assignment,
				'Teachers.id' => Sessions::id()
			]
		]);
		return compact('assignment');
	}


	/**
	 * Edit Assignment in class :class for semester :semester for the program :program [1.1.1.1a] [1.1.1.5]
	 *
	 * @url    GET /semester/:semester/program/:program/class/:class/assignment/:assignment
	 * @url    PUT /semester/:semester/program/:program/class/:class/assignment/:assignment
	 */
	protected function _edit() {
		$assignment = Assignments::find('first', [
			'order' => ['id' => 'ASC'],
			'with' => ['Classes', 'Classes.Concurrences.Semesters', 'Classes.Modules.Programs'],
			'conditions' => [
				'Semesters.slug' => $this->request->semester,
				'Programs.slug' => $this->request->program,
				'Classes.slug' => $this->request->class,
				'Assignments.slug' => $this->request->assignment
			]
		]);

		if ($this->request->is('put')) {
			$assignment->save($this->request->data);
		}

		return compact('assignment');
	}

	public function adminEdit() {
		return $this->_edit();
	}

	public function prgLeadEdit() {
		return $this->_edit();
	}

	public function acdServEdit() {
		return $this->_edit();
	}

	/**
	 * Edit assignment :assignment class :class for semester :semester for the program :program
	 * for the current teacher that is logged in [1.1.1.1a] [1.1.1.5]
	 *
	 * @url    GET /semester/:semester/myclasses/:class/assignment/:assignment
	 * @url    PUT /semester/:semester/myclasses/:class/assignment/:assignment
	 */
	public function teacherEdit() {
		$assignment = Assignments::find('first', [
			'order' => ['id' => 'ASC'],
			'with' => ['Classes', 'Classes.Concurrences.Semesters', 'Classes.Modules.Programs', 'Classes.TeacherAuthorizations.Teachers'],
			'conditions' => [
				'Semesters.slug' => $this->request->semester,
				'Classes.slug' => $this->request->class,
				'Assignments.slug' => $this->request->assignment,
				'Teachers.id' => Sessions::id()
			]
		]);

		if ($this->request->is('put')) {
			$assignment->save($this->request->data);
		}

		return compact('assignment');
	}


	/**
	 * Delete assignment :assignment class :class for semester :semester for the program :program [1.1.1.1a]
	 *
	 * @url    DELETE /semester/:semester/program/:program/class/:class/assignment/:assignment
	 */
	protected function _destroy() {
		$assignment = Assignments::find('first', [
			'order' => ['id' => 'ASC'],
			'with' => ['Classes', 'Classes.Concurrences.Semesters', 'Classes.Modules.Programs', 'Classes.TeacherAuthorizations.Teachers'],
			'conditions' => [
				'Semesters.slug' => $this->request->semester,
				'Classes.slug' => $this->request->class,
				'Assignments.slug' => $this->request->assignment
			]
		]);

		$assignment->delete();
		return compact('assignment');
	}

	public function adminDestroy() {
		return $this->_destroy();
	}

	public function prgLeadDestroy() {
		return $this->_destroy();
	}

	public function acdServDestroy() {
		return $this->_destroy();
	}
}

?>