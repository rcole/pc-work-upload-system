<?php

namespace app\controllers;

use app\models\ExtensionCodes;
use app\models\Sessions;
use lithium\g11n\Message;

use app\extensions\helper\Debug;

class ExtensionCodesController extends \app\extensions\action\Controller {

	/**
	 * A list of statuses.
	 *
	 * @see    \app\models\ExtensionCodes::$_statuses
	 * @url    GET /extensioncodes/statuses
	 */
	protected function _statuses() {
		$extensionCodes = new ExtensionCodes;
		return json_encode($extensionCodes->statuses());
	}

	public function adminStatuses() {
		return $this->_statuses();
	}

	public function acdServStatuses() {
		return $this->_statuses();
	}

	/**
	 * Shows a single statuse whose id is :status
	 *
	 * @see    \app\models\ExtensionCodes::$_statuses
	 * @url    GET /extensioncodes/status/:status
	 */
	protected function _status() {
		$extensionCodes = new ExtensionCodes;
		return json_encode($extensionCodes->status($this->request->status));
	}

	public function adminStatus() {
		return $this->_status();
	}

	public function acdServStatus() {
		return $this->_status();
	}

	/**
	 * A list of all Extension Codes [1.3]
	 *
	 * @url    GET /extensionCodes
	 */
	protected function _index() {
		$extensionCodes = ExtensionCodes::all(['order' => ['id' => 'ASC']]);
		return compact('extensionCodes');
	}

	public function adminIndex() {
		return $this->_index();
	}

	public function acdServIndex() {
		return $this->_index();
	}

	/**
	 * A list of Extension Codes granted in all authorized Classes [1.7]
	 *
	 * @url    GET /semester/:semester/myclasses/extensioncodes
	 */
	public function teacherIndex() {
		$extensionCodes = ExtensionCodes::all([
			'order' => ['id' => 'ASC'],
			'with' => [
				'Submissions.Assignments.Classes.Concurrences.Semesters',
				'Submissions.Assignments.Classes.TeacherAuthorizations.Teachers'
			],
			'conditions' => [
				'Semesters.slug' => $this->request->semester,
				'Teachers.id' => Sessions::id()
			]
		]);
		return compact('extensionCodes');
	}

	/**
	 * A list of Extension Codes granted to the current Student [2.3]
	 *
	 * @url    GET /semester/:semester/myextensioncodes
	 */
	public function studentIndex() {
		$extensionCodes = ExtensionCodes::all([
			'order' => ['id' => 'ASC'],
			'with' => [
				'Submissions.Assignments.Classes.Concurrences.Semesters',
				'Submissions.Students'
			],
			'conditions' => [
				'Semesters.slug' => $this->request->semester,
				'Students.id' => Sessions::id()
			]
		]);
		return compact('extensionCodes');
	}

	/**
	 * [1.3]
	 *
	 * @url    POST /extensioncodes
	 */
	protected function _create() {
		$extensionCode = ExtensionCodes::create();

		if ($this->request->is('post')) {
			$extensionCode->save($this->request->data);
		}

		return compact('extensionCode');
	}

	public function adminCreate() {
		return $this->_create();
	}

	public function acdServCreate() {
		return $this->_create();
	}


	/**
	 * Displays extension code with id :id [1.3a]
	 *
	 * @url GET /api/extensioncode/:id
	 */
	protected function _view() {
		$extensionCode = ExtensionCodes::find($this->request->id);
		return compact('extensionCode');
	}

	public function adminView() {
		return $this->_view();
	}

	public function acdServView() {
		return $this->_view();
	}


	/**
	 * Edits extension code with id :id [1.3a]
	 *
	 * @url GET /api/extensioncode/:id
	 * @url PUT /api/extensioncode/:id
	 */
	protected function _edit() {
		$extensionCode = ExtensionCodes::find($this->request->id);
		if ($this->request->is('put')) {
			$extensionCode->save($this->request->data);
		}
		return compact('extensionCode');
	}

	public function adminEdit() {
		return $this->_edit();
	}

	public function acdServEdit() {
		return $this->_edit();
	}


	/**
	 * Deletes extension code with id :id [1.3a]
	 *
	 * @url DELETE /api/extensioncode/:id
	 */
	public function PREFIXDestroy() {
		$extensionCode = ExtensionCodes::find($this->request->id);

		$extensionCode->delete();
		return compact('extensionCode');
	}
}

?>