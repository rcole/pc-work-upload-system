<?php

namespace app\controllers;

use app\models\Concurrences;
use lithium\g11n\Message;

use app\extensions\helper\Debug;

class ConcurrencesController extends \app\extensions\action\Controller {

	/**
	 * [ short description of PREFIXDestroy action ]
	 *
	 * [ long description of PREFIXDestroy action ]
	 *
	 * @todo   Incomplete
	 * @url    DELETE /concurrences/:id
	 */
	public function PREFIXDestroy() {
		extract(Message::aliases());
		$concurrence = Concurrences::find($this->request->id);

		if ($concurrence->delete()) {
			$this->flashSuccess($t('The concurrence was deleted.'));
			return $this->redirect(['Concurrences::index']);
		}

		$this->flashError($t('The concurrence could not be deleted.'));
		return $this->redirect($this->request->referer());
	}

	/**
	 * [ short description of PREFIXEdit action ]
	 *
	 * [ long description of PREFIXEdit action ]
	 *
	 * @todo   Incomplete
	 * @url    GET /concurrences/:id/edit
	 * @url    PUT /concurrences/:id/edit
	 */
	public function PREFIXEdit() {
		$concurrence = Concurrences::find($this->request->id);
		if ($this->request->is('put')) {
			extract(Message::aliases());

			if ($concurrence->save($this->request->data)) {
				$this->flashSuccess($t('The concurrence was updated.'));
				return $this->redirect([
					'Concurrences::view',
					'id' => $concurrence->id
				]);
			}
			$this->flashError($t('The concurrence could not be updated.'));
		}
		return compact('concurrence');
	}

	/**
	 * [ short description of PREFIXIndex action ]
	 *
	 * [ long description of PREFIXIndex action ]
	 *
	 * @todo   Incomplete
	 * @url    GET /concurrences
	 * @url    GET /concurrences/index
	 */
	public function PREFIXIndex() {
		$limit = 10;
		$page  = $this->request->page ?: 1;

		$concurrences = Concurrences::all([
			'limit' => $limit,
			'page'  => $page,
			'order' => ['id' => 'ASC']
		]);

		$total = Concurrences::find('count');
		return compact('concurrences', 'total', 'limit', 'page');
	}

	/**
	 * [ short description of PREFIXCreate action ]
	 *
	 * [ long description of PREFIXCreate action ]
	 *
	 * @todo   Incomplete
	 * @url    GET  /concurrences/create
	 * @url    POST /concurrences/create
	 */
	public function PREFIXCreate() {
		$concurrence = Concurrences::create();

		if ($this->request->is('post')) {
			extract(Message::aliases());

			if ($concurrence->save($this->request->data)) {
				$this->flashSuccess($t('The concurrence was added.'));
				return $this->redirect([
					'Concurrences::view',
					'id' => $concurrence->id
				]);
			}
			$this->flashError($t('The concurrence could not be added.'));
		}

		return compact('concurrence');
	}

	/**
	 * Allows users with role `PREFIX` to search within this model.
	 *
	 * @todo   Incomplete
	 * @see    app\models\Concurrences
	 * @url    GET /concurrences/search/q=<search term>&<field_1>=&<field_2>...
	 */
	public function PREFIXSearch() {
		return $this->search();
	}

	/**
	 * [ short description of PREFIXView action ]
	 *
	 * [ long description of PREFIXView action ]
	 *
	 * @todo   Incomplete
	 * @url GET /concurrences/1
	 */
	public function PREFIXView() {
		$concurrence = Concurrences::find($this->request->id);
		return compact('concurrence');
	}
}

?>