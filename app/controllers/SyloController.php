<?php

namespace app\controllers;

use app\models\Sylos;
use lithium\g11n\Message;

use app\extensions\helper\Debug;

class SyloController extends \app\extensions\action\Controller {
	/**
	 * Catch all for Angular to handle.
	 */
	protected function _index() {
		return true;
	}

	public function adminIndex() {
		return $this->_index();
	}

	public function prgLeadIndex() {
		return $this->_index();
	}

	public function teacherIndex() {
		return $this->_index();
	}

	public function acdServIndex() {
		return $this->_index();
	}

	public function extVerIndex() {
		return $this->_index();
	}
	public function studRecIndex() {
		return $this->_index();
	}
	public function studentIndex() {
		return $this->_index();
	}
}

?>