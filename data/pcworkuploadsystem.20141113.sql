-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 13, 2014 at 12:04 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pcworkuploadsystem`
--

-- --------------------------------------------------------

--
-- Table structure for table `assignments`
--

CREATE TABLE IF NOT EXISTS `assignments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_id` int(11) NOT NULL,
  `semester_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Slug of the name. Added automatically',
  `visibility` int(1) NOT NULL COMMENT '0 = hidden; 1 = visible;',
  `due` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `updated` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `class_id` (`class_id`),
  KEY `semester_id` (`semester_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `assignments`
--

INSERT INTO `assignments` (`id`, `class_id`, `semester_id`, `name`, `slug`, `visibility`, `due`, `created`, `updated`) VALUES
(1, 5, 4, 'Semesters first assignment', 'Semesters-first-assignment', 1, 1393632000, 1415801434, 1415801434),
(2, 4, 4, 'Semesters second assignment', 'Semesters-second-assignment', 1, 1393632000, 1415801434, 1415801434);

-- --------------------------------------------------------

--
-- Table structure for table `authorizations`
--

CREATE TABLE IF NOT EXISTS `authorizations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `updated` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `class_id` (`class_id`),
  KEY `teacher_id` (`teacher_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE IF NOT EXISTS `classes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Slug of the title. Added automatically',
  `begins` int(11) DEFAULT NULL COMMENT 'Can be specified if class has a beginning date different to that of Semesters.begins',
  `ends` int(11) DEFAULT NULL COMMENT 'Can be specified if class has a ending date different to that of Semesters.ends',
  `created` int(11) NOT NULL,
  `updated` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `module_id` (`module_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`id`, `module_id`, `title`, `slug`, `begins`, `ends`, `created`, `updated`) VALUES
(1, 1, 'Intro to Rasters', 'Intro-to-Rasters', 1404259200, 1412035200, 1415801434, 1415801434),
(9, 12, 'CCR (NAJ)', 'CCR-NAJ', NULL, 1422572400, 1415830714, 1415830714),
(6, 7, 'Mass Text (CAR/BRO)', 'Mass-Text-CAR-BRO', 1412208000, 1419984000, 1415802504, 1415802504),
(7, 7, 'Mass Text Application (CAR/BRO)', 'Mass-Text-Application-CAR-BRO', 1412028000, 1422658800, 1415830512, 1415830512),
(10, 12, 'CCR (SCH)', 'CCR-SCH', 1412028000, 1422572400, 1415830781, 1415830781),
(11, 2, 'Interactive Media Design and Prototyping', 'Interactive-Media-Design-and-Prototyping', 1412028000, 1422572400, 1415830874, 1415830874),
(12, 14, 'Information and Interpretation in A&D (1404)', 'Information-and-Interpretation-in-A-D-1404', 1412028000, 1422572400, 1415831426, 1415831426),
(13, 14, 'Researching, Recording and Responding in A&D (1404)', 'Researching-Recording-and-Responding-in-A-D-1404', 1412028000, 1422572400, 1415831474, 1415831474),
(14, 15, 'Extended Media Development (1404)', 'Extended-Media-Development-1404', 1412028000, 1422572400, 1415831581, 1415831581),
(15, 16, 'DMAD (1404)', 'DMAD-1404', 1412028000, 1422572400, 1415831739, 1415831739),
(16, 17, 'DICD (1404)', 'DICD-1404', 1412028000, 1422572400, 1415831771, 1415831771);

-- --------------------------------------------------------

--
-- Table structure for table `concurrences`
--

CREATE TABLE IF NOT EXISTS `concurrences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_id` int(11) NOT NULL,
  `semester_id` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `updated` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `class_id` (`class_id`),
  KEY `semester_id` (`semester_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Dumping data for table `concurrences`
--

INSERT INTO `concurrences` (`id`, `class_id`, `semester_id`, `created`, `updated`) VALUES
(10, 10, 5, 1415830781, 1415830781),
(9, 9, 5, 1415830714, 1415830714),
(7, 7, 5, 1415830512, 1415830512),
(6, 6, 4, 1415802504, 1415802504),
(5, 5, 4, 1415801434, 1415801434),
(11, 11, 5, 1415830874, 1415830874),
(12, 12, 5, 1415831426, 1415831426),
(13, 13, 5, 1415831474, 1415831474),
(14, 14, 5, 1415831581, 1415831581),
(15, 15, 5, 1415831739, 1415831739),
(16, 16, 5, 1415831771, 1415831771);

-- --------------------------------------------------------

--
-- Table structure for table `documentations`
--

CREATE TABLE IF NOT EXISTS `documentations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `semester_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Name of the uploaded file',
  `created` int(11) NOT NULL,
  `updated` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `class_id` (`class_id`),
  KEY `semester_id` (`semester_id`),
  KEY `teacher_id` (`teacher_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `documentations`
--

INSERT INTO `documentations` (`id`, `class_id`, `teacher_id`, `semester_id`, `title`, `created`, `updated`) VALUES
(1, 1, 1, 4, 'Documentation for Intro to Rasters by Housni', 1415801434, 1415801434),
(2, 1, 2, 4, 'Documentation for Intro to Rasters by Ryan', 1415801434, 1415801434),
(3, 2, 2, 4, 'Documentation for Tracing Rasters by Ryan', 1415801434, 1415801434),
(4, 3, 1, 4, 'Documentation for Intro to Pottery by Housni', 1415801434, 1415801434),
(5, 4, 1, 4, 'Documentation for Photoshop 101 by Housni', 1415801434, 1415801434);

-- --------------------------------------------------------

--
-- Table structure for table `enrollments`
--

CREATE TABLE IF NOT EXISTS `enrollments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_id` int(11) NOT NULL,
  `semester_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `updated` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `class_id` (`class_id`),
  KEY `student_id` (`student_id`),
  KEY `semester_id` (`semester_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=86 ;

--
-- Dumping data for table `enrollments`
--

INSERT INTO `enrollments` (`id`, `class_id`, `semester_id`, `student_id`, `created`, `updated`) VALUES
(1, 15, 5, 72, 1415831872, 1415831872),
(2, 15, 5, 71, 1415831872, 1415831872),
(3, 15, 5, 70, 1415831872, 1415831872),
(4, 15, 5, 69, 1415831872, 1415831872),
(5, 15, 5, 68, 1415831872, 1415831872),
(6, 16, 5, 67, 1415831998, 1415831998),
(7, 16, 5, 66, 1415831998, 1415831998),
(8, 16, 5, 65, 1415831998, 1415831998),
(9, 16, 5, 64, 1415831998, 1415831998),
(10, 16, 5, 63, 1415831998, 1415831998),
(11, 16, 5, 62, 1415831998, 1415831998),
(12, 16, 5, 61, 1415831998, 1415831998),
(13, 16, 5, 60, 1415831998, 1415831998),
(14, 16, 5, 59, 1415831998, 1415831998),
(15, 16, 5, 58, 1415831998, 1415831998),
(16, 16, 5, 57, 1415831998, 1415831998),
(17, 16, 5, 56, 1415831998, 1415831998),
(18, 16, 5, 55, 1415831998, 1415831998),
(19, 13, 5, 54, 1415832133, 1415832133),
(20, 13, 5, 53, 1415832133, 1415832133),
(21, 13, 5, 52, 1415832133, 1415832133),
(22, 13, 5, 51, 1415832133, 1415832133),
(23, 13, 5, 50, 1415832133, 1415832133),
(24, 13, 5, 49, 1415832133, 1415832133),
(25, 13, 5, 48, 1415832133, 1415832133),
(26, 13, 5, 47, 1415832133, 1415832133),
(27, 13, 5, 46, 1415832133, 1415832133),
(28, 13, 5, 45, 1415832133, 1415832133),
(29, 13, 5, 44, 1415832133, 1415832133),
(30, 13, 5, 43, 1415832133, 1415832133),
(31, 13, 5, 42, 1415832133, 1415832133),
(32, 13, 5, 41, 1415832133, 1415832133),
(33, 12, 5, 84, 1415832772, 1415832772),
(34, 12, 5, 83, 1415832772, 1415832772),
(35, 12, 5, 80, 1415832772, 1415832772),
(36, 12, 5, 81, 1415832772, 1415832772),
(37, 12, 5, 82, 1415832772, 1415832772),
(38, 12, 5, 79, 1415832772, 1415832772),
(39, 12, 5, 78, 1415832772, 1415832772),
(40, 12, 5, 85, 1415832859, 1415832859),
(41, 14, 5, 83, 1415832924, 1415832924),
(42, 14, 5, 80, 1415832924, 1415832924),
(43, 14, 5, 84, 1415832924, 1415832924),
(44, 14, 5, 85, 1415832924, 1415832924),
(45, 14, 5, 82, 1415832924, 1415832924),
(46, 14, 5, 81, 1415832924, 1415832924),
(47, 14, 5, 79, 1415832924, 1415832924),
(48, 14, 5, 78, 1415832924, 1415832924),
(49, 11, 5, 40, 1415833044, 1415833044),
(50, 11, 5, 39, 1415833044, 1415833044),
(51, 11, 5, 38, 1415833044, 1415833044),
(52, 11, 5, 37, 1415833044, 1415833044),
(53, 11, 5, 36, 1415833044, 1415833044),
(54, 11, 5, 35, 1415833044, 1415833044),
(55, 11, 5, 34, 1415833044, 1415833044),
(56, 11, 5, 33, 1415833044, 1415833044),
(57, 11, 5, 31, 1415833044, 1415833044),
(58, 9, 5, 30, 1415833137, 1415833137),
(59, 9, 5, 29, 1415833137, 1415833137),
(60, 9, 5, 28, 1415833137, 1415833137),
(61, 9, 5, 27, 1415833137, 1415833137),
(62, 9, 5, 26, 1415833137, 1415833137),
(63, 9, 5, 25, 1415833137, 1415833137),
(64, 9, 5, 24, 1415833137, 1415833137),
(65, 9, 5, 23, 1415833137, 1415833137),
(66, 9, 5, 22, 1415833137, 1415833137),
(67, 10, 5, 21, 1415833190, 1415833190),
(68, 10, 5, 20, 1415833190, 1415833190),
(69, 10, 5, 19, 1415833190, 1415833190),
(70, 10, 5, 18, 1415833190, 1415833190),
(71, 10, 5, 17, 1415833190, 1415833190),
(72, 10, 5, 16, 1415833190, 1415833190),
(73, 10, 5, 15, 1415833190, 1415833190),
(74, 7, 5, 14, 1415833283, 1415833283),
(75, 7, 5, 13, 1415833283, 1415833283),
(76, 7, 5, 12, 1415833283, 1415833283),
(77, 7, 5, 11, 1415833283, 1415833283),
(78, 7, 5, 10, 1415833283, 1415833283),
(79, 7, 5, 9, 1415833283, 1415833283),
(80, 7, 5, 8, 1415833283, 1415833283),
(81, 7, 5, 5, 1415833283, 1415833283),
(82, 7, 5, 6, 1415833283, 1415833283),
(83, 7, 5, 7, 1415833283, 1415833283),
(84, 7, 5, 4, 1415833283, 1415833283),
(85, 7, 5, 3, 1415833283, 1415833283);

-- --------------------------------------------------------

--
-- Table structure for table `extension_codes`
--

CREATE TABLE IF NOT EXISTS `extension_codes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `submission_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `assignment_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `title` varchar(7) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '0 = Unused; 1 = Used; 10 = Expired; 20 = Canceled',
  `expires_date` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `updated` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`),
  KEY `submission_id` (`submission_id`),
  KEY `user_id` (`user_id`),
  KEY `assignment_id` (`assignment_id`),
  KEY `student_id` (`student_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `extension_codes`
--

INSERT INTO `extension_codes` (`id`, `submission_id`, `user_id`, `assignment_id`, `student_id`, `title`, `status`, `expires_date`, `created`, `updated`) VALUES
(1, 1, 60, 2, 71, '235D950', 'claimed', 1413802760, 1415801434, 1415801434),
(2, 2, 60, 2, 70, '71E36E2', 'expired', 1413802760, 1415801434, 1415801434),
(3, 3, 60, 0, 73, '02418CB', 'unclaimed', 1413802760, 1415801434, 1415801434),
(4, 4, 60, 0, 72, '0DCB4B4', 'unclaimed', 1413802760, 1415801434, 1415801434);

-- --------------------------------------------------------

--
-- Table structure for table `feedbacks`
--

CREATE TABLE IF NOT EXISTS `feedbacks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `semester_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Name of the uploaded file',
  `created` int(11) NOT NULL,
  `updated` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `class_id` (`class_id`),
  KEY `semester_id` (`semester_id`),
  KEY `teacher_id` (`teacher_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `feedbacks`
--

INSERT INTO `feedbacks` (`id`, `class_id`, `teacher_id`, `semester_id`, `title`, `created`, `updated`) VALUES
(1, 1, 1, 4, 'Feedback for Intro to Rasters by Housni', 1415801434, 1415801434),
(2, 1, 2, 4, 'Feedback for Intro to Rasters by Ryan', 1415801434, 1415801434),
(3, 2, 2, 4, 'Feedback for Tracing Rasters by Ryan', 1415801434, 1415801434),
(4, 3, 1, 4, 'Feedback for Intro to Pottery by Housni', 1415801434, 1415801434),
(5, 4, 1, 4, 'Feedback for Photoshop 101 by Housni', 1415801434, 1415801434);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `program_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `short_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unit_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Slug of the title. Added automatically',
  `created` int(11) NOT NULL,
  `updated` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `program_id` (`program_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `program_id`, `title`, `short_name`, `unit_number`, `slug`, `created`, `updated`) VALUES
(1, 1, 'Design Principals', 'DP', 'U54', 'Design-Principals', 1415801434, 1415801434),
(2, 1, 'Interactive Media Design and Prototyping', 'IMDP', 'U24', 'Interactive-Media-Design-and-Prototyping', 1415801434, 1415801434),
(3, 2, 'Typographic Skills', 'TS', 'U32', 'Typographic-Skills', 1415801434, 1415801434),
(4, 2, 'Interactive Media Web Authoring', 'IMWA', 'U87', 'Interactive-Media-Web-Authoring', 1415801434, 1415801434),
(16, 3, 'Digital Media in Art and Design', 'DMAD', 'U11', 'Digital-Media-in-Art-and-Design', 1415831647, 1415831647),
(17, 3, 'Digital Image Creation and Development', 'DICD', 'U37', 'Digital-Image-Creation-and-Development', 1415831665, 1415831665),
(7, 1, 'Mass Text Applications', 'Mass Text', 'U27', 'Mass-Text-Applications', 1415802190, 1415802190),
(14, 4, 'Information and Interpretation in A&D', 'II in A&D', 'Unit 2', 'Information-and-Interpretation-in-A-D', 1415831308, 1415831308),
(15, 4, 'Extended Media Development in A&D', 'EMD', 'Unit 3', 'Extended-Media-Development-in-A-D', 1415831542, 1415831542),
(12, 1, 'Contextual and Cultural Referencing', 'CCR', 'U00', 'Contextual-and-Cultural-Referencing', 1415830647, 1415830647),
(13, 4, 'Researching, Recording and Responding in Art & Design', 'RRR', 'Unit 1', 'Researching-Recording-and-Responding-in-Art-Design', 1415831113, 1415831113);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `resource_id` int(11) NOT NULL,
  `allowed` tinyint(1) NOT NULL COMMENT '0 = denied; 1 = allowed',
  `notes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` int(11) NOT NULL,
  `updated` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  KEY `resource_id` (`resource_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `programs`
--

CREATE TABLE IF NOT EXISTS `programs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Slug of the title. Added automatically',
  `created` int(11) NOT NULL,
  `updated` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `programs`
--

INSERT INTO `programs` (`id`, `title`, `slug`, `created`, `updated`) VALUES
(1, 'HND Graphic Design', 'HND-Graphic-Design', 1415801434, 1415801434),
(2, 'HND Interactive Media', 'HND-Interactive-Media', 1415801434, 1415801434),
(3, 'HND Computing', 'HND-Computing', 1415801434, 1415801434),
(4, 'Foundation Diploma', 'Foundation-Diploma', 1415802137, 1415802137);

-- --------------------------------------------------------

--
-- Table structure for table `program_authorizations`
--

CREATE TABLE IF NOT EXISTS `program_authorizations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `program_id` int(11) NOT NULL,
  `program_leader_id` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `updated` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `program_id` (`program_id`),
  KEY `program_leader_id` (`program_leader_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `resources`
--

CREATE TABLE IF NOT EXISTS `resources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `controller` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `operation` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'A particular operation (eg: delete, verify, *, etc)',
  `notes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` int(11) NOT NULL,
  `updated` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `label` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `home` varchar(255) NOT NULL DEFAULT '/' COMMENT 'Home URL (relative) for this role',
  `status` int(5) NOT NULL DEFAULT '1' COMMENT '1 = enabled; 0 = disabled;',
  `rank` int(2) NOT NULL DEFAULT '100' COMMENT 'The ranking of this role. 1 is the highest rank and 100 is the lowest',
  `created` int(11) NOT NULL,
  `updated` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `title`, `label`, `description`, `home`, `status`, `rank`, `created`, `updated`) VALUES
(1, 'Administrator', 'admin', 'Administrator role', '/profile', 1, 1, 1415801434, 1415801434),
(2, 'Program Leader', 'prgLead', 'Program Leader role', '/profile', 1, 2, 1415801434, 1415801434),
(3, 'Teacher', 'teacher', 'Teacher role', '/profile', 1, 3, 1415801434, 1415801434),
(4, 'Academic Services', 'acdServ', 'Academic Services role', '/profile', 1, 2, 1415801434, 1415801434),
(5, 'External Verifier', 'extVer', 'External Verifier role', '/profile', 1, 2, 1415801434, 1415801434),
(6, 'Student Records', 'studRec', 'Student Records role', '/profile', 1, 2, 1415801434, 1415801434),
(7, 'Student', 'student', 'Student role', '/profile', 1, 4, 1415801434, 1415801434);

-- --------------------------------------------------------

--
-- Table structure for table `roles_users`
--

CREATE TABLE IF NOT EXISTS `roles_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `updated` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=88 ;

--
-- Dumping data for table `roles_users`
--

INSERT INTO `roles_users` (`id`, `role_id`, `user_id`, `created`, `updated`) VALUES
(1, 1, 1, 1415801434, 1415801434),
(2, 3, 1, 1415801434, 1415801434),
(3, 1, 2, 1415801434, 1415801434),
(4, 3, 2, 1415801434, 1415801434),
(5, 7, 3, 1415801444, 1415801444),
(6, 7, 4, 1415801444, 1415801444),
(7, 7, 5, 1415801445, 1415801445),
(8, 7, 6, 1415801445, 1415801445),
(9, 7, 7, 1415801445, 1415801445),
(10, 7, 8, 1415801445, 1415801445),
(11, 7, 9, 1415801445, 1415801445),
(12, 7, 10, 1415801445, 1415801445),
(13, 7, 11, 1415801445, 1415801445),
(14, 7, 12, 1415801445, 1415801445),
(15, 7, 13, 1415801446, 1415801446),
(16, 7, 14, 1415801446, 1415801446),
(17, 7, 15, 1415801446, 1415801446),
(18, 7, 16, 1415801446, 1415801446),
(19, 7, 17, 1415801446, 1415801446),
(20, 7, 18, 1415801446, 1415801446),
(21, 7, 19, 1415801446, 1415801446),
(22, 7, 20, 1415801446, 1415801446),
(23, 7, 21, 1415801446, 1415801446),
(24, 7, 22, 1415801447, 1415801447),
(25, 7, 23, 1415801447, 1415801447),
(26, 7, 24, 1415801447, 1415801447),
(27, 7, 25, 1415801447, 1415801447),
(28, 7, 26, 1415801447, 1415801447),
(29, 7, 27, 1415801447, 1415801447),
(30, 7, 28, 1415801447, 1415801447),
(31, 7, 29, 1415801447, 1415801447),
(32, 7, 30, 1415801447, 1415801447),
(33, 7, 31, 1415801448, 1415801448),
(34, 7, 32, 1415801448, 1415801448),
(35, 7, 33, 1415801448, 1415801448),
(36, 7, 34, 1415801448, 1415801448),
(37, 7, 35, 1415801448, 1415801448),
(38, 7, 36, 1415801448, 1415801448),
(39, 7, 37, 1415801448, 1415801448),
(40, 7, 38, 1415801448, 1415801448),
(41, 7, 39, 1415801448, 1415801448),
(42, 7, 40, 1415801449, 1415801449),
(43, 7, 41, 1415801449, 1415801449),
(44, 7, 42, 1415801449, 1415801449),
(45, 7, 43, 1415801449, 1415801449),
(46, 7, 44, 1415801449, 1415801449),
(47, 7, 45, 1415801449, 1415801449),
(48, 7, 46, 1415801449, 1415801449),
(49, 7, 47, 1415801449, 1415801449),
(50, 7, 48, 1415801450, 1415801450),
(51, 7, 49, 1415801450, 1415801450),
(52, 7, 50, 1415801450, 1415801450),
(53, 7, 51, 1415801450, 1415801450),
(54, 7, 52, 1415801450, 1415801450),
(55, 7, 53, 1415801450, 1415801450),
(56, 7, 54, 1415801450, 1415801450),
(57, 7, 55, 1415801450, 1415801450),
(58, 7, 56, 1415801450, 1415801450),
(59, 7, 57, 1415801451, 1415801451),
(60, 7, 58, 1415801451, 1415801451),
(61, 7, 59, 1415801451, 1415801451),
(62, 7, 60, 1415801451, 1415801451),
(63, 7, 61, 1415801451, 1415801451),
(64, 7, 62, 1415801451, 1415801451),
(65, 7, 63, 1415801451, 1415801451),
(66, 7, 64, 1415801451, 1415801451),
(67, 7, 65, 1415801452, 1415801452),
(68, 7, 66, 1415801452, 1415801452),
(69, 7, 67, 1415801452, 1415801452),
(70, 7, 68, 1415801452, 1415801452),
(71, 7, 69, 1415801452, 1415801452),
(72, 7, 70, 1415801452, 1415801452),
(73, 7, 71, 1415801452, 1415801452),
(74, 7, 72, 1415801452, 1415801452),
(75, 3, 73, 1415801980, 1415801980),
(76, 3, 74, 1415801980, 1415801980),
(77, 3, 75, 1415801980, 1415801980),
(78, 3, 76, 1415801980, 1415801980),
(79, 3, 77, 1415801980, 1415801980),
(80, 7, 78, 1415832616, 1415832616),
(81, 7, 79, 1415832616, 1415832616),
(82, 7, 80, 1415832616, 1415832616),
(83, 7, 81, 1415832617, 1415832617),
(84, 7, 82, 1415832617, 1415832617),
(85, 7, 83, 1415832617, 1415832617),
(86, 7, 84, 1415832617, 1415832617),
(87, 7, 85, 1415832845, 1415832845);

-- --------------------------------------------------------

--
-- Table structure for table `semesters`
--

CREATE TABLE IF NOT EXISTS `semesters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Slug of the title. Added automatically',
  `year` int(4) NOT NULL COMMENT 'The year for the semester',
  `semester` int(1) NOT NULL COMMENT 'The semester count where: `semester` > 0 && `semester` < 5',
  `begins` int(11) NOT NULL,
  `ends` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `updated` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `semesters`
--

INSERT INTO `semesters` (`id`, `title`, `slug`, `year`, `semester`, `begins`, `ends`, `created`, `updated`) VALUES
(1, '1404', '1404', 0, 0, 1412028000, 1422658800, 1415802591, 1415802591);

-- --------------------------------------------------------

--
-- Table structure for table `submissions`
--

CREATE TABLE IF NOT EXISTS `submissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assignment_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Name of the uploaded file',
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Path to the uploaded file',
  `receipt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Auto generated code for verification',
  `created` int(11) NOT NULL,
  `updated` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`),
  KEY `assignment_id` (`assignment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sylos`
--

CREATE TABLE IF NOT EXISTS `sylos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` int(11) NOT NULL,
  `updated` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `teacher_authorizations`
--

CREATE TABLE IF NOT EXISTS `teacher_authorizations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `semester_id` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `updated` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `class_id` (`class_id`),
  KEY `teacher_id` (`teacher_id`),
  KEY `semester_id` (`semester_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Dumping data for table `teacher_authorizations`
--

INSERT INTO `teacher_authorizations` (`id`, `class_id`, `teacher_id`, `semester_id`, `created`, `updated`) VALUES
(9, 11, 2, 5, 1415830874, 1415830874),
(8, 10, 77, 5, 1415830781, 1415830781),
(7, 9, 77, 5, 1415830741, 1415830741),
(5, 7, 73, 5, 1415830512, 1415830512),
(10, 12, 74, 5, 1415831426, 1415831426),
(11, 13, 73, 5, 1415831474, 1415831474),
(12, 14, 2, 5, 1415831581, 1415831581),
(13, 15, 75, 5, 1415831739, 1415831739),
(14, 16, 75, 5, 1415831771, 1415831771);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Slug of the `first_name` and `last_name`. Added automatically',
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(5) NOT NULL DEFAULT '1' COMMENT '1 = enabled; 0 = disabled;',
  `timezone` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Europe/Budapest',
  `created` int(11) NOT NULL,
  `updated` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=86 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `slug`, `password`, `salt`, `email`, `status`, `timezone`, `created`, `updated`) VALUES
(1, 'Housni', 'Yakoob', 'Housni-Yakoob', '$2a$10$4WWCmKSdUA5UaARFUDMQJezVjL2vR6D/aaGJhMkVWm2SZOL250Rl.', '$2a$10$4WWCmKSdUA5UaARFUDMQJe', 'housni@koobi.co', 1, 'Asia/Colombo', 0, 0),
(2, 'Ryan', 'Cole', 'Ryan-Cole', '$2a$10$cFqwoS5kkDcFtuIRIt1bner9oqkSn88Rb25LMQp572ozE4rZe7i8q', '$2a$10$cFqwoS5kkDcFtuIRIt1bne', 'ryan@koobi.co', 1, 'Europe/Budapest', 0, 0),
(3, 'Elena', 'Firstova', 'Elena-Firstova', '$2a$10$.x1Zn7LsynDsp/12Umb1Ie9OKBva43iG8QTMAaDTjM8yi8mmHbvCy', '$2a$10$.x1Zn7LsynDsp/12Umb1Ie', 'elena.firstova@praguecollege.cz', 1, 'Europe/Budapest', 1415801444, 1415801444),
(4, 'Marcel', 'Lacko', 'Marcel-Lacko', '$2a$10$//Y9acNhSJQ76OmQO/CXYeNY4ErLHM9VWM0/KsDz.Wd8xRdBdUeFG', '$2a$10$//Y9acNhSJQ76OmQO/CXYe', 'marcel.lacko@praguecollege.cz', 1, 'Europe/Budapest', 1415801444, 1415801444),
(5, 'Belinda', 'O''Connell', 'Belinda-O-Connell', '$2a$10$dm4lRuT2M19H9QvXIBadzeete3ogYSZct4nAFoahYiRDdJr89akEi', '$2a$10$dm4lRuT2M19H9QvXIBadze', 'belinda.oconnell@praguecollege.cz', 1, 'Europe/Budapest', 1415801445, 1415801445),
(6, 'Roman', 'Jang', 'Roman-Jang', '$2a$10$KY1E8xHpRHeyEhq7DpPGT.zi/FDAGencWUgV04b2rh4JIBjQ5955y', '$2a$10$KY1E8xHpRHeyEhq7DpPGT.', 'roman.jang@praguecollege.cz', 1, 'Europe/Budapest', 1415801445, 1415801445),
(7, 'Jana', 'Krchova', 'Jana-Krchova', '$2a$10$.2PQxsn4SUQKtIX1d2TFLu.nb.wx9gAtTQNVFjJtAHE.wksdq0B5m', '$2a$10$.2PQxsn4SUQKtIX1d2TFLu', 'jana.krchova@praguecollege.cz', 1, 'Europe/Budapest', 1415801445, 1415801445),
(8, 'Sara', 'Vedeskog', 'Sara-Vedeskog', '$2a$10$LePt5mEErL6ArX/sErAzme/RUODPDqpp/EXXlcXtqpnEf37FgRJHq', '$2a$10$LePt5mEErL6ArX/sErAzme', 'sara.vedeskog@praguecollege.cz', 1, 'Europe/Budapest', 1415801445, 1415801445),
(9, 'Yuriy', 'Shiryaev', 'Yuriy-Shiryaev', '$2a$10$c38YebPhCX.nfnRCnwsTX.afK/s.livH2iJypfvdIdf3W6FyUGV8O', '$2a$10$c38YebPhCX.nfnRCnwsTX.', 'yuriy.shiryaev@praguecollege.cz', 1, 'Europe/Budapest', 1415801445, 1415801445),
(10, 'Pamela', 'Hickey', 'Pamela-Hickey', '$2a$10$1q9faJldzjie2/FmStXSg.M.sl7IwiQQ4spOq3wcuVLIO0zC2TyI2', '$2a$10$1q9faJldzjie2/FmStXSg.', 'pamela.hickey@praguecollege.cz', 1, 'Europe/Budapest', 1415801445, 1415801445),
(11, 'Sofya', 'Shamikh', 'Sofya-Shamikh', '$2a$10$YJctgEiypRjBYqYj/z/BHu2z1KyYdYE.lERoxNBKEwdHy6eDpKrpS', '$2a$10$YJctgEiypRjBYqYj/z/BHu', 'sofya.shamikh@praguecollege.cz', 1, 'Europe/Budapest', 1415801445, 1415801445),
(12, 'Ainura', 'Karimzhanova', 'Ainura-Karimzhanova', '$2a$10$v4OwqphXQXY2x4G2rhZbjOnO79WGXS.vt79aLWnTOBMZDq/2zu/u6', '$2a$10$v4OwqphXQXY2x4G2rhZbjO', 'ainura.karimzhanova@praguecollege.cz', 1, 'Europe/Budapest', 1415801445, 1415801445),
(13, 'Quynh Anh', ' Pham', 'Quynh-Anh-Pham', '$2a$10$v6c.HszmtrXyJW5sieT/i.p7FWtqg9wQBbnHYNt8JBzdGgGcDZ6y6', '$2a$10$v6c.HszmtrXyJW5sieT/i.', 'anhquynh.anh@praguecollege.cz', 1, 'Europe/Budapest', 1415801445, 1415801445),
(14, 'Ravi', 'Satpute', 'Ravi-Satpute', '$2a$10$xrlcWD3oOu8m0cpf/8wn.OWQNysDGjjPMFeFyLEiHlfmJmo9kRyFu', '$2a$10$xrlcWD3oOu8m0cpf/8wn.O', 'ravi.satpute@praguecollege.cz', 1, 'Europe/Budapest', 1415801446, 1415801446),
(15, 'Hubert', 'Gaca', 'Hubert-Gaca', '$2a$10$UvgFHug1YkKcw4BUx.uKYuiDiKdYFQbiFV3.zmJ83K3JJaxFV8lXK', '$2a$10$UvgFHug1YkKcw4BUx.uKYu', 'hubert.gaca@praguecollege.cz', 1, 'Europe/Budapest', 1415801446, 1415801446),
(16, 'Kieu', 'Nguyen', 'Kieu-Nguyen', '$2a$10$SrEZ.2LKVRfyOujmMfxbRuRh1Et7quYRi6FPNz8ngwNYK6.3vaJty', '$2a$10$SrEZ.2LKVRfyOujmMfxbRu', 'kieunguyen.son@praguecollege.cz', 1, 'Europe/Budapest', 1415801446, 1415801446),
(17, 'Ieva', 'Ozola', 'Ieva-Ozola', '$2a$10$u1ltkXpcBe.pWlFKTlDjN.FQdh8G4864x.Wy02Xu83gr6gtjeCE3a', '$2a$10$u1ltkXpcBe.pWlFKTlDjN.', 'ieva.ozola@praguecollege.cz', 1, 'Europe/Budapest', 1415801446, 1415801446),
(18, 'Elizaveta', 'Kolupaeva', 'Elizaveta-Kolupaeva', '$2a$10$RgUM0uIDxwgB3IlX8BcY0./1wmURfABOZ/2DyfBBf1HH.XB.fj102', '$2a$10$RgUM0uIDxwgB3IlX8BcY0.', 'ms.julia.mail@gmail.com', 1, 'Europe/Budapest', 1415801446, 1415801446),
(19, 'Fyodor', 'Rashevskiy', 'Fyodor-Rashevskiy', '$2a$10$XLlDkidK9FuuUL28aCb1z.xuCeFzJCDhBj6.aRLfZuWftLYCVUpC.', '$2a$10$XLlDkidK9FuuUL28aCb1z.', 'fyodor.rashevskiy@praguecollege.cz', 1, 'Europe/Budapest', 1415801446, 1415801446),
(20, 'Alice', 'Knirsova', 'Alice-Knirsova', '$2a$10$6jRjOj/oQBf4pcMASilG8uvg4aDaGQTb9lDwlB.25gQtt3BMJbCEq', '$2a$10$6jRjOj/oQBf4pcMASilG8u', 'alice.knirsova@praguecollege.cz', 1, 'Europe/Budapest', 1415801446, 1415801446),
(21, 'Andrey', 'Rashevskiy', 'Andrey-Rashevskiy', '$2a$10$Be/Vn/d/jsrskGOkt5.l7e5chiYTuSDYyJvLoKaPd8DCmgY.9QhIq', '$2a$10$Be/Vn/d/jsrskGOkt5.l7e', 'andrey.rashevskiy@praguecollege.cz', 1, 'Europe/Budapest', 1415801446, 1415801446),
(22, 'Oleksandra', 'Bokancha', 'Oleksandra-Bokancha', '$2a$10$y6dBYFxnyKzuK3PNzHFqz.TPgNPJIinvLCQdrMeq5E8Tfeu.ePdxS', '$2a$10$y6dBYFxnyKzuK3PNzHFqz.', 'oleksandra.bokancha@praguecollege.cz', 1, 'Europe/Budapest', 1415801446, 1415801446),
(23, 'Marianna', 'Hudakova', 'Marianna-Hudakova', '$2a$10$0aD/DT/cLmJkdwFMY5czPOw2xKSS78L8RplJE0woLYPDn02MQhhae', '$2a$10$0aD/DT/cLmJkdwFMY5czPO', 'marianna.hudakova@praguecollege.cz', 1, 'Europe/Budapest', 1415801447, 1415801447),
(24, 'Artur', 'Mnatsakanyan', 'Artur-Mnatsakanyan', '$2a$10$Eh7NfXTlYr63YGpgjmdeb.1OhTsZI.8.50p6vabwlYJkvAXdIHAm2', '$2a$10$Eh7NfXTlYr63YGpgjmdeb.', 'artur.mnatsakanyan@praguecollege.cz', 1, 'Europe/Budapest', 1415801447, 1415801447),
(25, 'Zuleykha', 'Abbasova', 'Zuleykha-Abbasova', '$2a$10$L6p0f7e5R8wQUCZTM4VxYu6xFUT4DSfQcX/TEIfn8/p4okr0V2NRu', '$2a$10$L6p0f7e5R8wQUCZTM4VxYu', 'zuleykha.abbasova@praguecollege.cz', 1, 'Europe/Budapest', 1415801447, 1415801447),
(26, 'Zofia', 'Ziakova', 'Zofia-Ziakova', '$2a$10$aLq.f0gLbyLR64YdfJoET.sB1ZomktK.IMyAKQWjEE3ChEQj7INaW', '$2a$10$aLq.f0gLbyLR64YdfJoET.', 'zofia.ziakova@praguecollege.cz', 1, 'Europe/Budapest', 1415801447, 1415801447),
(27, 'Veronika', 'Vrbova', 'Veronika-Vrbova', '$2a$10$j.NkyXHXpkHtTIkyQVlmxur86VTFrKBXh6x1sDqEKstfbjdhYtHvW', '$2a$10$j.NkyXHXpkHtTIkyQVlmxu', 'veronika.vrbova@praguecollege.cz', 1, 'Europe/Budapest', 1415801447, 1415801447),
(28, 'Ebenezer', 'Animah', 'Ebenezer-Animah', '$2a$10$aJlAAmcPWj1SBko/Yq7ECudJnhpv/hzYM3QzZEs4DFJdi/luMxnTO', '$2a$10$aJlAAmcPWj1SBko/Yq7ECu', 'ebenezer.animah@praguecollege.cz', 1, 'Europe/Budapest', 1415801447, 1415801447),
(29, 'Dora', 'Ivanova', 'Dora-Ivanova', '$2a$10$VrBNvLjOY/BS23lIAL66xuyEvSkdpnpqUbfYmdC8KNWL5qG551CoW', '$2a$10$VrBNvLjOY/BS23lIAL66xu', 'dora.ivanova@praguecollege.cz', 1, 'Europe/Budapest', 1415801447, 1415801447),
(30, 'Matej', 'Vajner', 'Matej-Vajner', '$2a$10$.9BpZn22cRhSn6thvsWZg.dAAAmwoHmHdbreZi06biZ0znILC802C', '$2a$10$.9BpZn22cRhSn6thvsWZg.', 'matej.vajner@praguecollege.cz', 1, 'Europe/Budapest', 1415801447, 1415801447),
(31, 'Jan', 'Docekal', 'Jan-Docekal', '$2a$10$eVco4pQLIlCKBf6dNK05UOqTRv40ZMfBtw4ZOf9eoFMTkiyqSknB.', '$2a$10$eVco4pQLIlCKBf6dNK05UO', 'jan.docekal@praguecollege.cz', 1, 'Europe/Budapest', 1415801447, 1415801447),
(32, 'Michal', 'Marianek', 'Michal-Marianek', '$2a$10$ZUwuxUiuzCI3nWw/9GREZ.BAmKlxxMwiNE1AgBImkjQZzA2d8nZka', '$2a$10$ZUwuxUiuzCI3nWw/9GREZ.', 'michal.m@praguecollege.cz', 1, 'Europe/Budapest', 1415801448, 1415801448),
(33, 'Huyen Thanh', 'Nguyen', 'Huyen-Thanh-Nguyen', '$2a$10$1t4T5goRRZM0juwRq6z4/ewf1RL.iOEZchVCuvKUjKuRG57TzNYUe', '$2a$10$1t4T5goRRZM0juwRq6z4/e', 'julie_2610@yahoo.com', 1, 'Europe/Budapest', 1415801448, 1415801448),
(34, 'Martin', 'Platil', 'Martin-Platil', '$2a$10$f9cf2YwO6AdrWMMlsNvXXO6wwbZO5S5XX618LlpOKB6rs3nBmxZja', '$2a$10$f9cf2YwO6AdrWMMlsNvXXO', 'martin.platil@praguecollege.cz', 1, 'Europe/Budapest', 1415801448, 1415801448),
(35, 'Daniel', 'Saloun', 'Daniel-Saloun', '$2a$10$q5sfXVH87/KKxkhoMVr0gOAjSBSZJy73NuSqmbl7I7XXw8GUaMqFS', '$2a$10$q5sfXVH87/KKxkhoMVr0gO', 'daniel.saloun@praguecollege.cz', 1, 'Europe/Budapest', 1415801448, 1415801448),
(36, 'Evdokia', 'Zamakhina', 'Evdokia-Zamakhina', '$2a$10$/5IdFDe04Ebb2b5kh4wKSuN5.RAia77H5Cb.0m49.nZikb7m3t3SK', '$2a$10$/5IdFDe04Ebb2b5kh4wKSu', 'evdokia.zamakhina@praguecollege.cz', 1, 'Europe/Budapest', 1415801448, 1415801448),
(37, 'Rostislav', 'Jirkal', 'Rostislav-Jirkal', '$2a$10$Urs9qAECkludjJmEdEWIpepLwicbCkEBchwCrDaHh5L/jRR4t85YG', '$2a$10$Urs9qAECkludjJmEdEWIpe', 'rostislav.jirkal@praguecollege.cz', 1, 'Europe/Budapest', 1415801448, 1415801448),
(38, 'Dominik', 'Gavor', 'Dominik-Gavor', '$2a$10$xR9uPUbLE01Y1B/hiV.n1Oyqi.6QRIBi4..ll9A1Ftt7Q9Gh/Nom.', '$2a$10$xR9uPUbLE01Y1B/hiV.n1O', 'dominik.gavor@praguecollege.cz', 1, 'Europe/Budapest', 1415801448, 1415801448),
(39, 'Anton', 'Stupnev', 'Anton-Stupnev', '$2a$10$oFnHrFnBW9Mll6KY3ttNXeggEpwkdLOIMLQWKSBCANTZHWp48PWCK', '$2a$10$oFnHrFnBW9Mll6KY3ttNXe', 'anton.stupnev@praguecollege.cz', 1, 'Europe/Budapest', 1415801448, 1415801448),
(40, 'Petr', 'Skovajsa', 'Petr-Skovajsa', '$2a$10$LaQR5oq2aI2TTd39vnNuwO6Ynh5zs.vehm5S1OzhCp5C4GkcJWBIu', '$2a$10$LaQR5oq2aI2TTd39vnNuwO', 'petr.skovajsa@praguecollege.cz', 1, 'Europe/Budapest', 1415801449, 1415801449),
(41, 'Linda', 'Zbirkova', 'Linda-Zbirkova', '$2a$10$FVVtPMbcwJu1CiPQ074m5OIQWK.ciCiVGmAgynoaIjQOGPnhSYasi', '$2a$10$FVVtPMbcwJu1CiPQ074m5O', 'linda.zbirkova@praguecollege.cz', 1, 'Europe/Budapest', 1415801449, 1415801449),
(42, 'Leyla', 'Gabbasova', 'Leyla-Gabbasova', '$2a$10$FP/Ay6ks4e33oYjmw7JQXuo1lF.tVTKRQ/E5vnZCBURqgmAjSI5BC', '$2a$10$FP/Ay6ks4e33oYjmw7JQXu', 'leyla.gabbasova@praguecollege.cz', 1, 'Europe/Budapest', 1415801449, 1415801449),
(43, 'Soojeong', 'Hyun', 'Soojeong-Hyun', '$2a$10$VAyJ/i8bQDAd8ntgOf5jG.hP6zNvztwdLkFc00/og3ne6.0FXzpyy', '$2a$10$VAyJ/i8bQDAd8ntgOf5jG.', 'soojeong.hyun@praguecollege.cz', 1, 'Europe/Budapest', 1415801449, 1415801449),
(44, 'CiOn', 'Kim', 'Ci-On-Kim', '$2a$10$JA.ZlpEOe/RVzt43skpWsezcnlUjFFITefUkR4vll6e1MghkJ7u5.', '$2a$10$JA.ZlpEOe/RVzt43skpWse', 'cion.kim@praguecollege.cz', 1, 'Europe/Budapest', 1415801449, 1415801449),
(45, 'Patricia', 'Potancokova', 'Patricia-Potancokova', '$2a$10$smv/NE5jKaO427Iu5.rNaO2tCLk3iAHT53xiAIb6xTiNAsoByUkOy', '$2a$10$smv/NE5jKaO427Iu5.rNaO', 'patricia.potancokova@praguecollege.cz', 1, 'Europe/Budapest', 1415801449, 1415801449),
(46, 'Almira', 'Yausheva', 'Almira-Yausheva', '$2a$10$wQB/wfjs8EvWMWcinFp3Y.HY6tJqnEbpEPX1V53/4KaNEIkERQYca', '$2a$10$wQB/wfjs8EvWMWcinFp3Y.', 'almira.yausheva@praguecollege.cz', 1, 'Europe/Budapest', 1415801449, 1415801449),
(47, 'Iva', 'Borisova', 'Iva-Borisova', '$2a$10$ZyPNARfbreAWPbtFcdUQ4ub38KLrsmVxoIVjkHAIISWueK/UswLBK', '$2a$10$ZyPNARfbreAWPbtFcdUQ4u', 'iva.borisova@praguecollege.cz', 1, 'Europe/Budapest', 1415801449, 1415801449),
(48, 'Sandra', 'Abdulhakova', 'Sandra-Abdulhakova', '$2a$10$D5jEW9kqPBurrQttGqFmuOLo9YoRd9SEdEY.NYCsJw/iMR7anumm.', '$2a$10$D5jEW9kqPBurrQttGqFmuO', 'sandra.abdulhakova@praguecollege.cz', 1, 'Europe/Budapest', 1415801449, 1415801449),
(49, 'Maria', 'Jonasson', 'Maria-Jonasson', '$2a$10$SfzOj7Ok2plHOdM4oBL8ru7Su5xAZ2v.LfKwKDiFH9WE/vrj.FHXG', '$2a$10$SfzOj7Ok2plHOdM4oBL8ru', 'maria.jonasson@praguecollege.cz', 1, 'Europe/Budapest', 1415801450, 1415801450),
(50, 'Gangaanai', 'Ganbaatar', 'Gangaanai-Ganbaatar', '$2a$10$cRpnnsXWp5tayikfiF3fB.Z0i7aPJ9YqIFzwN1F0UaPLEy6/KxXZe', '$2a$10$cRpnnsXWp5tayikfiF3fB.', 'gangaanai.ganbaatar@praguecollege.cz', 1, 'Europe/Budapest', 1415801450, 1415801450),
(51, 'Jakub', 'Idziak', 'Jakub-Idziak', '$2a$10$llHf48N/B6Yj7fP4cFSi5uyn7v2fn43fHoO9KJzu8ukFLK6QnL7Iu', '$2a$10$llHf48N/B6Yj7fP4cFSi5u', 'jakub.idziak@praguecollege.cz', 1, 'Europe/Budapest', 1415801450, 1415801450),
(52, 'Hamza', 'Benyahia', 'Hamza-Benyahia', '$2a$10$/MCvzWcUJkY0y.HxAkyHbONBtOKFTAY6tOSKbUTf2ayQuQt0lAUiu', '$2a$10$/MCvzWcUJkY0y.HxAkyHbO', 'hamza.benyahia@praguecollege.cz', 1, 'Europe/Budapest', 1415801450, 1415801450),
(53, 'Irena', 'Svobodova', 'Irena-Svobodova', '$2a$10$OHLcQsCKXq3OIUFpImrjg.hEqSfwlTbuNfG/7aENCopce9bLvPeRm', '$2a$10$OHLcQsCKXq3OIUFpImrjg.', 'irena.svobodova@praguecollege.cz', 1, 'Europe/Budapest', 1415801450, 1415801450),
(54, 'Veronica', 'Dasa', 'Veronica-Dasa', '$2a$10$UEJFVoqxkhbrInJ1aIcsWelX6BeEPh6V/Prk1JT7VKtVQLUZjJloy', '$2a$10$UEJFVoqxkhbrInJ1aIcsWe', 'veronica.dasa@praguecollege.cz', 1, 'Europe/Budapest', 1415801450, 1415801450),
(55, 'Vladimir', 'Martynov', 'Vladimir-Martynov', '$2a$10$4GYB97qmIzsTf3.u2lKcvedSQJcAM8FXkH6em7VPzOLEZPeQY/mcS', '$2a$10$4GYB97qmIzsTf3.u2lKcve', 'vladimir.martynov@praguecollege.cz', 1, 'Europe/Budapest', 1415801450, 1415801450),
(56, 'Basel', 'Mohab', 'Basel-Mohab', '$2a$10$c0PrqAB8hTLQnAXCyUXWr.BDlsFF52MRTrgQZOJNxTHRHHFf4KMcy', '$2a$10$c0PrqAB8hTLQnAXCyUXWr.', 'basel.mohab@praguecollege.cz', 1, 'Europe/Budapest', 1415801450, 1415801450),
(57, 'Elias', 'Bauer', 'Elias-Bauer', '$2a$10$BpaKtKslOvncmlZBzpyJyOUAgRF.YFsjaDZr/G4cWRCKQnOW.DxOK', '$2a$10$BpaKtKslOvncmlZBzpyJyO', 'elias.bauer@praguecollege.cz', 1, 'Europe/Budapest', 1415801450, 1415801450),
(58, 'Emilio', 'Herrera', 'Emilio-Herrera', '$2a$10$fZK2LJT0K1fepV4iKwbK7OSSk4DhBCC0qskaYo8JzOwhr/AAQkHBS', '$2a$10$fZK2LJT0K1fepV4iKwbK7O', 'emilio.herrera@praguecollege.cz', 1, 'Europe/Budapest', 1415801451, 1415801451),
(59, 'Milos', 'Cekal', 'Milos-Cekal', '$2a$10$gAcZxdrvjF8f0/vB6rQ9EOOz8wBl2CRGfDvFA8VF64QrpAsRJi0V.', '$2a$10$gAcZxdrvjF8f0/vB6rQ9EO', 'milos.cekal@praguecollege.cz', 1, 'Europe/Budapest', 1415801451, 1415801451),
(60, 'Marek', 'Vodvarka', 'Marek-Vodvarka', '$2a$10$RLOMC1XR/3ks8T3NjyHh/OmKTtGJADezR9b/wL10xs.GDCA6sxx2G', '$2a$10$RLOMC1XR/3ks8T3NjyHh/O', 'marek.vodvarka@praguecollege.cz', 1, 'Europe/Budapest', 1415801451, 1415801451),
(61, 'Lukas', 'Lohnicky', 'Lukas-Lohnicky', '$2a$10$sPDoGQQNfOjFetrQt8k9ue/0eqRVozTbCNO4r3tNziHDm94w9NPkK', '$2a$10$sPDoGQQNfOjFetrQt8k9ue', 'lukas.lohnicky@praguecollege.cz', 1, 'Europe/Budapest', 1415801451, 1415801451),
(62, 'Anton', 'Kondrashov', 'Anton-Kondrashov', '$2a$10$Ot6q7/BNi3Pg0TJ6pO7xTeP1aDcF7wOUQb1r0j.TnZY.tBNF7ixhK', '$2a$10$Ot6q7/BNi3Pg0TJ6pO7xTe', 'anton.kondrashov@praguecollege.cz', 1, 'Europe/Budapest', 1415801451, 1415801451),
(63, 'Minh Anh', 'Nguyen', 'Minh-Anh-Nguyen', '$2a$10$wk0zKZqfZX8k9cIbrz3dKOWTDZiOXquntmsF72VPo9vxVZbxh7oc6', '$2a$10$wk0zKZqfZX8k9cIbrz3dKO', 'minhanh.nguyen@praguecollege.cz', 1, 'Europe/Budapest', 1415801451, 1415801451),
(64, 'Adrian', 'Devder', 'Adrian-Devder', '$2a$10$qbHsK2Mz8pKmSms6wjPf0ORey0ZF7lbJBr0Nw6SMCud3mE963vE5a', '$2a$10$qbHsK2Mz8pKmSms6wjPf0O', 'adrian.devder@praguecollege.cz', 1, 'Europe/Budapest', 1415801451, 1415801451),
(65, 'George', 'Marcuk', 'George-Marcuk', '$2a$10$g.bpcSZBamcIYLlsVE.8seuOcrdDcoj6V0R1GPhtsn6bWCD34IPRi', '$2a$10$g.bpcSZBamcIYLlsVE.8se', 'george.marcuk@praguecollege.cz', 1, 'Europe/Budapest', 1415801451, 1415801451),
(66, 'Edoardo', 'Puiatti', 'Edoardo-Puiatti', '$2a$10$7hjh0AQceL1X2wHjs/.laOZbveW3EnJoLO6oJGzRWccQJaYmOTUx6', '$2a$10$7hjh0AQceL1X2wHjs/.laO', 'edoardo.puiatti@praguecollege.cz', 1, 'Europe/Budapest', 1415801452, 1415801452),
(67, 'Phichit', 'Mekkluan', 'Phichit-Mekkluan', '$2a$10$NdojzHn7PEXX3T/VvxjjseMHRpsmZLYmkbBG3XqqmXJUciuRY235i', '$2a$10$NdojzHn7PEXX3T/Vvxjjse', 'phichit.mekkluan@praguecollege.cz', 1, 'Europe/Budapest', 1415801452, 1415801452),
(68, 'Kevin', 'Van Der Poll', 'Kevin-Van-Der-Poll', '$2a$10$CU9alA3r8zVCBRrE9lmyeOlIshLcD46g1/1Tlqm8ZiwE9EFLmyQu2', '$2a$10$CU9alA3r8zVCBRrE9lmyeO', 'kevin.vanderpoll@praguecollege.cz', 1, 'Europe/Budapest', 1415801452, 1415801452),
(69, 'Pavel', 'Kaseljak', 'Pavel-Kaseljak', '$2a$10$U6D.dkM07TjnfsoRQ52ziuFKVNRF.Tjrn9pmXniO6v.vNTtQuCs92', '$2a$10$U6D.dkM07TjnfsoRQ52ziu', 'pavel.kaseljak@praguecollege.cz', 1, 'Europe/Budapest', 1415801452, 1415801452),
(70, 'Jan', 'Bartonicek', 'Jan-Bartonicek', '$2a$10$SKHEhDHb9LYBMWaf5pnaH./.KYMd.DfIRRx5fS8suvEnvUDYmjb5i', '$2a$10$SKHEhDHb9LYBMWaf5pnaH.', 'jan.bartonicek@praguecollege.cz', 1, 'Europe/Budapest', 1415801452, 1415801452),
(71, 'Nikoletta', 'Minarova', 'Nikoletta-Minarova', '$2a$10$oH2me0dT6DCOzHDYveHqSuifLau9crDbwFuecs7jFPGNrs8g6JY9q', '$2a$10$oH2me0dT6DCOzHDYveHqSu', 'nikoletta.minarova@praguecollege.cz', 1, 'Europe/Budapest', 1415801452, 1415801452),
(72, 'Anita', 'Obdrzalkova', 'Anita-Obdrzalkova', '$2a$10$DMITaLHQLrCAzPbzp/GgTOkpyaQ5EoK1OmcOP2t9erN3YPtwJrFO.', '$2a$10$DMITaLHQLrCAzPbzp/GgTO', 'anita.obdrzalkova@praguecollege.cz', 1, 'Europe/Budapest', 1415801452, 1415801452),
(73, 'Sean', 'McAlorum', 'Sean-Mc-Alorum', '$2a$10$o3xopJWRRXNp/KUUsSXwguPAytw0UPkiYFyP6qCHyW12JlxkfUySO', '$2a$10$o3xopJWRRXNp/KUUsSXwgu', 'sean.m@praguecollege.cz', 1, 'Europe/Budapest', 1415801979, 1415801979),
(74, 'Kelly', 'Hoben', 'Kelly-Hoben', '$2a$10$uvD5sk.kCycxfF3ZbLBHMeAOJMZGkhOYRpC9reo7CaOiH.c4J5ku.', '$2a$10$uvD5sk.kCycxfF3ZbLBHMe', 'kelly.h@praguecollege.cz', 1, 'Europe/Budapest', 1415801980, 1415801980),
(75, 'Bohus', 'Ziskal', 'Bohus-Ziskal', '$2a$10$5.qV6Ts6F5i/sy91x8HlV.5smcd7lmh6Ra7vS8LRNtcXbb3AyHqK.', '$2a$10$5.qV6Ts6F5i/sy91x8HlV.', 'bohus.z@praguecollege.cz', 1, 'Europe/Budapest', 1415801980, 1415801980),
(76, 'Simon', 'Gray', 'Simon-Gray', '$2a$10$h78j8t65Rd2AdHvlaIRORO8K4JL7J3hV7.NWJKQkwVvxkc5ayf.vq', '$2a$10$h78j8t65Rd2AdHvlaIRORO', 'simon.g@praguecollege.cz', 1, 'Europe/Budapest', 1415801980, 1415801980),
(77, 'Paul', 'DeLave', 'Paul-De-Lave', '$2a$10$095bVtHGAFD7AXvkEPUSLuDumB6ZZuwxNhYB9q2mSWWdSNazaS5mG', '$2a$10$095bVtHGAFD7AXvkEPUSLu', 'paul.d@praguecollege.cz', 1, 'Europe/Budapest', 1415801980, 1415801980),
(78, 'Johanna', 'Loof', 'Johanna-Loof', '$2a$10$6.9tBxEwGvo9kvVtqFkEqesMHsOSae7T3.qcJj.iQMsOZcxj/wGO.', '$2a$10$6.9tBxEwGvo9kvVtqFkEqe', 'johanna.loof@praguecollege.cz', 1, 'Europe/Budapest', 1415832616, 1415832616),
(79, 'Katerina', 'Kodytkova', 'Katerina-Kodytkova', '$2a$10$NLTROPfUnzFx/KFPyhl5P.U2MTtw/oX3liVV2Ump0j8Lw2FFvSH6C', '$2a$10$NLTROPfUnzFx/KFPyhl5P.', 'katerina.kodytkova@praguecollege.cz', 1, 'Europe/Budapest', 1415832616, 1415832616),
(80, 'Anastasiia', 'Masko', 'Anastasiia-Masko', '$2a$10$FmV0qxc2WVJeRxSllby7auO6nuF.NVoJwOP097hftbN0xbLlNLIJ.', '$2a$10$FmV0qxc2WVJeRxSllby7au', 'anastasiia.masko@praguecollege.cz', 1, 'Europe/Budapest', 1415832616, 1415832616),
(81, 'Kirill', 'Nosov', 'Kirill-Nosov', '$2a$10$MFrhuH5E1Q9Jj.HHwPCJm.iEag1dAxq33ZoQDKxLaIE4CHtBnM8ny', '$2a$10$MFrhuH5E1Q9Jj.HHwPCJm.', 'kirill.nosov@praguecollege.cz', 1, 'Europe/Budapest', 1415832617, 1415832617),
(82, 'Kirill', 'Diakonov', 'Kirill-Diakonov', '$2a$10$ZAhC0MrSWm65Bg0tTuxRM.QFxkwzD/dkcOBy.DzJm2Whp7yoKpEdW', '$2a$10$ZAhC0MrSWm65Bg0tTuxRM.', 'kirill.diakonov@praguecollege.cz', 1, 'Europe/Budapest', 1415832617, 1415832617),
(83, 'Lin', 'Mingji', 'Lin-Mingji', '$2a$10$Q7goht0Wd6cCOneTybE/peGb97BjdGzQe3S2n1KNa0vNwe9KfyeLe', '$2a$10$Q7goht0Wd6cCOneTybE/pe', 'lin.mingji@praguecollege.cz', 1, 'Europe/Budapest', 1415832617, 1415832617),
(84, 'Nikita', 'Alekhin', 'Nikita-Alekhin', '$2a$10$UHliKiIlAW0beRB9nzjrbe15UCxg3R8xUItMCiG5UMEmHMrhGqZXS', '$2a$10$UHliKiIlAW0beRB9nzjrbe', 'nikita.alekhin@praguecollege.cz', 1, 'Europe/Budapest', 1415832617, 1415832617),
(85, 'Kira', 'Motorina', 'Kira-Motorina', '$2a$10$QDJltMnAyzkYry4l2/JdDuAaFbFMdfinNO.Zu78va3WrXKz90LLRu', '$2a$10$QDJltMnAyzkYry4l2/JdDu', 'kira.motorina@praguecollege.cz', 1, 'Europe/Budapest', 1415832845, 1415832845);

-- --------------------------------------------------------



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
