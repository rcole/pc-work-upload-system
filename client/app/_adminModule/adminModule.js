// adminModule.js Starts here 

var adminApp = angular.module('adminApp' , [
  'globals',
  'semesterProgramModule',
  'semesterProgramClassModule',
  'semestersModule',
  'programsModule',
  'extcodesModule',
  'myclassesModule',
  'usersModule',
  'modulesModule',
  'flow',
  'MessageCenterModule',
  'uservoiceModule',
  'analyticsModule',
  'submissionsListModule'
]);

adminApp.config(['$stateProvider','$urlRouterProvider','$locationProvider',function($stateProvider, $urlRouterProvider, $locationProvider){
    
  $urlRouterProvider.otherwise("/programs");
  $locationProvider.html5Mode(false);
    
}]);