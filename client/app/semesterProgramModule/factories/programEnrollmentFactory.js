// usersFactories.js

semesterProgramModule.factory('programEnrollmentFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {
  
  var programEnrollmentResource = $resource('api/semester/:sem/program/:prg/enrollment.json', 

    { sem: '@sem', prg: '@prg' },
   
    { 
      
      'query' : {method: 'GET', isArray: false}

    });

  var factory = {

    // add promises
    query : function (params) {
    
      var deferred = $q.defer();
    
      programEnrollmentResource.query({sem: params.semester, prg: params.program},
    
        function (resp) {
    
          deferred.resolve(resp);
    
        });
    
      return deferred.promise;
    
    }

  };

  return factory;

}]);