semesterProgramModule.controller('prgClassesController', ['$scope', '$q', '$stateParams', 'classesFactory', 'usersFactory', 'messageCenterService', function ($scope, $q, $stateParams, classesFactory, usersFactory, messageCenterService) {

    // as prgClasses

    console.log('prgClassesController instantiated');

    var ctrl = this;

    ctrl.loaded = false;

    ctrl.classList = [];

    ctrl.loadList = function(){

        console.log('loading classes');

        classesFactory.query($stateParams).then(function(data){

            ctrl.loaded = true; 

            ctrl.classList =  data.classes;

        }, function (error){

            console.log('ruh roh');

        });

    };

    ctrl.loadList();

    usersFactory.teachers().then(function (data){

        ctrl.teachersList =  data;

    }, function (error){

        console.log('ruh roh');

    });
  
}]);