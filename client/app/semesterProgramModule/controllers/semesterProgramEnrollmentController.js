// start programSubControllers.js

semesterProgramModule.controller('prgEnrollmentController', ['$scope', '$q', '$stateParams', 'programEnrollmentFactory', function ($scope, $q, $stateParams, programEnrollmentFactory) {
 
  console.log('prgEnrollmentController instantiated');

  var ctrl = this;
 
  ctrl.enrollment = [];

  ctrl.displayList = [];

  programEnrollmentFactory.query($stateParams)

    .then(function (data){

      ctrl.enrollment = data.students;

      ctrl.displayList = [].concat(ctrl.enrollment);

    }, function (error){

      console.log('ruh roh');

  });

}]);