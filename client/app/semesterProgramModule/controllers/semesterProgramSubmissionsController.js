semesterProgramModule.controller('prgSubmissionsController', ['$scope', '$q', '$stateParams', 'programSubmissionsFactory', function ($scope, $q, $stateParams, programSubmissionsFactory) {

    console.log('prgSubmissionsController instantiated');

    var ctrl = this;

    ctrl.subsList =  [];

    ctrl.displayList = [];

    ctrl.submissionsExist = false;

    programSubmissionsFactory.query($stateParams)

    .then(function (data){

        if (data.submissions && data.submissions.length > 0) ctrl.submissionsExist = true; 
    
        ctrl.subsList = data.submissions;
  
        ctrl.displayList = data.submissions;
  
        return data;

    }, function (error){

        console.log('ruh roh');

    });

}]);