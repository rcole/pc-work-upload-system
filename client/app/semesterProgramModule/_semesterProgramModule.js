// _semesterProgramModule.js 

var semesterProgramModule = angular.module('semesterProgramModule', ['ui.router','smart-table','ngResource','usersModule']);

// routes for semester > program > class pathways
semesterProgramModule.config(['$stateProvider','$urlRouterProvider','$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider){    
    
  $urlRouterProvider.when('/semester/{semester}/program/{program}', '/semester/{semester}/program/{program}/classes');

  var semesterView = {
    name: "semesterView",
    url: "/semester/:semester",
    controller: "semesterController as sem",
    abstract: true,
    template: '<ui-view/>'
  }, 
  programView = {
    name: "programView",
    parent: 'semesterView',
    url: "/program/:program",
    templateUrl: "app/webroot/tpl/program.tabFrame.tpl.html",
    controller: "programController as prg"
  },
  programSubmissions = {
    name: "programSubmissions",
    parent: 'programView',
    url: "/submissions",
    templateUrl: "app/webroot/tpl/program.submissions.tpl.html",
    controller: "prgSubmissionsController as prgSubs"
  },
  programClasses = {
    name: "programClasses",
    parent: 'programView',
    url: "/classes",
    templateUrl: "app/webroot/tpl/program.classList.tpl.html",
    controller: "prgClassesController as prgClasses"
  },
  classEdit = {
    name: "classEdit",
    parent: 'programClasses',
    url: "/:class/edit",
    templateUrl: "app/webroot/tpl/classForm.tpl.html",
    controller: "prgClassEditController as classForm"
  },
  classCreate = {
    name: "classCreate",
    parent: 'programClasses',
    url: "/create",
    templateUrl: "app/webroot/tpl/classForm.tpl.html",
    controller: "prgClassCreateController as classForm"
  },
  programEnrollment = {
    name: "programEnrollment",
    parent: 'programView',
    url: "/enrollment",
    templateUrl: "app/webroot/tpl/program.enrollment.tpl.html",
    controller: "prgEnrollmentController as prgEnroll"
  };

  $stateProvider
    .state(semesterView)
    .state(programView)
    .state(programSubmissions)
    .state(programClasses)
    .state(classEdit)
    .state(classCreate)
    .state(programEnrollment);
 }]);
