globals.directive('checkSame', ["$document", function ($document) {

	// check-same="lang1 false"
	// first > field to compare with by ID
	// second > true : should match , false : should not match
	
	return {
	
		require: 'ngModel',
	
		link: function (scope, elem, attrs, ctrl) {
	
			var expression = attrs.checkSame,
				firstField = '#' + expression.split(' ')[0],
				match = expression.split(' ')[1];

			var $firstField = angular.element($document[0].querySelector(firstField));
	
			function checkIt(){

				scope.$apply(function () {
		
					var v = elem.val() === $firstField.val();
					
					v = match === 'true' ? v : !v;

					ctrl.$setValidity('match', v);
		
				});

			}

			elem.on('input propertychange', function () {
			
				checkIt();
			
			});

			$firstField.on('input propertychange', function () {
			
				checkIt();
			
			});

		}

	};

}]);