globals.directive('back', function factory($window) {
 
      return {
 
        restrict   : 'EA',
   
        link: function (scope, element, attrs) {

            element.on('click', function(){

                $window.history.back();

            });
  
        }
 
      };
 
    });