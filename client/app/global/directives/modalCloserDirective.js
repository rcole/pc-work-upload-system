globals.directive('modalBack', ['$window', '$state', '$document', function factory($window, $state, $document) {
 
      return {
 
        restrict   : 'EA',
   
        link: function (scope, element, attrs) {

            // This is not possible due to no click event on : after!!!!

            var elsewhere = true;

            var clickElsewhere = function() {

                console.log('clicked elsewhere');
            
                if (elsewhere) {
            
                    // go back here

                    $state.go('^');
            
                    $document.off('click', clickElsewhere);
            
                }
            
                elsewhere = true;
            
            };
        
            element.on('click', function(e){

                console.log('clicked', elsewhere);

                // catch clicks here as to not close the modal.
        
                elsewhere = false;
            
                $document.off('click', clickElsewhere);
            
                $document.on('click', clickElsewhere);
        
            });
  
        }
 
      };
 
    }]);