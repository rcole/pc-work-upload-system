globals.directive('personMenu', ['usersFactory', '$document', function (usersFactory, $document) {
		return {
				restrict: 'A',

				scope: {pID : '=personMenu'},
				
				link: function (scope, element, attrs) {

					var elsewhere = true;

					var clickElsewhere = function() {
					
						if (elsewhere) {
					
							element.find('.personMenu').removeClass('personMenuOpen');
					
							$document.off('click', clickElsewhere);
					
						}
					
						elsewhere = true;
					
					};
				
					element.on('click', function(e){
				
						element.find('.personMenu').addClass('personMenuOpen');

						elsewhere = false;
					
						$document.off('click', clickElsewhere);
					
						$document.on('click', clickElsewhere);
				
					});

					usersFactory.get(scope.pID).then(function(result){

						if(result){

							scope.fullName = result.user.first_name + ' ' + result.user.last_name;

							scope.email = result.user.email;

						} else {

							element.off('click');

							scope.fullName = scope.pID;

							scope.email = '';

						}

					});
				
				},

				templateUrl: "app/webroot/tpl/part.personMenu.tpl.html"
		};
}]);