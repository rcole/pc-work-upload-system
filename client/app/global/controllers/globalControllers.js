// start globals.js

globals.controller('globalsController', ['$scope', '$location', '$stateParams', '$state', 'globalsFactory','semesterFactory', 'programsFactory', 'usersFactory', 'profileFactory', 'uvFactory', 'analyticsFactory', function($scope, $location, $stateParams, $state, globalsFactory, semesterFactory, programsFactory, usersFactory, profileFactory, uvFactory, analyticsFactory){
    
    console.log('globals ctrl');

    var ctrl = this;

    // still need this until all instances of old select2 plugin are removed.
    ctrl.selectOptions = {allowClear:true };

    ctrl.selectedSemester = semesterFactory.selectedSemester; 

    var semesterUpdater = function(){

        // this gets called from the semester factory whenever the selected sem changes. 

        ctrl.selectedSemester = semesterFactory.selectedSemester; 

        // if the selected semester does not match the one in the current route, then update the view

        if($stateParams.semester && $stateParams.semester != semesterFactory.selectedSemester) {

            $state.go($state.$current, {semester: semesterFactory.selectedSemester});

        }

    },

    initSemester = function(){

        if($stateParams.semester){

            semesterFactory.setSelectedSemester($stateParams.semester);

        } else {

            semesterFactory.setSelectedSemester(ctrl.currentSemester); 

        }

    };

    semesterFactory.registerObserverCallback(semesterUpdater); // register semesterUpdater to get called when the selected semester changes

    ctrl.changeSemester = function(selection){

        semesterFactory.setSelectedSemester(selection);

    };

    semesterFactory.getCurrentSemester().then(function(data){

        if(data.semester){

            ctrl.currentSemester = data.semester.title;

            // redundant if I can update the title-only references throughout the app.
            ctrl.currentSemesterObj = data.semester;

        } else {

            ctrl.currentSemester = "No Current Semester";

        }

        initSemester();
    
    }, function(error){
        
    });
    
    ctrl.userInfo = {};

    profileFactory.get().then(function(data){

        // find out if the current user is a teacher

        angular.forEach(data.user.roles_users, function(value, key){
    
            if (value.role_id == 3) {

                data.user.isTeacher = true;

            }
        
        });

        // sets up user voice user ID
        uvFactory(data.user);

        analyticsFactory.identify(data.user);

        ctrl.userInfo = data.user;

    }, function(error){
    
        console.log('profile error: ' + error);
    
    });

    ctrl.state = {

        userToolsVisible : false,
    
        toggleUserTools : function(){
  
            this.userToolsVisible = !this.userToolsVisible;
    
        }
    };

    // init on page load only when we have params. Otherwise current semester is not ready yet.

    if($stateParams.semester){

        initSemester();

    }

    $scope.$on('$locationChangeSuccess', function () {

        initSemester();

        ctrl.state.userToolsVisible = false;

    });

    ctrl.semesterList = [];

    semesterFactory.query().then(function(data){

        ctrl.semesterList = data.semesters;

    }, function (error){
  
        console.log('ruh roh');
  
    });

    ctrl.teachers = [];

    usersFactory.teachers().then(function (data){

      ctrl.teachers = data.roles;
    
    }, function (error){
    
      console.log('ruh roh');
    
    });

}]);
