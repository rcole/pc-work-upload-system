// Start nav.js

globals.controller ('navController', ['$scope','$q','$state','$stateParams','programsFactory','globalsFactory','semesterFactory', function($scope, $q, $state, $stateParams, programsFactory, globalsFactory, semesterFactory) {

    console.log('navController instantiated');  

    var ctrl = this;
 
    ctrl.programsListOpen = false;

    ctrl.toggleProgramsList = function(){
 
        ctrl.programsListOpen = !ctrl.programsListOpen; 
  
    };

    // populates nav items
    
    ctrl.programList = [];

    programsFactory.query().then(function (data){

        ctrl.programList = data.programs;

    }, function (error){

        console.log('ruh roh');

    });

}]);