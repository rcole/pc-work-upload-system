// start globals.js

globals.controller('studentGlobalsController', ['$scope', '$location', '$stateParams', '$state', 'globalsFactory','semesterFactory', 'programsFactory', 'usersFactory', 'profileFactory', 'uvFactory', 'analyticsFactory', function($scope, $location, $stateParams, $state, globalsFactory, semesterFactory, programsFactory, usersFactory, profileFactory, uvFactory, analyticsFactory){
    
    console.log('studentGlobalsController');

    var ctrl = this;

    ctrl.selectOptions = {allowClear:true };

    ctrl.selectedSemester = semesterFactory.selectedSemester; 

    var initSemester = function(){

      if($stateParams.semester){

        semesterFactory.setSelectedSemester($stateParams.semester);

      } else {

        semesterFactory.setSelectedSemester(ctrl.currentSemester); 

      }

    };

    semesterFactory.getCurrentSemester().then(function(data){

      if(data.semester){

        ctrl.currentSemester = data.semester.title;

      } else {

        ctrl.currentSemester = "No Current Semester";

      }

      initSemester();
    
    }, function(error){
        
    });
    

    ctrl.userInfo = {};

    profileFactory.get().then(function(data){

        uvFactory(data.user);

        analyticsFactory.identify(data.user);

        ctrl.userInfo = data.user;

    }, function(error){
    
      console.log('profile error: ' + error);
    
    });

    ctrl.state = {

        userToolsVisible : false,
    
        toggleUserTools : function(){
  
          this.userToolsVisible = !this.userToolsVisible;
    
        }
    };

    // init on page load only when we have params. Otherwise current semester is not ready yet.

    if($stateParams.semester){

      initSemester();

    }

    $scope.$on('$locationChangeSuccess', function () {

      initSemester();

      ctrl.state.userToolsVisible = false;

    });
    

    ctrl.semesterList = [];

    semesterFactory.query().then(function(data){

      ctrl.semesterList = data.semesters;

    }, function (error){
  
      console.log('ruh roh');
  
    });

}]);
