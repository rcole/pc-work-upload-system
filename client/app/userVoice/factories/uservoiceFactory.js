uservoiceModule.factory('uvFactory', [function() {    

    factory = function(profile){

        UserVoice.push(['identify', {
    
            email: profile.email,
    
            name: profile.first_name + ' ' + profile.last_name,
    
            id: profile.id,
    
            type: profile.roles_users[0].role_id
    
        }]);

    };

    return factory;
    
}]);