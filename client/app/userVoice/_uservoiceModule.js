// usersModule.js Starts here 

var uservoiceModule = angular.module('uservoiceModule', []);

uservoiceModule.config(['$stateProvider','$urlRouterProvider','$locationProvider',function($stateProvider, $urlRouterProvider, $locationProvider){
    
    console.log('user voice facotry called.');

    UserVoice=window.UserVoice||[];

    (function(){

      var uv=document.createElement('script');

      uv.type='text/javascript';

      uv.async=true;

      uv.src='//widget.uservoice.com/PfyxW3kgH950qRwRdgXMeQ.js';

      var s=document.getElementsByTagName('script')[0];

      s.parentNode.insertBefore(uv,s);

    })();

    UserVoice.push(['set', {
      accent_color: '#6aba2e',
      trigger_color: 'white',
      trigger_background_color: '#6aba2e'
    }]);

    UserVoice.push(['addTrigger', { mode: 'contact', trigger_position: 'bottom-left' }]);

    // Autoprompt for Satisfaction and SmartVote (only displayed under certain conditions)
    // UserVoice.push(['autoprompt', {}]);

    // the user info gets filled in when the profile endpoint is called in globals controller by the uvFactory.

}]);