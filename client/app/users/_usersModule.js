// usersModule.js Starts here 

var usersModule = angular.module('usersModule', ['smart-table','ngResource']);

usersModule.config(['$stateProvider','$urlRouterProvider','$locationProvider',function($stateProvider, $urlRouterProvider, $locationProvider){
    
    var users = {
        name: "users",
        url: "/users",
        templateUrl: "app/webroot/tpl/userList.tpl.html",
        controller: "usersController as users"
    },
    user = {
        name: "user",
        url: "/user/:user",
        templateUrl: "app/webroot/tpl/part.editUserForm.tpl.html",
        controller: "userController as user",
        parent: users
    },
    editUser = {
        name: "editUser",
        url: "/:user/edit",
        templateUrl: "app/webroot/tpl/userForm.tpl.html",
        controller: "userEditController as userForm",
        parent: 'users'
    },
    createUser = {
        name: "createUser",
        url: "/create",
        templateUrl: "app/webroot/tpl/userForm.tpl.html",
        controller: "userCreateController as userForm",
        parent: 'users'
    },
    importUsers = {
        name: "importUsers",
        url: "/import",
        templateUrl: "app/webroot/tpl/importForm.tpl.html",
        controller: "usersImportController as usersImport",
        parent: 'users'
    };

    $stateProvider
        .state(users)
        .state(user)
        .state(editUser)
        .state(createUser)
        .state(importUsers);

}]);

usersModule.constant('userRoles', [
    {   
        role_id: '1',
        role_title: 'Admin'
    },
    {
        role_id: '2',
        role_title: 'Program Leader'
    },
    {
        role_id: '3',
        role_title: 'Teacher'
    },
    {
        role_id: '4',
        role_title: 'Adacemic Services'
    },
    {
        role_id: '5',
        role_title: 'External Verifier'
    },
    {
        role_id: '6',
        role_title: 'Student Records'
    },
    {
        role_id: '7',
        role_title: 'Student'
    }
]);

usersModule.constant('userStatus', [
    
    { 
        status_id : '1',
        status_title: 'Active'
    },
    { 
        status_id : '2',
        status_title:'Inactive'
    }
    
]);

