// usersControllers.js

usersModule.controller('userEditController', ['$scope', '$stateParams', '$state', '$timeout', 'usersFactory', 'messageCenterService', 'userRoles', function ($scope, $stateParams, $state, $timeout, usersFactory, messageCenterService, userRoles) {
  
    console.log('userEditController instantiated');

    var ctrl = this;

    ctrl.mode = 'edit';

    ctrl.modal = true; 

    ctrl.deleting = false;

    ctrl.message = '';

    ctrl.rolesList = userRoles;

    ctrl.toggleDelete = function(){

        ctrl.deleting = !ctrl.deleting;

    };

    ctrl.deleteItem = function(){
  
        usersFactory.remove($stateParams).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while deleting the user.", { timeout: 6000 });

                console.log(data);

            } else {
                
                ctrl.message = 'User Deleted';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);
            }

            $scope.$parent.users.loadList();

        },

        function(error){

            console.log(error);

        });

    };
    
    ctrl.updateItem = function(){

        ctrl.item.roles_users = [];

        for (var i = ctrl.roles.length - 1; i >= 0; i--) {

            var temp = {
            
                role_id: ctrl.roles[i],

                user_id: ctrl.item.id
            
            };
            
            ctrl.item.roles_users.push(temp);
        
        }
  
        usersFactory.update($stateParams, ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the user.", { timeout: 6000 });

                console.log(data);

            } else {

                ctrl.message = 'User Updated';

                $timeout(function(){

                    ctrl.message = '';

                }, 1500);

            }

            $scope.$parent.users.loadList();

        },

        function(error){

            console.log(error);

        });

    };

    usersFactory.get($stateParams).then(function(data){

        if (data._error.code == 1) {

            messageCenterService.add('danger', "Something went wrong while retrieving the user.", { timeout: 6000 });

        } else {

            ctrl.item = data.user;

            ctrl.roles = [];

            for (var i = ctrl.item.roles_users.length - 1; i >= 0; i--) {
          
                ctrl.roles.push(ctrl.item.roles_users[i].role_id);
          
            }

        }

    },

    function(error){

        console.log(error);

    });

}]);