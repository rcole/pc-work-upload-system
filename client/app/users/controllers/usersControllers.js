// usersControllers.js

usersModule.controller('usersController', ['$scope', 'usersFactory', 'messageCenterService', function ($scope, usersFactory, messageCenterService) {
  
    console.log('usersController instantiated');

    var ctrl = this;

    ctrl.usersList = [];

    ctrl.displayList = [];

    ctrl.loaded = false;

    ctrl.loadList = function(){

        usersFactory.query().then(function (data){
    
            ctrl.usersList = data.users;

            ctrl.displayList = [].concat(ctrl.usersList);

            ctrl.loaded = true;
      
        }, function (error){
      
            console.log('ruh roh');
      
        });

    };

    ctrl.loadList();

}]);