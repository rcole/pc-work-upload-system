// really not done. Break this up, flatten the methods

myclassesModule.factory('myClassesSubmissionsFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {

    var mcAllSubsResource = $resource('api/semester/:semester/myclasses/submissions.json', 

    { semester: '@semester'},
    
    {

        'query': {method: 'GET'}

    });

    var mcClassSubsResource = $resource('api/semester/:semester/myclasses/:class/submissions.json', 

    { semester: '@semester', class: '@class' },
    
    {

        'query': {method: 'GET'}

    });

    var mcStuSubsResource = $resource('api/semester/:semester/myclasses/:class/submissions/:id.json', 

    { semester: '@semester', class: '@class', id: '@id' },
    
    {

        'query': {method: 'GET'}

    });


    var factory = {

        all : function(params) {
       
            var deferred = $q.defer();

            mcAllSubsResource.query({semester: params.semester},
       
            function (resp) {
       
                deferred.resolve(resp);
       
            });

            return deferred.promise;
       
        },
   
        classSubs : function(params) {
       
            var deferred = $q.defer();

            mcClassSubsResource.query({semester: params.semester, class: params.class},

            function (resp) {

                deferred.resolve(resp);

            });

            return deferred.promise;
       
        },

        student : function(params) {
       
            var deferred = $q.defer();

            mcStuSubsResource.query({semester: params.semester, class: params.class, id: params.id},

            function (resp) {

                deferred.resolve(resp);

            });

            return deferred.promise;
       
        }
  
    };
  
  return factory;

}]);