// really not done. Break this up, flatten the methods

myclassesModule.factory('myClassesEnrollmentFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {

   var mcEnrolledResource = $resource('api/semester/:semester/myclasses/:class/enrollment.json', 

    { semester: '@semester', class: '@class'},
    
    {

      'query': {method: 'GET', isArray: false, cache: true}

    });


  var factory = {
   
    query : function(params) {
   
      var deferred = $q.defer();

      mcEnrolledResource.query({semester: params.semester, class: params.class},
   
        function (resp) {
   
          deferred.resolve(resp);
   
        });

      return deferred.promise;
   
    }

  };
  
  return factory;

}]);