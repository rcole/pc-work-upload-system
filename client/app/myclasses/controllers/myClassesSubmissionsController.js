// myClassesSubmissionsController.js

myclassesModule.controller('myClassesSubmissionsController', ['$scope', '$q', '$stateParams', 'myClassesSubmissionsFactory', function ($scope, $q, $stateParams, myClassesSubmissionsFactory) {

  console.log('myClassesSubmissionsController instantiated');

  var ctrl = this;

  ctrl.loaded = false;

  ctrl.subsList = [];
  
  ctrl.displayList = [];

  myClassesSubmissionsFactory.all($stateParams).then(function (data){
  
      if (data._error != 1) {

        ctrl.subsList = data.submissions;

        ctrl.displayList = data.submissions;

      } else {

        // error here

      }

      ctrl.loaded = true;
  
    }, function (error){
  
      console.log('ruh roh');
  
    });

}]);