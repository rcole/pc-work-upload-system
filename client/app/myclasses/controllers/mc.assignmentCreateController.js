// semesterProgramClassAssignmentController.js

myclassesModule.controller('mcAssignmentCreateController', ['$scope', '$q', '$stateParams', '$state', '$timeout', 'myClassesAssignmentFactory', 'messageCenterService', function ($scope, $q, $stateParams, $state, $timeout, myClassesAssignmentFactory, messageCenterService) {
  
    console.log('mcAssignmentCreateController instantiated');

    var ctrl = this;

    ctrl.mode = 'create';

    ctrl.modal = true; 

    ctrl.message = '';

    ctrl.createItem = function(){

        ctrl.item.class_id = $scope.$parent.mcClass.classSelection.id;

        ctrl.item.semester_id = $scope.$parent.sem.semesterSelection.id;

        myClassesAssignmentFactory.create($stateParams, ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the user.", { timeout: 6000 });

                console.log(data);

            } else {

                ctrl.message = 'User Created';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);

            }

            $scope.$parent.mcClsAssign.loadList();

        },

        function(error){

            console.log(error);

        });

    };

}]);