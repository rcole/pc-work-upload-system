// myClassesClassController.js

myclassesModule.controller('myClassesClassController', ['$scope', '$q', 'myClassesFactory', 'globalsFactory', '$stateParams', '$state', function ($scope, $q, myClassesFactory, globalsFactory, $stateParams, $state) {
 
    console.log('myClassesClassController instantiated');
  
    var ctrl = this;

    ctrl.classSelection = {};

    myClassesFactory.get($stateParams)

    .then(function (data){

        ctrl.classSelection = data.classes[0];
    
    }, function (error){
    
        console.log('ruh roh');
  
    });

    ctrl.classSwitcher = function($item, $model){

        page = $state.current.name;

        $state.go(page, {class: $item.slug});

    };

}]);
