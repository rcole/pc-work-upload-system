// myClassesClassEnrollmentController.js

myclassesModule.controller('myClassesClassEnrollmentController', ['$scope', '$q', '$stateParams', 'myClassesEnrollmentFactory', function ($scope, $q, $stateParams, myClassesEnrollmentFactory) {

  console.log('myClassesClassEnrollmentController instantiated');

  var ctrl = this;

  ctrl.studentList = [];

  ctrl.displayList = [];


  myClassesEnrollmentFactory.query($stateParams)

    .then(function (data){

        ctrl.studentList =  data.students;

        ctrl.displayList =  data.students;

    }, function (error){

        console.log('ruh roh: error in myclasses controller');

    });

}]);