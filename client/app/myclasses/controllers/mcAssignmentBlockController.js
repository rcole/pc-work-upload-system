// semesterProgramClassAssignmentController.js

myclassesModule.controller('mcAssignmentBlockController', ['$scope', '$q', '$stateParams', 'myClassesAssignmentFactory', 'messageCenterService', function ($scope, $q, $stateParams, myClassesAssignmentFactory, messageCenterService) {
  
    console.log('mcAssignmentBlockController instantiated');

    var ctrl = this;

    ctrl.viewingSubs = false;

    ctrl.loading = false;

    ctrl.selectedForDownload = [];

    ctrl.toggleSubs = function(assign){
 
        ctrl.viewingSubs = !ctrl.viewingSubs;
 
        ctrl.selectedForDownload = [];

        getAssignmentData(assign);
 
    };

    ctrl.manyToDownload = function(){

        // do this in the template instead
 
        if(ctrl.selectedForDownload.length > 1) return true;
 
        return false;
  
    };

    function getAssignmentData(assign){

        if(assign) {

            ctrl.loading = true;

            $stateParams.assg = assign;

            myClassesAssignmentFactory.get($stateParams)
            
            .then(function (data){

                if (data._error.code == 1) {

                    messageCenterService.add('danger', "Something went wrong while saving the program.", { timeout: 6000 });

                } else {

                    ctrl.assignmentSubsGrid = data.submissions;

                    ctrl.assignmentSubsDisplayList = [].concat(ctrl.assignmentSubsGrid);

                    ctrl.loading = false;
                }

            }, 

            function (error){

                console.log('Error');

                console.log(error);

            });
        }

    }

}]);