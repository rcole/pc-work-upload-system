// myClassesClassSubmissionsController.js

myclassesModule.controller('myClassesClassSubmissionsController', ['$scope', '$q', '$stateParams', 'myClassesSubmissionsFactory', function ($scope, $q, $stateParams, myClassesSubmissionsFactory) {

    console.log('myClassesClassSubmissionsController instantiated');

    var ctrl = this;

    ctrl.subsList = [];
  
    ctrl.displayList = [];

    ctrl.loaded = false;

    myClassesSubmissionsFactory.classSubs($stateParams)

    .then(function (data){

      if (data._error != 1) {

        ctrl.subsList = data.submissions;

      } else {

        // error here

      }

      ctrl.loaded = true;

    }, function (error){

      console.log('ruh roh');

});

}]);