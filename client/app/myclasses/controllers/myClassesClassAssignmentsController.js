// myClassesClassAssignmentsController.js

myclassesModule.controller('myClassesClassAssignmentsController', ['$scope', '$q', '$stateParams', 'myClassesAssignmentFactory', function ($scope, $q, $stateParams, myClassesAssignmentFactory) {
  
    console.log('myClassesClassAssignmentsController instantiated');

    var ctrl = this;

    ctrl.loaded = false;

    ctrl.assignmentList = [];

    ctrl.loadList = function(){

        myClassesAssignmentFactory.query($stateParams).then(function (data){

            ctrl.assignmentList = data.assignments;

            ctrl.loaded = true;

        }, function (error){

            console.log('ruh roh');

        });

    };

    ctrl.loadList();
  
}]);