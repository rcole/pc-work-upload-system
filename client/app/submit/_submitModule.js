// submitModule.js Starts here 

var submitModule = angular.module('submitModule', ['smart-table','ngResource','flow','ui.select']);

submitModule.config(['$stateProvider','$urlRouterProvider','$locationProvider','flowFactoryProvider',function($stateProvider, $urlRouterProvider, $locationProvider,flowFactoryProvider){
  
    var submit = {
        name: "submit",
        url: "/submit",
        templateUrl: "app/webroot/tpl/submitForm.tpl.html",
        controller: "submitController as submit"
    },
    mySubmissions = {
        name: "mysubmissions",
        url: "/mysubmissions",
        templateUrl: "app/webroot/tpl/mySubmissions.tpl.html",
        controller: "mySubmissionsController as mysubmits"
    };

    $stateProvider.state(submit);
    $stateProvider.state(mySubmissions);

    flowFactoryProvider.defaults = {
        target: 'api/submit.json',
        permanentErrors: [404, 415, 500, 501],
        maxChunkRetries: 1,
        chunkRetryInterval: 5000,
        simultaneousUploads: 4
    };

    flowFactoryProvider.on('filesAdded', function ($files) {

    });

}]);

submitModule.constant('ignoredFileTypes', ['.DS_Store', '.localized', 'Thumbs.db', '.dropbox.cache', '.dropbox', 'Icon']);

