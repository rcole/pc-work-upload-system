// start submitFactory.js

submitModule.factory('submitFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {
  

  var submitResource = $resource('api/submit.json', {}, { 'create': {method: 'POST'} });

  var mySubmitsResource = $resource('api/semester/:semester/mysubmissions.json', {semester: '@semester'}, { 'query': {method: 'GET'} });

  var factory = {

    // this is unused so far as we are using $flow for uploads.
    submit : function (payload) {

      var deferred = $q.defer();
  
      submitResource.create(payload, function(resp){
 
        deferred.resolve(resp);
 
      });

      return deferred.promise;
 
    },
    mySubmissions : function (params) {

      var deferred = $q.defer();
  
      mySubmitsResource.query(params, function(resp){
 
        deferred.resolve(resp);
 
      });

      return deferred.promise;
 
    }
  
  };
  
  return factory;

}]);