uservoiceModule.factory('analyticsFactory', [function() {    

    factory = {
        
        identify: function(profile){

            heap.identify({
                name: profile.first_name + ' ' + profile.last_name,
                syloId: profile.id,
                userRole: profile.roles_users[0].role_id,
                email: profile.email
            });

        },

        uploadErrors : function(report){

            console.log('error report sent', report);

            heap.track('Upload Errors', report);

        },

        uploadSuccess : function(report){

            console.log('success report sent', report);

            heap.track('Upload Success', report);

        }

    };

    return factory;
    
}]);