// APP.js Starts here 

// This file declares and sets all the module dependencies at the top level.
// Also declares all routes, by module. This is simplfy making modules per user type.

// define all modules
var programsModule = angular.module('programsModule', ['smart-table','ngResource']);

programsModule.config(['$stateProvider','$urlRouterProvider','$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider){
  
	var programs = {
		name: "programs",
		url: "/programs",
		templateUrl: "app/webroot/tpl/programList.tpl.html",
		controller: "programsController as programs"
    },
    programEdit = {
		name: "programEdit",
		parent: "programs",
		url: "/:program/edit",
		templateUrl: "app/webroot/tpl/programForm.tpl.html",
		controller: "programEditController as prgForm"
    },
    programCreate = {
		name: "programCreate",
		parent: "programs",
		url: "/create",
		templateUrl: "app/webroot/tpl/programForm.tpl.html",
		controller: "programCreateController as prgForm"
    };

    $stateProvider
		.state(programs)
		.state(programEdit)
		.state(programCreate);

}]);