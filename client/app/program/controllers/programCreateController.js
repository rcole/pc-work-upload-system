programsModule.controller('programCreateController', ['$scope','$q','$timeout','$state','programsFactory', 'usersFactory', 'semesterFactory', 'messageCenterService', function($scope, $q, $timeout, $state, programsFactory, usersFactory, semesterFactory, messageCenterService) {
  
    console.log('programCreateController');
    
    var ctrl = this;

    ctrl.mode = 'create';

    ctrl.modal = true; 

    ctrl.message = '';

    ctrl.program_authorizations = [];

    ctrl.createItem = function(){

        ctrl.item.program_authorizations = [];

        for (var i = ctrl.program_authorizations.length - 1; i >= 0; i--) {

            var temp = {
            
                program_leader_id: ctrl.program_authorizations[i]
            
            };
            
            ctrl.item.program_authorizations.push(temp);
        
        }
  
        programsFactory.create(ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the program.", { timeout: 6000 });

            } else {

                ctrl.message = 'Program Created';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);

            }

            $scope.$parent.programs.loadList();

        },

        function(error){

            console.log(error);

        });

    };
    
}]);