programsModule.controller('programEditController', ['$scope','$q','$stateParams','$state','$timeout','programsFactory', 'usersFactory', 'semesterFactory', 'messageCenterService', function($scope, $q, $stateParams, $state, $timeout, programsFactory, usersFactory, semesterFactory, messageCenterService) {
  
  console.log('programEditController');

  var ctrl = this;

    ctrl.mode = 'edit';

    ctrl.modal = true; 

    ctrl.deleting = false;

    ctrl.message = '';

    ctrl.toggleDelete = function(){

        ctrl.deleting = !ctrl.deleting;

    };

        ctrl.onSelect = function($item, $model){

        ctrl.savingLeader = true;

        var auth = {
        
            program_authorizations : [

                {
                    program_id: ctrl.item.id,

                    program_leader_id: $model
                }

            ]
        
        };

        programsFactory.addAuth(auth).then(function(data){

            ctrl.savingLeader = false;

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while adding the program leader.", { timeout: 6000 });

            } 

            $scope.$parent.programs.loadList();

        },

        function(error){

            console.log(error);

        });

    };

    ctrl.onRemove = function($item, $model){

        ctrl.savingLeader = true;

        var auth = {
        
            program_authorizations : [

                {
                    program_id: ctrl.item.id,

                    program_leader_id: $model
                }

            ]
        
        };

        programsFactory.removeAuth(auth).then(function(data){

            ctrl.savingLeader = false;

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while removing the program leader.", { timeout: 6000 });

            }

            $scope.$parent.programs.loadList();

        },

        function(error){

            console.log(error);

        });

    };

    ctrl.deleteItem = function(){
  
        programsFactory.remove($stateParams).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while deleting the program.", { timeout: 6000 });

            } else {
                
                ctrl.message = 'Program Deleted';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);
            }

            $scope.$parent.programs.loadList();
        },

        function(error){

            console.error(error);

        });

    };
    
    ctrl.updateItem = function(){
  
        programsFactory.update($stateParams, ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the program.", { timeout: 6000 });

            } else {

                ctrl.message = 'Program Updated';

                $timeout(function(){

                    ctrl.message = '';

                    $state.go('programEdit', {program: data.program.slug});

                }, 1500);

            }

            $scope.$parent.programs.loadList();

        },

        function(error){

            console.error(error);

        });

    };

    programsFactory.get($stateParams).then(function(data){

        if (data._error.code == 1) {

            messageCenterService.add('danger', "Something went wrong while retrieving the program.", { timeout: 6000 });

        } else {

            ctrl.item = data.program;

            ctrl.program_authorizations = [];

            for (var i = ctrl.item.program_authorizations.length - 1; i >= 0; i--) {
          
                ctrl.program_authorizations.push(ctrl.item.program_authorizations[i].program_leader_id);
          
            }

        }

    },

    function(error){

        console.error(error);

    });

}]);