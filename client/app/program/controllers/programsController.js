programsModule.controller('programsController', ['$scope','$q','programsFactory', 'usersFactory', 'semesterFactory', 'messageCenterService', function($scope, $q, programsFactory, usersFactory, semesterFactory, messageCenterService) {
  
    console.log('programsController');

    var ctrl = this;

    ctrl.loaded = false;

    ctrl.loadList = function(){

        programsFactory.query().then(function (data){

            ctrl.programList = data.programs;

            ctrl.loaded = true;

        }, function (error){

            console.log('ruh roh');

        }); 

    };

    ctrl.loadList();

    ctrl.programLeaders = [];

    usersFactory.programLeaders().then(function (data){

        ctrl.programLeaders = data.roles;
    
    }, function (error){
    
        console.log('ruh roh');
    
    });

}]);