// start classesControllers.js

semesterProgramModule.controller('prgClassEditController', ['$scope', '$q', '$stateParams', '$state', '$timeout', 'classesFactory', 'modulesFactory', 'globalsFactory', 'messageCenterService', function ($scope, $q, $stateParams, $state, $timeout, classesFactory, modulesFactory, globalsFactory, messageCenterService) {

    console.log('prgClassEditController instantiated');
    
    var ctrl = this;
    
    ctrl.mode = 'edit';

    ctrl.modal = true; 

    ctrl.deleting = false;

    ctrl.message = '';

    ctrl.moduleList = [];

    ctrl.toggleDelete = function(){

        ctrl.deleting = !ctrl.deleting;

    };

    ctrl.onSelect = function($item, $model){

        ctrl.savingTeachers = true;

        var auth = {
        
            teacher_authorizations : [

                {
                    class_id: ctrl.item.id,

                    teacher_id: $model,

                    semester_id: $scope.$parent.globals.currentSemesterObj.id
                }

            ]
        
        };

        classesFactory.addAuth(auth).then(function(data){

            ctrl.savingTeachers = false;

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while adding the teacher.", { timeout: 6000 });

            } 

            $scope.$parent.prgClasses.loadList();

        },

        function(error){

            console.log(error);

        });

    };

    ctrl.onRemove = function($item, $model){

        ctrl.savingTeachers = true;

        var auth = {
        
            teacher_authorizations : [

                {
                    class_id: ctrl.item.id,

                    teacher_id: $model,

                    semester_id: $scope.$parent.globals.currentSemesterObj.id
                }

            ]
        
        };

        classesFactory.removeAuth(auth).then(function(data){

            ctrl.savingTeachers = false;

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while removing the teacher.", { timeout: 6000 });

            }

            $scope.$parent.prgClasses.loadList();

        },

        function(error){

            console.log(error);

        });

    };

    ctrl.deleteItem = function(){
  
        classesFactory.remove($stateParams).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while deleting the class.", { timeout: 6000 });

                console.log(data);

            } else {
                
                ctrl.message = 'Class Deleted';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);
            }

            $scope.$parent.prgClasses.loadList();

        },

        function(error){

            console.log(error);

        });

    };
    
    ctrl.updateItem = function(){

        classesFactory.update($stateParams, ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the class.", { timeout: 6000 });

                console.log(data);

            } else {

                ctrl.message = 'Class Updated';

                $timeout(function(){

                    ctrl.message = '';

                    $state.go('classEdit', {class: data.class.slug});

                }, 1500);

            }

            $scope.$parent.prgClasses.loadList();

        },

        function(error){

            console.log(error);

        });

    };

    classesFactory.get($stateParams).then(function(data){

        if (data._error.code == 1) {

            messageCenterService.add('danger', "Something went wrong while retrieving the class.", { timeout: 6000 });

        } else {

            ctrl.item = data.class;

            ctrl.teachers = [];

            for (var i = ctrl.item.teacher_authorizations.length - 1; i >= 0; i--) {
          
                ctrl.teachers.push(ctrl.item.teacher_authorizations[i].teacher_id);
          
            }

        }

    },

    function(error){

        console.log(error);

    });

    modulesFactory.query($stateParams).then(function (data){

        ctrl.moduleList = data.modules;

    }, function (error){

        console.log('ruh roh');

    });

}]);