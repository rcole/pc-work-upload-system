// semesterProgramClassAssignmentController.js

semesterProgramClassModule.controller('assignmentCreateController', ['$scope', '$q', '$stateParams', '$state', '$timeout', 'classAssignmentFactory', 'programsFactory', 'messageCenterService', function ($scope, $q, $stateParams, $state, $timeout, classAssignmentFactory, programsFactory, messageCenterService) {
  
    console.log('assignmentCreateController instantiated');

    var ctrl = this;

    var localProgramList = [];

    ctrl.mode = 'create';

    ctrl.modal = true; 

    ctrl.message = '';

    ctrl.createItem = function(){

        for (var i = localProgramList.length - 1; i >= 0; i--) {
          
            if(localProgramList[i].slug == $stateParams.program){

                ctrl.item.program_id = localProgramList[i].id;
                
            }
      
        }

        ctrl.item.class_id = $scope.$parent.cls.classSelection.id;

        ctrl.item.semester_id = $scope.$parent.sem.semesterSelection.id;

        classAssignmentFactory.create($stateParams, ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the assignment.", { timeout: 6000 });

            } else {

                ctrl.message = 'Assignment Created';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);

            }

            $scope.$parent.clsAssign.loadList();

        },

        function(error){

            console.log(error);

        });

    };

    programsFactory.query().then(function (data){

        localProgramList = data.programs;

    }, function (error){

        console.log('ruh roh');

    });

}]);