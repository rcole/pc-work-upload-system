// semesterProgramClassFeedbackController.js

semesterProgramClassModule.controller('classFeedbackController', ['$scope', '$q', '$stateParams', 'classesFactory', function ($scope, $q, $stateParams, classesFactory) {
  
  console.log('classFeedbackController instantiated');

  var ctrl = this;

  ctrl.classFeedbackExist = false;

  // TODO: build this factory and update 'exists'
  ctrl.classFeedbackListData = classesFactory.query($stateParams)
  
    .then(function (data){

      if (data.users && data.users.length > 0) ctrl.classFeedbackExist = true; // this will need to change to reflect actual data structure
  
      //console.log(data.users);
  
      return data.users;
  
    }, function (error){
  
      console.log('ruh roh');
  
    });

  // fix columns
  ctrl.classFeedbackList = { 
  
    data: 'classFeedbackListData',
  
    enableSorting: true,
  
    headerRowHeight: 30,
  
    headerClass: 'gridHeader',
  
    rowHeight: 40,
  
    columnDefs: [
  
      {field:'first_name', displayName:'First name'}, 
  
      {field:'last_name', displayName:'Last name'},
  
      {field:'email', displayName:'Email'}, 
  
      {field:'role_id', displayName:'Role'}, 
  
      {field:'id', displayName:'edit', cellTemplate: '<div><div class="ngCellText"><a href="users/{{row.getProperty(col.field)}}">view</a> - <a href="users/{{row.getProperty(col.field)}}/edit">edit</a></div></div>'}
  
    ]};
  
}]);