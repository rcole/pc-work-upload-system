// semesterProgramClassEnrollmentController.js

semesterProgramClassModule.controller('classEnrollmentFormController', ['$scope', '$q', '$stateParams', '$timeout', '$state', 'messageCenterService', 'classEnrollmentFactory', 'usersFactory', function ($scope, $q, $stateParams, $timeout, $state, messageCenterService, classEnrollmentFactory, usersFactory) {
  
  console.log('classEnrollmentFormController instantiated');

  var ctrl = this;

  ctrl.modal = true;

  ctrl.message = '';
  
  ctrl.newEnrollments = [];

  ctrl.studentList = [];

  ctrl.addEnrollment = function(){

    var enrollments = [];

    for (var i = ctrl.newEnrollments.length - 1; i >= 0; i--) {

      var temp = {

        class_id: $scope.$parent.cls.classSelection.id,

        semester_id: $scope.$parent.sem.semesterSelection.id,

        student_id: ctrl.newEnrollments[i].id

      };
    
      enrollments.push(temp);
    
    }

    classEnrollmentFactory.create($stateParams, {enrollments: enrollments}).then(function (data){
    
      if (data._error.code == 1) {

          messageCenterService.add('danger', "Something went wrong while enrolling the students.", { timeout: 6000 });

          console.log(data);

      } else {
          
          ctrl.message = 'Enrollment Updated';

          $timeout(function(){

              $state.go('^', {reload:true});

          }, 1500);
      }

      $scope.$parent.clsEnroll.loadList();
  
    }, function (error){
  
      console.log('ruh roh');
  
    });

  };

  usersFactory.students().then(function (data){
    
      ctrl.studentList = data.roles;
  
    }, function (error){
  
      console.log('ruh roh');
  
    });
  
}]);