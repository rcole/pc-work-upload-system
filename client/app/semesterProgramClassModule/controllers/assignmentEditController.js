// semesterProgramClassAssignmentController.js

semesterProgramClassModule.controller('assignmentEditController', ['$scope', '$q', '$stateParams', '$state', '$timeout', 'classAssignmentFactory', 'messageCenterService', function ($scope, $q, $stateParams, $state, $timeout, classAssignmentFactory, messageCenterService) {
  
    console.log('assignmentEditController instantiated');
    
    var ctrl = this;

    ctrl.mode = 'edit';

    ctrl.modal = true; 

    ctrl.deleting = false;

    ctrl.message = '';

    ctrl.toggleDelete = function(){

        ctrl.deleting = !ctrl.deleting;

    };

    ctrl.deleteItem = function(){
  
        classAssignmentFactory.remove($stateParams).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while deleting the assignment.", { timeout: 6000 });

            } else {
                
                ctrl.message = 'Assignment Deleted';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);
            }

            $scope.$parent.clsAssign.loadList();

        },

        function(error){

            console.log(error);

        });

    };
    
    ctrl.updateItem = function(){
  
        classAssignmentFactory.update($stateParams, ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the assignment.", { timeout: 6000 });

                console.log(data);

            } else {

                ctrl.message = 'Assignment Updated';

                $timeout(function(){

                    ctrl.message = '';

                    $state.go('assignmentEdit', {assg: data.assignment.slug});

                }, 1500);

            }

            $scope.$parent.clsAssign.loadList();

        },

        function(error){

            console.log(error);

        });

    };

    classAssignmentFactory.get($stateParams).then(function(data){

        if (data._error.code == 1) {

            messageCenterService.add('danger', "Something went wrong while retrieving the assignment.", { timeout: 6000 });

        } else {

            ctrl.item = data.assignment;

        }

    },

    function(error){

        console.log(error);

    });

}]);