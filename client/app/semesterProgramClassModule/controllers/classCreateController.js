// start classesControllers.js

semesterProgramModule.controller('prgClassCreateController', ['$scope', '$q', '$stateParams', '$state', '$timeout', 'classesFactory', 'modulesFactory', 'usersFactory', 'programsFactory', 'messageCenterService', function ($scope, $q, $stateParams, $state, $timeout, classesFactory, modulesFactory, usersFactory, programsFactory, messageCenterService) {

    console.log('prgClassCreateController instantiated');

    var ctrl = this;

    var localProgramList = [];

    ctrl.mode = 'create';

    ctrl.modal = true; 

    ctrl.message = '';

    ctrl.moduleList = [];

    ctrl.teachers = [];

    ctrl.createItem = function(){

        ctrl.item.teacher_authorizations = [];

        for (var j = ctrl.teachers.length - 1; j >= 0; j--) {

            var temp = {
            
                teacher_id: ctrl.teachers[j],

                semester_id: $scope.$parent.globals.currentSemesterObj.id
            
            };
            
            ctrl.item.teacher_authorizations.push(temp);
        
        }

        ctrl.item.semester_id = $scope.$parent.globals.currentSemesterObj.id;

        for (var i = localProgramList.length - 1; i >= 0; i--) {
          
            if(localProgramList[i].slug == $stateParams.program){

                ctrl.item.program_id = localProgramList[i].id;
                
            }
      
        }
  
        classesFactory.create($stateParams, ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the class.", { timeout: 6000 });

                console.log(data);

            } else {

                ctrl.message = 'Class Created';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);

            }

            $scope.$parent.prgClasses.loadList();

        },

        function(error){

            console.log(error);

        });

    };

    modulesFactory.query($stateParams).then(function (data){

        ctrl.moduleList = data.modules;

    }, function (error){

        console.log('ruh roh');

    });

    usersFactory.teachers().then(function (data){

        ctrl.teachers = data.roles;

    }, function (error){

        console.log('ruh roh');

    });

    programsFactory.query().then(function (data){

        localProgramList = data.programs;

    }, function (error){

        console.log('ruh roh');

    });

}]);