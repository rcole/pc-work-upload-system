// classAssignmentsController.js

semesterProgramClassModule.controller('classAssignmentsController', ['$scope', '$q', '$stateParams', 'classAssignmentFactory', 'usersFactory', 'messageCenterService', function ($scope, $q, $stateParams, classAssignmentFactory, usersFactory, messageCenterService) {
  
    console.log('classAssignmentsController instantiated');

    var ctrl = this;

    ctrl.loaded = false;

    ctrl.assignmentList = [];

    ctrl.loadList = function(){

        classAssignmentFactory.query($stateParams).then(function (data){

            ctrl.assignmentList = data.assignments;

            ctrl.loaded = true;

        }, function (error){

            console.log('ruh roh');

        });

    };

    ctrl.loadList();

}]);