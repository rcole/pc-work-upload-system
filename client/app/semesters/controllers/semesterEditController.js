// start semestersController.js

semestersModule.controller('semesterEditController', ['$scope', '$q', '$stateParams', '$state', '$timeout', 'semesterFactory', 'messageCenterService', function ($scope, $q, $stateParams, $state, $timeout, semesterFactory, messageCenterService) {
  
    console.log('semesterEditController instantiated');

    // get semester
    // map to form model
    // flatten to only scope methods

    var ctrl = this;    
    
    ctrl.mode = 'edit';

    ctrl.modal = true; 

    ctrl.deleting = false;

    ctrl.message = '';

    ctrl.toggleDelete = function(){

        ctrl.deleting = !ctrl.deleting;

    };

    ctrl.deleteItem = function(){
  
        semesterFactory.remove($stateParams).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while deleting the semester.", { timeout: 6000 });

                console.log(data);

            } else {
                
                ctrl.message = 'Semester Deleted';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);
            }

            $scope.$parent.semesters.loadList();

        },

        function(error){

            console.log(error);

        });

    };
    
    ctrl.updateItem = function(){
  
        semesterFactory.update($stateParams, ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the semester.", { timeout: 6000 });

                console.log(data);

            } else {

                ctrl.message = 'Semester Updated';

                $timeout(function(){

                    $state.go('semesterEdit', {semester: data.semester.slug});

                }, 1500);

            }

            $scope.$parent.semesters.loadList();

        },

        function(error){

            console.log(error);

        });

    };

    semesterFactory.get($stateParams).then(function(data){

        if (data._error.code == 1) {

            messageCenterService.add('danger', "Something went wrong while retrieving the semester.", { timeout: 6000 });

        } else {

            ctrl.item = data.semester;

        }

    },

    function(error){

        console.log(error);

    });
  
}]);
