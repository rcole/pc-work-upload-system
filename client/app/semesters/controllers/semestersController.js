// start semestersController.js

semestersModule.controller('semestersController', ['$scope', '$q', 'semesterFactory', 'messageCenterService', function ($scope, $q, semesterFactory, messageCenterService) {
  
    console.log('semestersController instantiated');

    var ctrl = this;

    ctrl.loaded = false;

    // semester list is also done in globals since we use it app wide basically, but also here so we can reload it rationally.

    ctrl.semesterList = [];

    ctrl.loadList = function(){

        semesterFactory.query().then(function(data){

            ctrl.semesterList = data.semesters;

            ctrl.loaded = true;

        }, function (error){
      
            console.log('ruh roh');
      
        });

    };

    ctrl.loadList();

}]);
