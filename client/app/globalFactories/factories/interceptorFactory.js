globals.factory('httpInterceptor', ['$q', '$rootScope', '$log', function ($q, $rootScope, $log) {

    var numLoadings = 0;

    return {
        request: function (request) {
            // Show loader
            
            numLoadings++;
            
            $rootScope.$broadcast("loader_show");
            
            return request || $q.when(request);

        },
        response: function (response) {

            if ((--numLoadings) === 0) {
                // Hide loader
            
                $rootScope.$broadcast("loader_hide");
            }

            return response || $q.when(response);

        },
        responseError: function (response) {

            if ((--numLoadings) === 0) {
                // Hide loader
             
                $rootScope.$broadcast("loader_hide");
            
            }

            return $q.reject(response);
        }
    };
}]);
