// start extensionsCodesFactories.js

globals.factory('extensioncodesFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {

  var extcodesResource = $resource('api/extensioncodes.json', {}, 

    {

      'query': {method: 'GET', isArray: false},

      'create': {method: 'POST'}

    });

  var extcodeResource = $resource('api/extensioncode/:id.json',  {id:'@id'},

    {

      'get': {method: 'GET', isArray: false},

      'update': {method: 'POST'},

      'remove': {method: 'POST'}

    }); 

  var factory = {
    
    // add promises
    query : function () {

      var deferred = $q.defer();

      extcodesResource.query({},

        function (resp) {

          deferred.resolve(resp);

        });

      return deferred.promise;

    },

    get : function (params) {
  
      var deferred = $q.defer();
  
      extcodeResource.get({id: params.id},
  
        function (resp) {
  
          deferred.resolve(resp);
  
        });
  
      return deferred.promise;
  
    },
  
    create : function (payload) {

      console.log(payload);
  
      var deferred = $q.defer();
  
      extcodesResource.create(payload, function(resp){
  
        deferred.resolve(resp);
  
      });

      return deferred.promise;
  
    },
  
    update : function (params, payload) {
  
      payload._method = 'put';

      delete payload.id;
      
      delete payload.created;
      
      delete payload.updated;

      var deferred = $q.defer();
    
      extcodeResource.update({id: params.id}, payload, function(resp){
  
        deferred.resolve(resp);
  
      });

      return deferred.promise;
  
    },
  
    remove : function (params) {
  
      var deferred = $q.defer();

      var payload = { _method: 'delete' };
  
      extcodeResource.remove({id: params.id}, payload, function(resp){
  
        deferred.resolve(resp);
  
      });

      return deferred.promise;
  
    }
  
  };

  return factory;

}]);