// start semesterFactory.js

globals.factory('semesterFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {
  

  var semestersResource = $resource('api/semesters.json', {},
    
    {

      'query': { method: 'GET', isArray: false, cache: false },

      'create': {method: 'POST'} 

    });


  var semesterResource = $resource('api/semester/:semester.json', {semester:'@semester'},

    {

      'get': {method: 'GET'},

      'update': {method: 'POST'},

      'remove': {method: 'POST'}

    }); 


  var currentSemesterResource = $resource('api/semesters/current.json', {}, 

    {'query': {method: 'GET', isArray: false, cache: true} });


  var observerCallbacks = [],


  //call this when you know the semester has been changed
  notifyObservers = function(){
    
    angular.forEach(observerCallbacks, function(callback){
    
      callback();
    
    });
  
  },

  setSelectedSemester = function(s){
  
    factory.selectedSemester = s;

    notifyObservers();
  
  },

  selectCurrentSemester = function(){

    factory.selectedSemester = factory.getCurrentSemester()
    
    .then(function(data){

      notifyObservers();

      return data.title;

    });

  },

  factory = {
    
    //register an observer from a controller
    registerObserverCallback : function(callback){
    
      observerCallbacks.push(callback);
    
    },
    
    query : function () {
    
      // returns list of all semesters
    
      var deferred = $q.defer();
    
      semestersResource.query({}, function(resp) {
    
          deferred.resolve(resp);
    
        });
    
      return deferred.promise;
    
    },
   
    get : function (params) {
 
      var deferred = $q.defer();
 
      semesterResource.get({semester: params.semester},
 
        function (resp) {
 
          deferred.resolve(resp);
 
        });
 
      return deferred.promise;
 
    },
 
    create : function (payload) {

      var deferred = $q.defer();
  
      semestersResource.create(payload, function(resp){
 
        deferred.resolve(resp);
 
      });

      return deferred.promise;
 
    },
 
    update : function (params, payload) {

      var deferred = $q.defer();

      payload._method = 'put';
   
      semesterResource.update({semester: params.semester}, payload, function(resp){
 
        deferred.resolve(resp);
 
      });

      return deferred.promise;
 
    },
 
    remove : function (params) {

      var deferred = $q.defer();

      var payload = { _method: 'delete' };
  
      semesterResource.remove({semester: params.semester}, payload, function(resp){
 
        deferred.resolve(resp);
 
      });

      return deferred.promise;
    
    },

    getCurrentSemester : function(){
   
      // returns currently active semester
   
      var deferred = $q.defer();
   
      currentSemesterResource.query({},
   
        function (resp) {
   
          deferred.resolve(resp);
   
        });
   
      return deferred.promise;
   
    },

    selectedSemester : "",

    setSelectedSemester : setSelectedSemester,

    selectCurrentSemester : selectCurrentSemester
  
  };

  // add methods for getting and setting selected semester
  
  return factory;

}]);