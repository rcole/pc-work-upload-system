globals.factory('profileFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {
  

  var profileResource = $resource('api/profile.json', {}, 

  {
  
    'get': {method: 'GET', isArray: false, cache: true},

    'update': {method: 'POST'}
  
  });


  var factory = {

    // add promises
    get : function () {

      var deferred = $q.defer();

      profileResource.get({},

        function (resp) {

          deferred.resolve(resp);

        });

      return deferred.promise;

    },

    update : function (payload) {

      payload._method = 'put';

      var deferred = $q.defer();

      profileResource.update({}, payload, function(resp){

        console.log(resp);

        deferred.resolve(resp);

      });

      return deferred.promise;

    }
  
  };

  return factory;

}]);