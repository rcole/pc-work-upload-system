// start extensionsCodesControllers.js

extcodesModule.controller('extensioncodesController', ['$scope', '$q', 'extensioncodesFactory', 'programsFactory', 'classesFactory', 'semesterFactory', 'usersFactory', 'classAssignmentFactory', 'messageCenterService', function ($scope, $q, extensioncodesFactory, programsFactory, classesFactory, semesterFactory, usersFactory, classAssignmentFactory, messageCenterService) {

    console.log('extensioncodesController instantiated');

    var ctrl = this;

    var currentSemester = ctrl.currentSemester; // to get this loaded in time... janky, but otherwise would need nested promises in loadXXX functions
    
    ctrl.extList = [];

    ctrl.displayList = [];

    ctrl.loaded = false;

    ctrl.loadList = function(){

        extensioncodesFactory.query().then(function(data){

            ctrl.loaded = true;

            ctrl.extList = data.extensionCodes;

            ctrl.displayList = data.extensionCodes;

        }, function (error){

            console.log('ruh roh');

        });

    };

    ctrl.loadList();

}]);