// extcodesModule.js Starts here 

var extcodesModule = angular.module('extcodesModule', ['smart-table','ngResource']);

extcodesModule.config(['$stateProvider','$urlRouterProvider','$locationProvider',function($stateProvider, $urlRouterProvider, $locationProvider){

  var extCodes = {
      name: "extCodes",
      url: "/extensioncodes",
      templateUrl: "app/webroot/tpl/extensioncodeList.tpl.html",
      controller: "extensioncodesController as extCodes"
    },
    extCodeEdit = {
      name: "extCodeEdit",
      parent: "extCodes",
      url: "/:id/edit",
      templateUrl: "app/webroot/tpl/extensioncodeForm.tpl.html",
      controller: "extensioncodeEditController as extForm"
    },
    extCodeCreate = {
      name: "extCodeCreate",
      parent: "extCodes",
      url: "/create",
      templateUrl: "app/webroot/tpl/extensioncodeForm.tpl.html",
      controller: "extensioncodeCreateController as extForm"
    };

  $stateProvider
	.state(extCodes)
	.state(extCodeEdit)
	.state(extCodeCreate);

}]);