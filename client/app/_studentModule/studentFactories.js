// studentFactory.js

// this is janky, but only makes sense here I think. Maybe could move to global module, or if profile stuff gets split off it would make sense there.

studentApp.factory('studentFactory', ['$http','$resource','$q', function($http, $resource, $q) {

  var enrolledClassesResource = $resource('api/student/:studentId/classes.json', {studentId: '@studentId'},

    { 'query' : { method: 'GET', isArray: false, cache: true } });

  var factory = {

    query : function (params) {

      var deferred = $q.defer();

      enrolledClassesResource.query({studentId: params.studentId},

        function (resp) {

          deferred.resolve(resp);

        });

      return deferred.promise;

    }

  };

  return factory;

}]);