// stuModule.js Starts here 

var studentApp = angular.module('studentApp' , [
  // uploads, prolly more too
  'globals',
  'semestersModule',
  'flow',
  'submitModule',
  'extcodesModule',
  'MessageCenterModule',
  'uservoiceModule',
  'usersModule',
  'analyticsModule',
  'submissionsListModule'
]);

// don't we need student routes? I think these are not done yet.

studentApp.config(['$stateProvider','$urlRouterProvider','$locationProvider',function($stateProvider, $urlRouterProvider, $locationProvider){
    
  $urlRouterProvider.otherwise("/submit");
  $locationProvider.html5Mode(false);
    
}]);