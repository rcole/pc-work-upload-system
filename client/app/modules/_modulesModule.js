// modulesModule.js Starts here 

var modulesModule = angular.module('modulesModule', []);

modulesModule.config(['$stateProvider','$urlRouterProvider','$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider){    

    var programModules = {
        name: "programModules",
        parent: 'programView',
        url: "/modules",
        templateUrl: "app/webroot/tpl/program.moduleList.tpl.html",
        controller: "prgModulesController as prgModules"
    },
    moduleEdit = {
        name: "moduleEdit",
        parent: 'programModules',
        url: "/:module/edit",
        templateUrl: "app/webroot/tpl/moduleForm.tpl.html",
        controller: "moduleEditController as modForm"
    },
    moduleCreate = {
        name: "moduleCreate",
        parent: 'programModules',
        url: "/create",
        templateUrl: "app/webroot/tpl/moduleForm.tpl.html",
        controller: "moduleCreateController as modForm"
    };

    $stateProvider
        .state(programModules)
        .state(moduleEdit)
        .state(moduleCreate);

}]);

