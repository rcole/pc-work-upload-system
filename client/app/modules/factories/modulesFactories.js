// modulesFactory.js

modulesModule.factory('modulesFactory', ['$http','$resource','$q', function($http, $resource, $q) {

  // deprecated endpoint
  var allModulesResource = $resource('api/modules.json', {},

    {

      'query' : {method: 'GET', isArray: false, cache: true}

    });

  var modulesResource = $resource('api/program/:prg/modules.json', {prg: '@prg'},

    {

      'query' : {method: 'GET'},

      'create': {method: 'POST'}

    });

  var moduleResource = $resource('api/program/:prg/module/:module.json', {prg: '@prg', module: '@module'},

    {

      'get': {method: 'GET'},      

      'update': {method: 'POST'},

      'remove': {method: 'POST'} 

    });


  var factory = {

    // deprecated
    queryAll : function () {

      var deferred = $q.defer();

      allModulesResource.query({},

        function (resp) {

          deferred.resolve(resp);

        });

      return deferred.promise;

    },

    query : function (params) {

      var deferred = $q.defer();

      modulesResource.query({prg: params.program},

        function (resp) {

          deferred.resolve(resp);

        });

      return deferred.promise;

    },

    get: function (params) {

      var deferred = $q.defer();

      moduleResource.get({prg: params.program, module: params.module},

        function (resp) {

          deferred.resolve(resp);

        });

      return deferred.promise;

    },

    create : function (params, payload) {

      var deferred = $q.defer();

      modulesResource.create({prg: params.program}, payload, function(resp){

        deferred.resolve(resp);

      });

      return deferred.promise;

    },

    update : function (params, payload) {

      payload._method = 'put';

      var deferred = $q.defer();

      moduleResource.update({prg: params.program, module: params.module}, payload, function(resp){

        deferred.resolve(resp);

      });

      return deferred.promise;

    },

    remove : function (params) {

      var deferred = $q.defer();

      var payload = { _method: 'delete' };

      moduleResource.remove({prg: params.program, module: params.module}, payload, function(resp){

        deferred.resolve(resp);

      });

      return deferred.promise;

    }

  };

  return factory;

}]);