modulesModule.controller('moduleCreateController', ['$scope','$q','$timeout','$stateParams','$state','modulesFactory', 'messageCenterService', function($scope, $q, $timeout, $stateParams, $state, modulesFactory, messageCenterService) {
  
    console.log('moduleCreateController');
    
    var ctrl = this;

    ctrl.mode = 'create';

    ctrl.modal = true; 

    ctrl.message = '';

    ctrl.createItem = function(){

        var prgs = $scope.$parent.prgModules.programList;

        for (var i = prgs.length - 1; i >= 0; i--) {
          
            if(prgs[i].slug == $stateParams.program){

                ctrl.item.program_id = prgs[i].id;
                
            }
      
        }
  
        modulesFactory.create($stateParams, ctrl.item)

        .then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the program.", { timeout: 6000 });

            } else {

                ctrl.message = 'Module Created';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);

            }

            $scope.$parent.prgModules.loadList();

        },

        function(error){

            console.log(error);

        });

    };
  
}]);