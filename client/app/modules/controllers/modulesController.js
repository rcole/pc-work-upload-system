modulesModule.controller('prgModulesController', ['$scope', '$q', '$stateParams', 'modulesFactory', 'programsFactory', 'messageCenterService', function ($scope, $q, $stateParams, modulesFactory, programsFactory, messageCenterService) {

    console.log('prgModuleController instantiated');

    var ctrl = this;

    ctrl.loaded = false;

    ctrl.loadList = function(){

        modulesFactory.query($stateParams).then(function (data){

            ctrl.loaded = true;

            ctrl.programModuleList = data.modules;

        }, function (error){

            console.log('ruh roh');

        });

    };

    ctrl.loadList();

    programsFactory.query().then(function (data){

        ctrl.programList = data.programs;

    }, function (error){
        
        console.log('ruh roh');

    });
    
}]);