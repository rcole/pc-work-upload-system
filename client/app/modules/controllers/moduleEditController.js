modulesModule.controller('moduleEditController', ['$scope','$q','$stateParams','$state','$timeout','modulesFactory', 'messageCenterService', function($scope, $q, $stateParams, $state, $timeout, modulesFactory, messageCenterService) {
  
  console.log('moduleEditController');

  var ctrl = this;

    ctrl.mode = 'edit';

    ctrl.modal = true; 

    ctrl.deleting = false;

    ctrl.message = '';

    ctrl.toggleDelete = function(){

        ctrl.deleting = !ctrl.deleting;

    };

    ctrl.deleteItem = function(){
  
        modulesFactory.remove($stateParams)

        .then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while deleting the module.", { timeout: 6000 });

                console.log(data);

            } else {
                
                ctrl.message = 'Module Deleted';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);
            }
        },

        function(error){

            console.log(error);

        });

    };
    
    ctrl.updateItem = function(){
  
        modulesFactory.update($stateParams, ctrl.item)

        .then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the module.", { timeout: 6000 });

                console.log(data);

            } else {

                ctrl.message = 'Module Updated';

                $timeout(function(){

                    $state.go('moduleEdit', {module: data.module.slug});

                    ctrl.message = '';

                }, 1500);

            }

        },

        function(error){

            console.log(error);

        });

    };

    modulesFactory.get($stateParams).then(function(data){

        if (data._error.code == 1) {

            messageCenterService.add('danger', "Something went wrong while retrieving the module.", { timeout: 6000 });

        } else {

            ctrl.item = data.module;

        }

    },

    function(error){

        console.log(error);

    });

}]);