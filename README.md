# Usage
Assumption: current directory is `/var/www` which is also your Apache `DocumentRoot`.

Clone the app
```
git clone git@bitbucket.org:rcole/pc-work-upload-system.git
```

Switch to the cloned directory
```
cd pc-work-upload-system
```

Initialize all submodules (aka dependencies used in this app)
```
git submodule update --init --recursive
```

Add the right permissions
```
chmod -R 0777 app/resources
```

Copy the database connections file and edit it to add your connection data
```
cp app/config/bootstrap/connections.sample.php app/config/bootstrap/connections.php
```

Set up db with some basic records
```
./li3 db reload
```